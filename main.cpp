/** @file main.cpp
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2024 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2014/12/02
 *
 *  basic QML handling and environment setup
 */

#include <QApplication>
#include <QIcon>
#include <QMessageBox>
#include <QQmlApplicationEngine>
#include <QtQml> // qmlRegisterType<>()

#include "include/icc_download.h"
#include "include/icc_profile.h"
#include "include/icc_profile_manager.h"

#include <QTextStream>
#include <QTranslator>
#include <QLocale>
#include <QUrl>
#include <QObject>
#include <QSysInfo>
#include <QtGui/QScreen>
#include <QTableWidget>

#ifdef OYRANOS_LIB
#include <oyranos_core.h>
#include <oyStruct_s.h>
#include <oyObject_s.h>
#else
# define oyMSG_DBG oyjlMSG_SECURITY_ALERT
# define oyMSG_WARN oyjlMSG_INFO
# define oyMSG_ERROR oyjlMSG_ERROR
# ifdef __cplusplus
extern "C" {
extern int * oyjl_debug;
}
# endif
# define oy_debug (*oyjl_debug)
#endif

#if defined(OYJL_LIB)
# include <oyjl.h>
oyjlTranslation_s * trc = NULL;
#else
# include "src/oyjl_args.c"
char *     oyjlStringCopy            ( const char        * string,
                                       void*            (* alloc)(size_t));
char*      oyjlStringAppendN         ( const char        * text,
                                       const char        * append,
                                       int                 append_len,
                                       void*            (* alloc)(size_t size) );
void               oyjlUi_ReleaseArgs( oyjlUi_s         ** ui );
# define oyjlUi_Release oyjlUi_ReleaseArgs
# define oyjlLibRelease()
#endif /* OYJL_LIB */
#ifdef OYJL_HAVE_LOCALE_H
# include <locale.h>
#endif

#if defined(Q_OS_ANDROID)
# include <android/log.h>
#endif
#ifdef _
#undef _
#endif
#if defined(OYJL_LIB)
#define _(text) oyjlTranslate( trc, text )
#else
#define _(x) x
#endif

// startup stuff
IccProfileManager mgr;

void printObjectClassNames( QObject * o )
{
  const QObjectList list = o->children();
  for(int i= 0; i < list.count(); ++i)
  {
    LOG(QString("QML object[") + QString::number(i) + QString("] ") + list[i]->objectName()
        + " " + list[i]->metaObject()->className() );
    printObjectClassNames( list[i] );
  }
}

int level_Prog = 0;
int iccMessageFunc( int code, const void* c, const char * format, ... )
{
#ifdef OYRANOS_LIB
  oyStruct_s * context = (oyStruct_s*) c;
#endif
  char* text = 0, *pos = 0;
  va_list list;
  const char * type_name = "";
  int id = -1, i;

  if(code == oyMSG_DBG && !oy_debug)
    return 0;


#ifdef OYRANOS_LIB
  if(context && oyOBJECT_NONE < context->type_)
  {
    type_name = oyStructTypeToText( context->type_ );
    id = oyObject_GetId( context->oy_ );
  }
#endif

  text = (char*)calloc(sizeof(char), 4096);
  text[0] = 0;

  if(format && strlen(format) > 6)
  {
    if(strncasecmp("Start:", format, 6 ) == 0)
      ++level_Prog;
    if(strncasecmp("  End:", format, 6 ) == 0)
      --level_Prog;
  }

# define MAX_LEVEL 20
  if(level_Prog < 0)
    level_Prog = 0;
  if(level_Prog > MAX_LEVEL)
    level_Prog = MAX_LEVEL;
  for (i = 0; i < level_Prog; i++)
    sprintf( &text[strlen(text)], " ");


  switch(code)
  {
    case oyMSG_WARN:
         strcpy( &text[strlen(text)], QObject::tr("WARNING").toLocal8Bit().data());
         break;
    case oyMSG_ERROR:
         strcpy( &text[strlen(text)], QObject::tr("!!! ERROR").toLocal8Bit().data());
         break;
    case oyMSG_DBG:
         break;
  }

  snprintf( &text[strlen(text)], 4096 - strlen(text), "%s[%d] ",
                                                      type_name,id );

  va_start( list, format);
  vsnprintf( &text[strlen(text)], 4096 - strlen(text), format, list);
  va_end  ( list );

  pos = &text[strlen(text)];
  *pos = '\n';
  pos++;
  *pos = 0;

  icc_examin_log( text );

  if(text) free( text );

  return 0;
}

#define ICC_EXAMIN_VERSION "1.0"
#define APP_NAME "iccExaminQml"
int main(int argc, char *argv[])
{
  /*LOG( QString("app args[") + QString::number(argc) + "]:" );
  for(int i = 0; i < argc; ++i)
      LOG( QString(argv[i]) );
  fprintf( stderr, "\n" );*/

#ifdef OYRANOS_LIB
  oyMessageFuncSet( iccMessageFunc );
#else
  extern oyjlMessage_f oyjlMessage_p;
  oyjlMessage_p = iccMessageFunc;
#endif
#define MY_DOMAIN APP_NAME
  const char * loc = NULL;
  const char * lang = getenv("LANG");
  loc = oyjlSetLocale(LC_ALL,"");
  if(!loc)
  {
    loc = lang;
    fprintf( stderr, "%s", oyjlTermColor(oyjlRED,"Usage Error:") );
    fprintf( stderr, " Environment variable possibly not correct. Translations might fail - LANG=%s\n", oyjlTermColor(oyjlBOLD,lang) );
  }
  if(lang)
    loc = lang;
#ifdef OYRANOS_LIB
  oyI18NSet(1,0);
#endif /* OYRANOS_LIB */
#if defined(OYJL_LIB) || defined(OYRANOS_LIB)
  oyjl_val catalog; 
# include "de.i18n.h"
  int size = sizeof(iccExaminQml_i18n_oiJS);
  catalog = (oyjl_val) oyjlStringAppendN( NULL, (const char*) iccExaminQml_i18n_oiJS, size, malloc );
  if(0)
    fprintf( stderr, "loc: \"%s\" domain: \"%s\" catalog-size: %d", loc, MY_DOMAIN, size );
  trc = oyjlTranslation_New( loc, MY_DOMAIN, &catalog, 0,0,0,0 );
#endif /* OYJL_LIB || OYRANOS_LIB */

  const char * help = 0;
  int verbose = 0;
  int state = 0;
  int version = 0;
  const char * exportX = NULL;

  oyjlUiHeaderSection_s sections[] = {
    /* type, nick,            label, name,                  description  */
    { "oihs", "version",      NULL,  ICC_EXAMIN_VERSION,    NULL},
    { "oihs", "manufacturer", NULL,  "Kai-Uwe Behrmann",    "https://gitlab.com/beku" },
    { "oihs", "copyright",    NULL,  "Copyright © 2014-2024 Kai-Uwe Behrmann", NULL },
    { "oihs", "license",      NULL,  "AGPL-3.0",            "https://gitlab.com/oyranos" },
    { "oihs", "oyjl_module_author",NULL,"Kai-Uwe Behrmann", "https://gitlab.com/beku" },
    { "oihs", "documentation",NULL,  "",                    _("The ICC Examin colour examination tool accepts as input ICC profiles, CGATS colour measurement files and named colour files.")},
    { "oihs", "date",         NULL,  "2024-05-31T12:00:00", _("May 31, 2024")},
    { "",0,0,0,0}};

  /* declare the option choices  *   nick,          name,               description,                  help */
  oyjlOptionChoice_s S_choices[] = {{oyjlStringCopy("oyranos(3)",NULL),NULL,NULL,                     NULL},
                                    {NULL,NULL,NULL,NULL}};
  /* declare options - the core information; use previously declared choices */
  oyjlOption_s oarray[] = {
  /* type,   flags,                      o,  option,          key,      name,          description,                  help, value_name,         
        value_type,              values,             variable_type, variable_name, properties */
    {"oiwi", 0,                          "#",NULL,            NULL,     _("Start"),    _("Show GUI"),                NULL, NULL,
        oyjlOPTIONTYPE_NONE,     {0},                oyjlNONE,      {0},           NULL},
    {"oiwi", OYJL_OPTION_FLAG_EDITABLE,  "@",NULL,            NULL,     _("Name"),     _("Profile Name"),            NULL, _("ICC_PROFILE_NAME"),
        oyjlOPTIONTYPE_CHOICE,   {0},                oyjlNONE,      {0},           NULL},
    {"oiwi", OYJL_OPTION_FLAG_ACCEPT_NO_ARG,"h", "help",NULL, NULL, NULL, NULL, NULL, oyjlOPTIONTYPE_CHOICE,{0}, oyjlSTRING, {.s = &help},NULL },
    {"oiwi", 0,     "v", "verbose", NULL, _("Verbose"), _("Verbose"),        NULL, NULL,          oyjlOPTIONTYPE_NONE, {0}, oyjlINT, {.i = &verbose},NULL },
    {"oiwi", 0,     "X", "export",  NULL, NULL,         NULL,                NULL, NULL,          oyjlOPTIONTYPE_CHOICE, {0}, oyjlSTRING, {.s = &exportX},NULL },
    {"oiwi", 0,     "V", "version", NULL, _("Version"),  _("Version"),                 NULL, NULL,               
        oyjlOPTIONTYPE_NONE,     {0},                oyjlINT,       {.i=&version},NULL},
    {"oiwi", 0,     "S", "man-see_also",NULL,_("SEE ALSO"),NULL,             NULL, NULL,
        oyjlOPTIONTYPE_CHOICE,   {.choices={(oyjlOptionChoice_s*) oyjlStringAppendN( NULL, (const char*)S_choices, sizeof(S_choices), malloc ), 0}}, oyjlNONE,      {0},NULL},
    {"",0,0,0,0,0,0,0, NULL, oyjlOPTIONTYPE_END, {NULL},oyjlNONE,{NULL},NULL}
  };

  /* declare option groups, for better syntax checking and UI groups */
  oyjlOptionGroup_s groups_no_args[] = {
  /* type,   flags, name,               description,                  help,               mandatory,     optional,      detail,        properties */
    {"oiwg", 0,     "ICCExamin",        _("ICC Viewer"),              NULL,               "@,#",         "v",           "@,#",         NULL},/* parsed and checked with -i option */
    {"oiwg", 0,     _("Misc"),          _("General options"),         NULL,               "h,X,V",       "v",           "h,X,V,v",     NULL},/* just show in documentation */
    {"",0,0,0,0,0,0,0,0}
  };

  /* tell about the tool */
  oyjlUi_s * ui = oyjlUi_Create( argc, (const char**)argv, /* argc+argv are required for parsing the command line options */
                                       APP_NAME, _("graphical ICC examination tool"), _("ICC Version 2 and 4 compatible Color Profile Examine Tool"), "logo",
                                       sections, oarray, groups_no_args, &state );
  if( state & oyjlUI_STATE_EXPORT &&
      exportX)
    return 0;
  if( version )
    return 0;
  if(state & oyjlUI_STATE_HELP)
  {
    fprintf( stderr, "%s\n\tman " APP_NAME "\n\n", _("For more information read the man page:"));
    oyjlUi_Release( &ui );
    oyjlLibRelease();
    return 0;
  }
  if(!ui) return 1;
  /* done with options handling */

  Q_INIT_RESOURCE(app);
/*#if QT_VERSION >= 0x050600
    // workaround 5.6 incompatibility with a <= 5.5.x desktop (Xft.dpi: 192)
    qputenv("QT_AUTO_SCREEN_SCALE_FACTOR", "1");
    qputenv("QT_SCALE_FACTOR", "1");
    //app.setAttribute(Qt::AA_DisableHighDpiScaling);
#endif*/
  QTranslator translator;
  QString lname( ":/translations/app_" + QLocale::system().name() );
  if(!translator.load( lname ))
  {
    fprintf(stderr, "failed loading locale: %s\n", lname.toLocal8Bit().data() );
    //LOG( QString("failed loading locale: ") + lname );
  }

  QApplication app(argc, argv);
  app.installTranslator(&translator);
#if QT_VERSION_MAJOR == 5
    foreach (QScreen * screen, QGuiApplication::screens())
        screen->setOrientationUpdateMask(Qt::LandscapeOrientation | Qt::PortraitOrientation |
                                         Qt::InvertedLandscapeOrientation | Qt::InvertedPortraitOrientation);
#endif

    app.setApplicationName(QString(APP_NAME));
    app.setApplicationDisplayName(QString("ICC Examin Qml"));
    app.setApplicationVersion(ICC_EXAMIN_VERSION);
    app.setOrganizationName(QString("oyranos.com"));




    QIcon icon(QStringLiteral(":/images/logo-sw.svg"));
    if(icon.isNull())
        LOG( "Icon does not exist ???" );
    app.setWindowIcon(icon);


    qmlRegisterType<IccProfile>("IccProfile", 1, 0, "IccProfile");
    qmlRegisterType<IccDownload>("IccDownload", 1, 0, "IccDownload");

    QQmlApplicationEngine engine;
    // not needed (inject the profile object into QML)
    //engine.rootContext()->setContextProperty( "profile", profile );
    engine.load(QUrl(QStringLiteral("qrc:qml/main.qml")));

    // extract the IccProfile from QML
      // get all objects from QML
    QList<QObject*> qmlObjects = engine.rootObjects();
    if(!qmlObjects.count())
      LOG( QString("no QML objects") );
    /*else
      LOG( QString("qml root objects: ") + QString::number( qmlObjects.count() ) );*/
    QObject * o = qmlObjects[0];
    //LOG( QString("qml objects: ") + QString::number( o->children().count() ) );
    //printObjectClassNames( o );
      // filter out the IccProfile class type
    QList<IccProfile*> profiles = o->findChildren<IccProfile*>();
    IccProfile * profile = NULL;
    if(!profiles.count())
      LOG( QString("profile: ").arg(QString::number(profiles.count())) );
    else
      // get the profile
      profile = profiles[0];
    if(!profile)
      LOG( QString("no profile found -> Katastrophe") );

    // keep the profile visible in C++ for the JNI callback
    mgr.setProfile( profile );

    QList<QTabWidget*> tabs = o->findChildren<QTabWidget*>();
    if(tabs.count())
    {
      QTabWidget * t = tabs[0];
      if(t)
        LOG( QString("tab found") );
    }

    if(argc > 1)
        mgr.setUri( QString( argv[1] ) );
    else
        mgr.setUri( QString( "rgb" ) );

    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("ApplicationVersion", QVariant::fromValue( app.applicationVersion() ));
    ctxt->setContextProperty("SysProductInfo", QVariant::fromValue( QSysInfo::prettyProductName() ));
    ctxt->setContextProperty("QtRuntimeVersion", QVariant::fromValue( QString(qVersion()) ));
    ctxt->setContextProperty("QtCompileVersion", QVariant::fromValue( QString(QT_VERSION_STR) ));

    return app.exec();
}


#if defined(Q_OS_ANDROID)
#include <jni.h>
// match the com.oyranos.iccExamin.ShareActivity class in android/src/com/oyranos/iccExamin/IccExaminActivity.java
extern "C" {
JNIEXPORT void JNICALL Java_com_oyranos_iccExamin_IccExaminActivity_passViewIntent(JNIEnv * pEnv, jclass, jstring in_string)
{
    const char * c_string = (*pEnv).GetStringUTFChars( in_string, 0 );

    __android_log_print(ANDROID_LOG_INFO, "IccExamin.passViewIntent()", "%s", c_string );

    // the string is stored and passed to the IccProfile as soon as possible
    mgr.setUri( QString(c_string) );
}

JNIEXPORT void JNICALL Java_com_oyranos_iccExamin_IccExaminActivity_passMemIntent(JNIEnv * pEnv, jclass, jbyteArray input, jint len, jstring in_string)
{
    // copy
    jbyte * jbuffer = pEnv->GetByteArrayElements( input, NULL);
    QByteArray data( (const char*) jbuffer, len );
    pEnv->ReleaseByteArrayElements( input, jbuffer, 0);
    const char * c_string = (*pEnv).GetStringUTFChars( in_string, 0 );

    __android_log_print(ANDROID_LOG_INFO, "IccExamin.passMemIntent()", "%s", c_string );

    mgr.setMem( data, QString(c_string) );
}

JNIEXPORT void JNICALL Java_com_oyranos_iccExamin_IccExaminActivity_passMessageCanNotHandleNewTask(JNIEnv, jclass )
{
    /*QMessageBox msgBox;
    msgBox.setText( QObject::tr("Can not handle new task for content. Please retry.") );
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();*/
    __android_log_print(ANDROID_LOG_INFO, "IccExamin.passMessageCanNotHandleNewTask()", "Want give up" );
    mgr.getProfile()->setQuitMsg( QObject::tr("Can not handle content in running task. Please retry.").toLocal8Bit().data() );
    __android_log_print(ANDROID_LOG_INFO, "IccExamin.passMessageCanNotHandleNewTask()", "Want give up?" );
}

//getExternalStorageDirectory();

}
#endif
