/** @file icc_profile.cpp
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2014/12/08
 *
 *  - IccProfLib and Oyranos -> JSON conversion
 *  - a few Oyranos functions
 */

#include "include/icc_profile.h"
#include <QFile>
#include <QFileInfo>
#include <QLocale>
#include <QUrl>
#include <QTextStream>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>


#if 0
#define ICC_LIB_NAME "SampleICC"
#include <../SampleICC-qmake/sampleicc.h>
#else
#define ICC_LIB_NAME "RefIccMAX"
#ifdef REFICCMAX_LIB
#include "IccProfile.h"
#include "IccTag.h"
#include "IccUtil.h"
#include "IccProfLibVer.h"
#include "IccMpeBasic.h"
#else
#include "../RefIccMAX/IccProfLib/IccProfile.h"
#include "../RefIccMAX/IccProfLib/IccTag.h"
#include "../RefIccMAX/IccProfLib/IccUtil.h"
#include "../RefIccMAX/IccProfLib/IccProfLibVer.h"
#include "../RefIccMAX/IccProfLib/IccMpeBasic.h"
#endif
#endif

#define ICC_H
#ifdef OYRANOS_LIB
#include <oyranos.h>
#include <oyranos_color.h>
#include <oyranos_image.h>
#include <oyProfile_s.h>
#include <oyProfiles_s.h>
const char * myProfile_GetFileName( void * ptr )
{ return oyProfile_GetFileName( (oyProfile_s*)ptr, -1 ); }
int myProfile_GetSize( void * ptr )
{ return oyProfile_GetSize( (oyProfile_s*)ptr, 0 ); }
const char * myProfile_GetDesc( void * ptr )
{ return oyProfile_GetText( (oyProfile_s*)ptr, oyNAME_DESCRIPTION ); }
void myProfile_Release( oyProfile_s ** ptr )
{ oyProfile_Release( (oyProfile_s**)ptr ); }
#else
#define myProfile_GetFileName(ptr) m_file_name.toLocal8Bit().data()
#define myProfile_GetSize(ptr) m_bytes.length() 
#define myProfile_GetDesc(ptr)     ""
#define myProfile_Release(ptr)     *ptr = NULL;
#define OYRANOS_VERSION_NAME       ""
#define oyVersionString(num) strdup("")
#endif

void ReplaceStringInPlace(std::string& subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

#ifdef OYRANOS_LIB
QString ieICCColorSpaceGetName ( icColorSpaceSignature sig )
{ return QString(oyICCColorSpaceGetName(sig)); }
QString  ieICCTagDescription ( icTagSignature      sig )
{ return QString(oyICCTagDescription(sig)); }
#else
QString ieICCColorSpaceGetName ( icColorSpaceSignature sig )
{
  QString text;

  switch ((unsigned int)sig) {
    case icSigXYZData: text =QObject::tr("XYZ"); break;
    case icSigLabData: text =QObject::tr("Lab"); break;
    case icSigLuvData: text =QObject::tr("Luv"); break;
    case icSigYCbCrData: text =QObject::tr("YCbCr"); break;
    case icSigYxyData: text =QObject::tr("Yxy"); break;
    case icSigRgbData: text =QObject::tr("Rgb"); break;
    case icSigGrayData: text =QObject::tr("Gray"); break;
    case icSigHsvData: text =QObject::tr("Hsv"); break;
    case icSigHlsData: text =QObject::tr("Hls"); break;
    case icSigCmykData: text =QObject::tr("Cmyk"); break;
    case icSigCmyData: text =QObject::tr("Cmy"); break;
    case icSig2colorData: text =QObject::tr("2color"); break;
    case icSig3colorData: text =QObject::tr("3color"); break;
    case icSig4colorData: text =QObject::tr("4color"); break;
    case icSig5colorData: text =QObject::tr("5color"); break;
    case icSig6colorData: text =QObject::tr("6color"); break;
    case icSig7colorData: text =QObject::tr("7color"); break;
    case icSig8colorData: text =QObject::tr("8color"); break;
    case icSig9colorData: text =QObject::tr("9color"); break;
    case icSig10colorData: text =QObject::tr("10color"); break;
    case icSig11colorData: text =QObject::tr("11color"); break;
    case icSig12colorData: text =QObject::tr("12color"); break;
    case icSig13colorData: text =QObject::tr("13color"); break;
    case icSig14colorData: text =QObject::tr("14color"); break;
    case icSig15colorData: text =QObject::tr("15color"); break;
    default: text = QString::number(sig); break;
  }
  return text;
}
/** Function: ieICCTagDescription
 *  @brief get tag description
 *
 *  @since Oyranos: version 0.1.8
 *  @date  1 january 2008 (API 0.1.8)
 */
QString  ieICCTagDescription ( icTagSignature      sig )
{
  switch ((unsigned int)sig) {
    case icSigAToB0Tag: return QObject::tr("Lookup table, device to PCS, intent perceptual");
    case icSigAToB1Tag: return QObject::tr("Lookup table, device to PCS, intent relative colorimetric");
    case icSigAToB2Tag: return QObject::tr("Lookup table, device to PCS, intent saturation");
    case icSigAToB3Tag: return QObject::tr("Lookup table, device to PCS, intent absolute");
    case icSigAToM0Tag: return QObject::tr("Lookup table, device to material");
    case icSigBlueColorantTag: return QObject::tr("Blue Colorant");
    case icSigBlueTRCTag: return QObject::tr("Blue tone reproduction curve");
    case icSigBRDFAToB0Tag: return QObject::tr("BRDF Lookup table, device to PCS, intent perceptual");
    case icSigBRDFAToB1Tag: return QObject::tr("BRDF Lookup table, device to PCS, intent relative colorimetric");
    case icSigBRDFAToB2Tag: return QObject::tr("BRDF Lookup table, device to PCS, intent saturation");
    case icSigBRDFAToB3Tag: return QObject::tr("BRDF Lookup table, device to PCS, intent absolute");
    case icSigBRDFDToB0Tag: return QObject::tr("BRDF Lookup table, device to spectral PCS, intent perceptual");
    case icSigBRDFDToB1Tag: return QObject::tr("BRDF Lookup table, device to spectral PCS, intent relative colorimetric");
    case icSigBRDFDToB2Tag: return QObject::tr("BRDF Lookup table, device to spectral PCS, intent saturation");
    case icSigBRDFDToB3Tag: return QObject::tr("BRDF Lookup table, device to spectral PCS, intent absolute");
    case icSigBToA0Tag: return QObject::tr("Lookup table, PCS to device, intent perceptual");
    case icSigBToA1Tag: return QObject::tr("Lookup table, PCS to device, intent relative colorimetric");
    case icSigBToA2Tag: return QObject::tr("Lookup table, PCS to device, intent saturation");
    case icSigBToA3Tag: return QObject::tr("Lookup table, PCS to device, intent absolute");
    case icSigCalibrationDateTimeTag: return QObject::tr("Calibration date");
    case icSigCharTargetTag: return QObject::tr("Color measurement data");
    case icSigChromaticAdaptationTag: return QObject::tr("Color adaption matrix");
    case icSigChromaticityTag: return QObject::tr("Chromaticity");
    case icSigColorEncodingParamsTag: return QObject::tr("Color encoding parameters");
    case icSigColorSpaceNameTag: return QObject::tr("Color space name");
    case icSigColorantOrderTag:  return QObject::tr("Color channel order");
    case icSigColorantTableTag: return QObject::tr("Color channel table");
    case icSigColorantTableOutTag: return QObject::tr("Colorant table output");
    case icSigColorimetricIntentImageStateTag: return QObject::tr("Colorimetric intent image state");
    case icSigCopyrightTag: return QObject::tr("Copyright");
    case icSigCrdInfoTag: return QObject::tr("Postscript CRD Information");
#ifdef OYRANOS_LIB
    case icSigCustomToStandardPcsTag: return QObject::tr("Custom to standard PCS");
#endif
    case icSigCxFTag: return "CxF";
    case icSigDataTag: return QObject::tr("Data");
    case icSigDateTimeTag: return QObject::tr("Date and time");
    case 0x646d7770 /*icSigDeviceMediaWhitePointTag*/: return QObject::tr("Device media white point");
    case icSigDeviceMfgDescTag: return QObject::tr("Device manufacturerer description");
    case icSigDeviceModelDescTag: return QObject::tr("Device model description");
    case icSigDeviceSettingsTag: return QObject::tr("Device Settings");
    case icSigDToB0Tag: return QObject::tr("Override lookup table, device to PCS, intent perceptual");
    case icSigDToB1Tag: return QObject::tr("Override lookup table, device to PCS, intent relative colorimetric");
    case icSigDToB2Tag: return QObject::tr("Override lookup table, device to PCS, intent saturation");
    case icSigDToB3Tag: return QObject::tr("Override lookup table, device to PCS, intent absolute");
    case icSigBToD0Tag: return QObject::tr("Override lookup table, PCS to device, intent perceptual");
    case icSigBToD1Tag: return QObject::tr("Override lookup table, PCS to device, intent relative colorimetric");
    case icSigBToD2Tag: return QObject::tr("Override lookup table, PCS to device, intent saturation");
    case icSigBToD3Tag: return QObject::tr("Override lookup table, PCS to device, intent absolute");
    case icSigGamutTag: return QObject::tr("gamut");
    case icSigGrayTRCTag: return QObject::tr("Gray tone reproduction curve");
    case icSigGreenColorantTag: return QObject::tr("Green Colorant");
    case icSigGreenTRCTag: return QObject::tr("Green tone reproduction curve");
    case icSigLuminanceTag: return QObject::tr("Luminance");
    case icSigMeasurementTag: return QObject::tr("Measurement");
    case icSigMediaBlackPointTag: return QObject::tr("Media black point");
    case icSigMediaWhitePointTag: return QObject::tr("Media white point");
    case icSigMetaDataTag: return QObject::tr("Meta Data");
    //case icSigNamedColorTag: return QObject::tr("Named Color");
    case icSigNamedColor2Tag: return QObject::tr("Named Color 2");
    case icSigPreview0Tag: return QObject::tr("Preview, perceptual");
    case icSigPreview1Tag: return QObject::tr("Preview, relative colorimetric");
    case icSigPreview2Tag: return QObject::tr("Preview, saturated");
    case icSigProfileDescriptionTag: return QObject::tr("Profile description");
    /*dscm*/
    case 1685283693: return QObject::tr("Profile description, multilingual");
    case icSigProfileSequenceDescTag: return QObject::tr("Profile sequence description");
    case 0x70736964 /*icSigProfileSequenceIdentifierTag*/: return QObject::tr("Profile sequence identification");
    case icSigPs2CRD0Tag: return QObject::tr("Postscript 2 CRD, perceptual");
    case icSigPs2CRD1Tag: return QObject::tr("Postscript 2 CRD, relative colorimetric");
    case icSigPs2CRD2Tag: return QObject::tr("Postscript 2 CRD, saturated");
    case icSigPs2CRD3Tag: return QObject::tr("Postscript 2 CRD, absolute colorimetric");
    case icSigPs2CSATag: return QObject::tr("Postscript 2 CSA");
    case icSigPs2RenderingIntentTag: return QObject::tr("Postscript 2 Rendering Intent");
    case icSigRedColorantTag: return QObject::tr("Red Colorant");
    case icSigRedTRCTag: return QObject::tr("Red tone reproduction curve");
    case icSigScreeningDescTag: return QObject::tr("Screening Description");
    case icSigScreeningTag: return QObject::tr("Screening");
    case icSigTechnologyTag: return QObject::tr("Technology");
    case icSigUcrBgTag: return QObject::tr("Under Color Removal (UCR)");
    case icSigViewingCondDescTag: return QObject::tr("Viewing conditions description");
    case icSigViewingConditionsTag: return QObject::tr("Viewing Conditions");
    /*DevD*/
    case 1147500100: return QObject::tr("Device colors");
    /*CIED*/
    case 1128875332: return QObject::tr("Measured colors");
    /*Pmtr*/
    case 1349350514: return QObject::tr("Profiling parameters");
    /*vcgt 1986226036*/
    case 0x76636774 /*icSigVideoCardGammaTable*/: return QObject::tr("VideoCardGammaTable");
    /*clrt*/
    //case icSigColorantTableOutType: return QObject::tr("Color channel output names");
    /**/
    case 0: return QObject::tr("----");
    default: { uint32_t i = sig;
             QString ot = ieICCTagDescription(sig);
             if(ot.length())
                 return ot;
             static char t[5];
             if( !iccExaminIsBigEndian() ) icSwab32Ptr(&i);
             memcpy (t,(char*)&i, 4);
             t[4] = 0;
             return QString(t);
           }
  }
}
#endif

#ifndef icSigUnix
#define icSigUnix 0x2A6E6978  /* '*nix' */
#endif
/** Function: ieICCPlatformDescription
 *  @brief get the ICC profile platform description
 *
 *  @since Oyranos: version 0.1.8
 *  @date  1 january 2008 (API 0.1.8)
 */
QString   ieICCPlatformDescription ( icPlatformSignature platform )
{
  switch ((unsigned int)platform)
  {
    case icSigMacintosh: return QObject::tr("Macintosh");
    case icSigMicrosoft: return QObject::tr("Microsoft");
    case icSigSolaris: return QObject::tr("Solaris");
    case icSigSGI: return QObject::tr("SGI");
    case icSigTaligent: return QObject::tr("Taligent");
    case icSigUnix: return QObject::tr("Unix and derivatives");
    default: return QString::number(platform);
  }
}

/** Function: ieICCCmmDescription
 *  @brief get the ICC profile CMM description
 *
 *  @since Oyranos: version 0.9.6
 */
QString  ieICCCmmDescription ( icSignature sig )
{
  switch ((unsigned int)sig)
  {
    case 0x41444245: /* ADBE */ return QObject::tr("Adobe CMM");
    case 0x41434D53: /* ACMS */ return QObject::tr("Agfa CMM");
    case 0x6170706c: /* appl */ return QObject::tr("Apple CMM");
    case 0x43434D53: /* CCMS */ return QObject::tr("ColorGear CMM");
    case 0x5543434D: /* UCCM */ return QObject::tr("ColorGear CMM Lite");
    case 0x55434D53: /* UCMS */ return QObject::tr("ColorGear CMM C");
    case 0x45464920: /* EFI  */ return QObject::tr("EFI CMM");
    case 0x46462020: /* FF   */ return QObject::tr("Fuji Film CMM");
    case 0x48434d4d: /* HCMM */ return QObject::tr("Harlequin RIP CMM");
    case 0x6172676C: /* argl */ return QObject::tr("Argyll CMS CMM");
    case 0x44676f53: /* LgoS */ return QObject::tr("LogoSync CMM");
    case 0x48444d20: /* HDM  */ return QObject::tr("Heidelberg CMM");
    case 0x6C636d73: /* lcms */ return QObject::tr("Little CMS CMM");
    case 0x4b434d53: /* KCMS */ return QObject::tr("Kodak CMS");
    case 0x4d434d44: /* MCML */ return QObject::tr("Konica Minolta CMM");
    case 0x5349474E: /* SIGN */ return QObject::tr("Mutho CMM");
    case 0x52474d53: /* RGMS / Rolf Gierling Multitools */ return QObject::tr("DeviceLink CMM");
    case 0x53494343: /* SICC */ return QObject::tr("SampleICC CMM");
    case 0x33324254: /* 32BT */ return QObject::tr("the imaging factory CMM");
    case 0x57544720: /* WTG  */ return QObject::tr("Ware To Go CMM");
    case 0x7a633030: /* zc00 */ return QObject::tr("Zoran CMM");
    default: return QString::number(sig);
  }
}

void describeIccCurve( CIccTagCurve * curve, const char * desc, QJsonObject & obj )
{
    int n = curve->GetSize();
    QJsonValue count(n);
    obj["count"] = count;
    QString d;
    if(n == 1)
        d = QString(QObject::tr("Curve with gamma %1").arg((double)(*curve)[0]*256.0));
    else
        d = QString(QObject::tr("Curve with %1 segments").arg(n));
    obj["value"] = desc ? desc : d;
    obj["description"] = d;
    obj["type"] = "curve";
    QJsonArray array;
    for(int i = 0; i < n; ++i) array.push_back( QJsonValue( (double)(*curve)[i] ) );
    obj["value_array"] = array;
}

void describeIccPCurve( CIccTagParametricCurve * curve, const char * desc, QJsonObject & obj )
{
    int n = curve->GetNumParam();
    QJsonValue count(n);
    obj["count"] = count;
    QJsonArray array;
    for(int i = 0; i < n; ++i)
        array.push_back( QJsonValue( (double)(*curve)[i] ) );
    int t = curve->GetFunctionType();
    QString d;
    switch(t)
    {
    case 0: d += QString(QObject::tr("Type:0 \nf(X)=X^%1")).arg( (double)(*curve)[0] ); break;
    case 1: d += QString(QObject::tr("Type:1 \nf(X)=(%2*X+%3)^%1 for X>=-%3/%2 \nf(X)=0 for (X<-%3/%2)"))
                 .arg( (double)(*curve)[0] )
                 .arg( (double)(*curve)[1] )
                 .arg( (double)(*curve)[2] ); break;
    case 2: d += QString(QObject::tr("Type:2 \nf(X)=(%2*X+%3)^%1 + %4 for X>=-%3/%2 \nf(X)=%4 for (X<-%3/%2)"))
                 .arg( (double)(*curve)[0] )
                 .arg( (double)(*curve)[1] )
                 .arg( (double)(*curve)[2] )
                 .arg( (double)(*curve)[3] ); break;
    case 3: d += QString(QObject::tr("Type:3 \nf(X)=(%2*X+%3)^%1 for X>=%5 \nf(X)=%4*X for (X<%5)"))
                 .arg( (double)(*curve)[0] )
                 .arg( (double)(*curve)[1] )
                 .arg( (double)(*curve)[2] )
                 .arg( (double)(*curve)[3] )
                 .arg( (double)(*curve)[4] ); break;
    case 4: d += QString(QObject::tr("Type:4 \nf(X)=(%2*X+%3)^%1+%6 for X>=%5 \nf(X)=%4*X+%7 for (X<%5)"))
                 .arg( (double)(*curve)[0] )
                 .arg( (double)(*curve)[1] )
                 .arg( (double)(*curve)[2] )
                 .arg( (double)(*curve)[3] )
                 .arg( (double)(*curve)[4] )
                 .arg( (double)(*curve)[5] )
                 .arg( (double)(*curve)[6] ); break;
    default: d = QString(QObject::tr("Parametric Curve Type: %1").arg(t));
    }
    obj["value"] = desc ? desc : d;
    obj["description"] = d;
    obj["value_array"] = array;
    obj["type"] = "pcurve";
    obj["curve_type"] = t;
}

std::string describeIccCurves( LPIccCurve * curves, int n, icColorSpaceSignature cs, QJsonArray & jCurves )
{
    std::string sDescription;
    for(int i = 0; i < n; ++i)
    {
        QJsonObject jCurve;
        icChar color[40];

        icColorIndexName(color, cs, i, n, "");
        if(color[0] == '_')
        {
            sprintf(color,"%d",i);
        }

        sDescription += color;
        sDescription += " ";

        CIccCurve * c_ = curves[i];
        CIccTagCurve * c = dynamic_cast<CIccTagCurve*>(c_);
        if(c)
        {
            describeIccCurve( c, color, jCurve );

            sDescription += QString::number(c->GetSize()).toUtf8().data();
            sDescription += " ";
        }
        CIccTagParametricCurve * p = dynamic_cast<CIccTagParametricCurve*>(c_);
        if(p)
        {
            describeIccPCurve( p, color, jCurve );

            sDescription += "T";
            sDescription += QString::number(p->GetFunctionType()).toUtf8().data();
            sDescription += " ";
        }

        jCurves.push_back(jCurve);
    }

    return sDescription;
}

std::string describeIccMpeCurves( CIccMpeCurveSet * curves, int n, icColorSpaceSignature cs, QJsonArray & jCurves )
{
    std::string sDescription;
    for(int i = 0; i < n; ++i)
    {
        QJsonObject jCurve;
        icChar color[40];

        icColorIndexName(color, cs, i, n, "");
        if(color[0] == '_')
        {
            sprintf(color,"%d",i);
        }

        sDescription += color;
        sDescription += " ";

        CIccTagCurve * c = dynamic_cast<CIccTagCurve*>(curves);
        if(c)
        {
            describeIccCurve( c, color, jCurve );

            sDescription += QString::number(c->GetSize()).toUtf8().data();
            sDescription += " ";
        }
        CIccTagParametricCurve * p = dynamic_cast<CIccTagParametricCurve*>(curves);
        if(p)
        {
            describeIccPCurve( p, color, jCurve );

            sDescription += "T";
            sDescription += QString::number(p->GetFunctionType()).toUtf8().data();
            sDescription += " ";
        }

        jCurves.push_back(jCurve);
    }

    return sDescription;
}

std::string describeIccCLut( CIccCLUT * lut, icColorSpaceSignature csInput, icColorSpaceSignature csOutput, bool bUseLegacy, QJsonObject & o )
{
    std::string sDescription;

    QString szName = QObject::tr("CLUT").toUtf8().data();

    int m_nInput = lut->GetInputDim(),
        m_nOutput = lut->GetOutputChannels();
    icUInt8Number //m_MaxGridPoint[16],
                  m_GridPoints[16];

    icChar szOutText[2048], szColor[40];
    int i;
    QString value;

    for(i = 0; i < m_nInput; ++i)
    {
        //m_MaxGridPoint[i] = lut->MaxGridPoint(i);
        m_GridPoints[i] = lut->GridPoint(i);
    }

    o["key"] = "clut";
    o["key_i18n"] = szName;
    o["value_in"] = m_nInput;
    o["value_out"] = m_nOutput;

    sprintf( szOutText, "<b>%s</b> <i>%d %d\n", szName.toUtf8().data(), m_nInput, m_nOutput );
    sDescription += szOutText;
    value = QString("%1 %2\n").arg(m_nInput).arg(m_nOutput);

    QJsonArray arr, arr1;
    for (i=0; i < m_nInput; i++) {
      icColorIndexName(szColor, csInput, i, m_nInput, "In");
      sprintf(szOutText, "%s%s %d", i?" ":"", szColor, m_GridPoints[i]);
      sDescription += szOutText;
      value += szOutText;
      //value += " ";
      //value += QString::number(m_GridPoints[i]).toUtf8().data();
      arr.push_back( m_GridPoints[i] );
      arr1.push_back( szColor );
    }
    o["values_gridpoints"] = arr;
    o["values_channels_names_in"] = arr1;

    sDescription += "\n";
    value += "\n";

    QJsonArray arr2;
    for (i=0; i<m_nOutput; i++) {
      icColorIndexName(szColor, csOutput, i, m_nOutput, "Out");
      sprintf(szOutText, "%s%s", i?" ":"", szColor);
      sDescription += szOutText;
      value += szOutText;
      arr2.push_back( szColor );
    }
    o["values_channels_names_out"] = arr2;

    sDescription += "</i>\n";
    value += "\n";

    QJsonArray arr3;
    for (i=0; i<m_nInput; i++) {
      icColorValue(szColor, 1.0, csInput, i, bUseLegacy);
      arr3.push_back( szColor );
    }
    o["values_max_in"] = arr3;
    QJsonArray arr4;
    for (i=0; i<m_nOutput; i++) {
      icColorValue(szColor, 1.0, csOutput, i, bUseLegacy);
      arr4.push_back( szColor );
    }
    o["values_max_out"] = arr4;

    int n = lut->NumPoints();
    o["count"] = n;

    QJsonArray arr5;
    QVariantList alist;
    alist.reserve(n*m_nOutput);
    for (i=0; i < n*m_nOutput; i++)
      alist.push_back( (double)(*lut)[i] );
    arr5 = QJsonArray::fromVariantList(alist);
    QJsonObject tmp;
    tmp["value_array"] = arr5;
    tmp["key"] = "clut";
    o["data"] = tmp;

    LOG( QString("clut[%1]: ").arg(arr5.count()) + value );
    o["value"] = value;

    return sDescription;
}

std::string describeIccMatrix( CIccMatrix * matrix, QJsonObject & obj )
{
    std::string sDescription;
    bool m_bUseConstants = matrix->m_bUseConstants;
    icFloatNumber * m_e = matrix->m_e;
    QString text;
    QJsonArray vector;
    int count = 9;

    if (!m_bUseConstants) {
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf\n",
        m_e[0], m_e[1], m_e[2]);
      sDescription += text.toStdString();
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf\n",
        m_e[3], m_e[4], m_e[5]);
      sDescription += text.toStdString();
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf\n",
        m_e[6], m_e[7], m_e[8]);
      sDescription += text.toStdString();
    }
    else {
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf  +  %8.4lf\n",
        m_e[0], m_e[1], m_e[2], m_e[9]);
      sDescription += text.toStdString();
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf  +  %8.4lf\n",
        m_e[3], m_e[4], m_e[5], m_e[10]);
      sDescription += text.toStdString();
      text = QString::asprintf("%8.4lf %8.4lf %8.4lf  +  %8.4lf\n",
        m_e[6], m_e[7], m_e[8], m_e[11]);
      sDescription += text.toStdString();

      count = 12;
    }

    for(int i = 0; i < count; ++i)
        vector.push_back(m_e[i]);

    obj["key"] = "matrix";
    obj["key_i18n"] = QObject::tr("Matrix");
    obj["value"] = sDescription.data();
    obj["value_array"] = vector;

    return sDescription;
}

void describeIccMBB( CIccMBB * mbb, QJsonObject & obj )
{
    int prec = mbb->GetPrecision();
    QJsonValue precJ(prec);
    obj["precision"] = precJ;

    std::string sDescription;
    QJsonArray array;
    QJsonObject tmp;

    if (mbb->IsInputMatrix()) {
      if (mbb->GetCurvesB() && !mbb->SwapMBCurves())
      {
        QJsonArray jCurves;
        LPIccCurve * curves = mbb->GetCurvesB();
        int n = mbb->InputChannels();
        icColorSpaceSignature cs = mbb->GetCsInput();
        tmp["key"] = "b_curves";
        tmp["key_i18n"] = QObject::tr("B-Curves");
        tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
        tmp["value_array"] = jCurves;
        tmp["type"] = "curves";
        array.push_back( tmp );

        sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
        sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetMatrix())
      {
          QJsonObject o;
          sDescription += "<b>" + QObject::tr("Matrix").toStdString() + "</b>\n<i>";
          sDescription += describeIccMatrix( mbb->GetMatrix(), o ) + "</i>";
          array.push_back( o );
      }

      if (mbb->GetCurvesM()) {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesM();
          int n = mbb->InputChannels();
          icColorSpaceSignature cs = mbb->GetCsInput();
          if (!mbb->SwapMBCurves())
          {
              tmp["key"] = "m_curves";
              tmp["key_i18n"] = QObject::tr("M-Curves");
          } else
          {
              tmp["key"] = "b_curves";
              tmp["key_i18n"] = QObject::tr("B-Curves");
          }
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetCLUT())
      {
        QJsonObject o;
        CIccCLUT * lut = mbb->GetCLUT();
        icColorSpaceSignature csInput  = mbb->GetCsInput();
        icColorSpaceSignature csOutput = mbb->GetCsOutput();
        bool bUseLegacy = mbb->GetType()==icSigLut16Type;

        sDescription += describeIccCLut( lut, csInput, csOutput, bUseLegacy, o );
        array.push_back( o );
      }

      if (mbb->GetCurvesA())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesA();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "a_curves";
          tmp["key_i18n"] = QObject::tr("A-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }
      std::string s;
      mbb->Describe(s);
    }
    else {
      if (mbb->GetCurvesA())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesA();
          int n = mbb->InputChannels();
          icColorSpaceSignature cs = mbb->GetCsInput();
          tmp["key"] = "a_curves";
          tmp["key_i18n"] = QObject::tr("A-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetCLUT())
      {
          QJsonObject o;
          CIccCLUT * lut = mbb->GetCLUT();
          icColorSpaceSignature csInput  = mbb->GetCsInput();
          icColorSpaceSignature csOutput = mbb->GetCsOutput();
          bool bUseLegacy = mbb->GetType()==icSigLut16Type;

          sDescription += describeIccCLut( lut, csInput, csOutput, bUseLegacy, o );
          array.push_back( o );
      }

      if (mbb->GetCurvesM() && mbb->GetType()!=icSigLut8Type)
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesM();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "m_curves";
          tmp["key_i18n"] = QObject::tr("M-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetMatrix())
      {
          QJsonObject o;
          sDescription += "<b>" + QObject::tr("Matrix").toStdString() + "</b>\n<i>";
          sDescription += describeIccMatrix( mbb->GetMatrix(), o ) + "</i>";
          array.push_back(o);
      }

      if (mbb->GetCurvesB())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesB();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "b_curves";
          tmp["key_i18n"] = QObject::tr("B-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }
    }

    obj["value"] = sDescription.data();
    obj["value_array"] = array;
}

void describeIccMpet( CIccTagMultiProcessElement * mpet, QJsonObject & obj )
{
    QString sDescription;
    QJsonArray array;
    QJsonObject tmp;

    int n = mpet->NumElements();
    obj["count"] = n;
    sDescription = QString(QObject::tr("Elements: %1")).arg(n);
    obj["value_in"] = mpet->NumInputChannels();
    obj["value_out"] = mpet->NumOutputChannels();

    for(int j = 0; j < n; ++j)
    {

        CIccMultiProcessElement * e = mpet->GetElement(j);

        icElemTypeSignature sig = e->GetType();
        CIccMpeCurveSet * curves = dynamic_cast<CIccMpeCurveSet*>(e);
        if(curves)
        {
            QJsonArray jCurves;
            tmp["key"] = "curves";
            tmp["key_i18n"] = QObject::tr("Curves");
            //tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
            tmp["value_array"] = jCurves;
            tmp["type"] = "curves";

            array.push_back( tmp );
        }
        CIccMpeCLUT * clut =  dynamic_cast<CIccMpeCLUT*>(e);
        if(clut)
        {
            tmp["key"] = "clut";
            tmp["key_i18n"] = QObject::tr("CLUT");
            //tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
            //tmp["value_array"] = jCurves;
            //tmp["type"] = "curves";

            array.push_back( tmp );

        }

        switch(sig)
        {
        case icSigCLutElemType: sDescription += QObject::tr("CLUT") + " "; break;
        case icSigCurveSetElemType: sDescription += QObject::tr("Curves") + " "; break;
        case icSigMatrixElemType: sDescription += QObject::tr("Matrix") + " "; break;
        }

        //        char * name = colr->GetEntry(j)->name;
//        array.push_back( name );
    }

    /*
    if (mbb->IsInputMatrix()) {
      if (mbb->GetCurvesB() && !mbb->SwapMBCurves())
      {
        QJsonArray jCurves;
        LPIccCurve * curves = mbb->GetCurvesB();
        int n = mbb->InputChannels();
        icColorSpaceSignature cs = mbb->GetCsInput();
        tmp["key"] = "b_curves";
        tmp["key_i18n"] = QObject::tr("B-Curves");
        tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
        tmp["value_array"] = jCurves;
        tmp["type"] = "curves";
        array.push_back( tmp );

        sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
        sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetMatrix())
      {
          QJsonObject o;
          sDescription += "<b>" + QObject::tr("Matrix").toStdString() + "</b>\n<i>";
          sDescription += describeIccMatrix( mbb->GetMatrix(), o ) + "</i>";
          array.push_back( o );
      }

      if (mbb->GetCurvesM()) {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesM();
          int n = mbb->InputChannels();
          icColorSpaceSignature cs = mbb->GetCsInput();
          if (!mbb->SwapMBCurves())
          {
              tmp["key"] = "m_curves";
              tmp["key_i18n"] = QObject::tr("M-Curves");
          } else
          {
              tmp["key"] = "b_curves";
              tmp["key_i18n"] = QObject::tr("B-Curves");
          }
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetCLUT())
      {
        QJsonObject o;
        CIccCLUT * lut = mbb->GetCLUT();
        icColorSpaceSignature csInput  = mbb->GetCsInput();
        icColorSpaceSignature csOutput = mbb->GetCsOutput();
        bool bUseLegacy = mbb->GetType()==icSigLut16Type;

        sDescription += describeIccCLut( lut, csInput, csOutput, bUseLegacy, o );
        array.push_back( o );
      }

      if (mbb->GetCurvesA())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesA();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "a_curves";
          tmp["key_i18n"] = QObject::tr("A-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }
      std::string s;
      mbb->Describe(s);
    }
    else {
      if (mbb->GetCurvesA())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesA();
          int n = mbb->InputChannels();
          icColorSpaceSignature cs = mbb->GetCsInput();
          tmp["key"] = "a_curves";
          tmp["key_i18n"] = QObject::tr("A-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetCLUT())
      {
          QJsonObject o;
          CIccCLUT * lut = mbb->GetCLUT();
          icColorSpaceSignature csInput  = mbb->GetCsInput();
          icColorSpaceSignature csOutput = mbb->GetCsOutput();
          bool bUseLegacy = mbb->GetType()==icSigLut16Type;

          sDescription += describeIccCLut( lut, csInput, csOutput, bUseLegacy, o );
          array.push_back( o );
      }

      if (mbb->GetCurvesM() && mbb->GetType()!=icSigLut8Type)
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesM();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "m_curves";
          tmp["key_i18n"] = QObject::tr("M-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }

      if (mbb->GetMatrix())
      {
          QJsonObject o;
          sDescription += "<b>" + QObject::tr("Matrix").toStdString() + "</b>\n<i>";
          sDescription += describeIccMatrix( mbb->GetMatrix(), o ) + "</i>";
          array.push_back(o);
      }

      if (mbb->GetCurvesB())
      {
          QJsonArray jCurves;
          LPIccCurve * curves = mbb->GetCurvesB();
          int n = mbb->OutputChannels();
          icColorSpaceSignature cs = mbb->GetCsOutput();
          tmp["key"] = "m_curves";
          tmp["key_i18n"] = QObject::tr("M-Curves");
          tmp["value"] = describeIccCurves( curves, n, cs, jCurves ).data();
          tmp["value_array"] = jCurves;
          tmp["type"] = "curves";
          array.push_back( tmp );

          sDescription += "<b>" + tmp["key_i18n"].toString().toStdString() + "</b>\n<i>";
          sDescription += std::string(tmp["value"].toString().toUtf8().data()) + "\n</i>";
      }
    }
*/

    obj["value"] = sDescription;
    obj["value_array"] = array;
}

void describeColorantTable( CIccTagColorantTable * colr, QString qtext, QJsonObject & obj )
{
    int n = colr->GetSize();
    QJsonValue count(n);
    obj["count"] = count;
    QJsonArray array;
    for(int j = 0; j < n; ++j)
    {
        char * name = colr->GetEntry(j)->name;
        array.push_back( name );
    }

    obj["value"] = qtext;
    obj["value_array"] = array;
}

void describeIccXYZ( CIccTagXYZ * XYZ, QJsonObject & obj )
{
    if(XYZ)
    {
        icXYZNumber * xyz = XYZ->GetXYZ(0);
        if(xyz)
        {
            QJsonValue X(icFtoD(xyz->X));
            obj["X"] = X;
            QJsonValue Y(icFtoD(xyz->Y));
            obj["Y"] = Y;
            QJsonValue Z(icFtoD(xyz->Z));
            obj["Z"] = Z;
            obj["value"] = QString("X=%1 Y=%2 Z=%3").arg(QString::number(icFtoD(xyz->X))).arg(QString::number(icFtoD(xyz->Y))).arg(QString::number(icFtoD(xyz->Z)));
        }
    }
}
void describeIccChrm( CIccTagChromaticity * chrm, QString & qtext, QJsonObject & obj )
{
    QString text;
    int j, count = chrm ->GetSize();
    QJsonArray arr;
    for(j = 0; j < count; ++j)
    {
        icChromaticityNumber * number = chrm->Getxy(j);
        QJsonObject xy;
        if(number)
        {
            QJsonValue x(icFtoD(number->x));
            xy["x"] = x;
            QJsonValue y(icFtoD(number->y));
            xy["y"] = y;
            text += QString("x=%1 y=%2").arg(QString::number(icFtoD(number->x))).arg(QString::number(icFtoD(number->y)));
            if(j + 1 < count)
                text += "\n";
        }
        arr.push_back(xy);
    }
    obj["value"] = text;
    obj["value2"] = qtext;
    obj["value_array"] = arr;
    obj["count"] = count;
    obj["subelements_compound"] = 1; // hide curve from tag elements list
}

void describeIccMluc( CIccTagMultiLocalizedUnicode * mluc, QJsonObject & obj )
{
    int n = mluc->m_Strings->size();
    QJsonValue count(n);
    obj["count"] = count;
    QJsonArray array;

    std::string sDescription;
    CIccMultiLocalizedUnicode * m_Strings = mluc->m_Strings;
    char szBuf[] = {0,0,0,0,0,0,0,0,0};
    CIccMultiLocalizedUnicode::iterator i;

    for (i=m_Strings->begin(); i!=m_Strings->end(); i++) {
      if (i!=m_Strings->begin())
        sDescription += "\n";

      char code[4] = {0,0,0,0};
      sprintf( code, "%c%c", i->m_nLanguageCode>>8, i->m_nLanguageCode);
      array.push_back( QJsonValue( (char*)code ) );
      sprintf( code, "%c%c", i->m_nCountryCode>>8, i->m_nCountryCode);
      array.push_back( QJsonValue( (char*)code ) );

      if(i->m_nCountryCode)
          sprintf(szBuf, "%c%c_%c%c: ",
                  i->m_nLanguageCode>>8, i->m_nLanguageCode,
                  i->m_nCountryCode>>8, i->m_nCountryCode);
      else
          sprintf(szBuf, "%c%c: ",
                  i->m_nLanguageCode>>8, i->m_nLanguageCode);

      sDescription += szBuf;

      const uint16_t * uni = i->GetBuf();
      QString desc; desc.resize(i->GetLength());
      unsigned int j;
      for(j = 0; j < i->GetLength(); ++j)
          desc[j] = uni[j];
      sDescription += desc.toUtf8().data();
      array.push_back( QJsonValue( desc ) );
    }

    obj["value"] = QString(sDescription.data());
    obj["value_array"] = array;
}

icUInt16Number
icValue (icUInt16Number val)
{
  icSwab16Ptr (&val);
  return val;
}
icUInt32Number
icValue (icUInt32Number val)
{
  icSwab32Ptr (&val);
  return val;
}

void describeIccUnknown( CIccTagUnknown * unkn, QString & qtext, QJsonObject & obj, icTagSignature tag_sig )
{
    icTagTypeSignature sig = unkn->GetType();
    if(unkn->GetSize())
    {
        const icUInt8Number * data_ = unkn->GetData();
        if(sig == 1986226036) // vcgt
        {
            //QJsonValue type("vcgt");
            obj["type"] = "curves";
            data_ = &data_[-4];
            int parametrisch        = icValue(*(icUInt32Number*) &data_[8]);
            QJsonValue param( parametrisch );
            obj["param"] = param;
            icUInt16Number nkurven  = icValue(*(icUInt16Number*) &data_[12]);
            icUInt16Number segmente = icValue(*(icUInt16Number*) &data_[14]);
            icUInt16Number byte     = icValue(*(icUInt16Number*) &data_[16]);

            if (parametrisch) { //icU16Fixed16Number
              double r_gamma = 1.0/icValue(*(icUInt32Number*)&data_[12])*65536.0;
              double start_r = icValue(*(icUInt32Number*)&data_[16])/65536.0;
              double ende_r = icValue(*(icUInt32Number*)&data_[20])/65536.0;
              double g_gamma = 1.0/icValue(*(icUInt32Number*)&data_[24])*65536.0;
              double start_g = icValue(*(icUInt32Number*)&data_[28])/65536.0;
              double ende_g = icValue(*(icUInt32Number*)&data_[32])/65536.0;
              double b_gamma = 1.0/icValue(*(icUInt32Number*)&data_[36])*65536.0;
              double start_b = icValue(*(icUInt32Number*)&data_[40])/65536.0;
              double ende_b = icValue(*(icUInt32Number*)&data_[44])/65536.0;
              QJsonArray curves;
              for (int j = 0; j < 3; j++)
              {
                QJsonObject tmp;
                QJsonArray params;
                tmp["key_i18n"] = QJsonValue(QObject::tr("Curve"));
                switch(j)
                {
                case 0: tmp["value"] = "RGB_R";
                    params.push_back(r_gamma);
                    params.push_back(start_r);
                    params.push_back(ende_r);
                    tmp["gamma"] = r_gamma;
                    tmp["start"] = start_r;
                    tmp["end"] = ende_r;
                    break;
                case 1: tmp["value"] = "RGB_G";
                    params.push_back(g_gamma);
                    params.push_back(start_g);
                    params.push_back(ende_g);
                    tmp["gamma"] = g_gamma;
                    tmp["start"] = start_g;
                    tmp["end"] = ende_g;
                    break;
                case 2: tmp["value"] = "RGB_B";
                    params.push_back(b_gamma);
                    params.push_back(start_b);
                    params.push_back(ende_b);
                    tmp["gamma"] = b_gamma;
                    tmp["start"] = start_b;
                    tmp["end"] = ende_b;
                    break;
                }
                tmp["type"] = "pcurve";
                tmp["curve_type"] = 5;
                tmp["value_array"] = params;
                tmp["count"] = params.count();
                curves.push_back (tmp);
              }
              obj["value_array"] = curves;
              QString d = QString(QObject::tr("Parametric Curves"));
              obj["value"] = QJsonValue(d);
              obj["subelements_compound"] = 1; // hide curve from tag elements list
            } else {
              int start = 18;
              double div   = 65536.0;
              QJsonArray curves;
              for (int j = 0; j < nkurven; j++)
              {
                QJsonObject tmp;
                QJsonArray curve;
                for (int i = segmente * j; i < segmente * (j+1); i++)
                {
                  curve.push_back( QJsonValue( ( (double) icValue (*(icUInt16Number*)&data_[start + byte*i])
                                   / div ) ) );
                }
                tmp["key_i18n"] = QJsonValue(QObject::tr("Curve"));
                switch(j)
                {
                case 0: tmp["value"] = "RGB_R"; break;
                case 1: tmp["value"] = "RGB_G"; break;
                case 2: tmp["value"] = "RGB_B"; break;
                }
                //tmp["description"] = QString(QObject::tr("Curve with %1 segments").arg(segmente));
                tmp["value_array"] = curve;
                tmp["type"] = QString("curve");
                tmp["count"] = QJsonValue(segmente);
                curves.push_back (tmp);
              }
              obj["value_array"] = curves;
              QString d = QString(QObject::tr("Curves with %1 segments").arg( segmente ));
              obj["value"] = QJsonValue(d);
              obj["subelements_compound"] = 1; // hide curve from tag elements list
            }
        } else if(sig == 0x74726C63) // clrt
        {
            unsigned int size = unkn->GetSize()+4;
            unsigned char * data = (unsigned char *)malloc(size);
            sprintf((char*)data,"clrt");
            memcpy(&data[4],unkn->GetData(),size-4);
            uint32_t count = (uint32_t)*(&data[8]),
                   * n = (uint32_t*)&data[8];
            *n = icValue(count);
            CIccMemIO mem;
            mem.Attach(data, size);
            CIccIO * io = dynamic_cast<CIccIO*>(&mem);
            CIccTagColorantTable colr;
            bool r = colr.Read(size,io);
            if(r)
            {
                describeColorantTable( &colr, qtext, obj );
                obj["type"] = "colorantTable";
            }
        } else
        {
            data_ = &data_[-4];
#ifdef OYRANOS_LIB
            size_t tag_size = unkn->GetSize() + 4;
            void * tag_data = malloc(tag_size);
            memcpy( tag_data, data_, tag_size );
            oyProfileTag_s * oy_tag = oyProfileTag_CreateFromData( tag_sig, sig, oyOK,
                                                      tag_size, tag_data, 0 );
            int32_t texts_n = 0;
            char ** texts = oyProfileTag_GetText( oy_tag, &texts_n, NULL,NULL,NULL, malloc );
            if(tag_data && tag_size)
                free(tag_data);
            oyProfileTag_Release( &oy_tag );
            obj["count"] = texts_n;
            if(texts_n && texts)
                qtext.clear();
            QJsonArray array;
            for(int j = 0; j < texts_n; ++j)
            {
                char * name = texts[j];
                array.push_back( name );
                if(j)
                {
                    if(j%2 == 0)
                        qtext += "\n";
                    else
                        qtext += " ";
                }
                qtext += name;
                if(name) free(name);
            }
            if(texts) free(texts);

            obj["key"] = oyICCTagTypeName(sig);
            obj["value"] = qtext;
            obj["value_array"] = array;
#endif
        }
    }
}

#ifdef OYRANOS_LIB
#define ieICCColorSpaceGetChannelCount oyICCColorSpaceGetChannelCount
QString ieICCColorSpaceGetChannelName( icColorSpaceSignature csp_, int pos_ )
{ return QString( oyICCColorSpaceGetChannelName(csp_, pos_, oyNAME_NICK) ); }
#else
int ieICCColorSpaceGetChannelCount( icColorSpaceSignature sig )
{
  int n;

  switch ((unsigned int)sig) {
    case icSigGrayData:
      n=1; break;
    case icSig2colorData: 
      n=2; break;
    case icSigXYZData:
    case icSigLabData:
    case icSigLuvData:
    case icSigYCbCrData:
    case icSigYxyData:
    case icSigRgbData:
    case icSigHsvData:
    case icSigHlsData:
    case icSigCmyData:
    case icSig3colorData:
      n=3; break;
    case icSigCmykData:
    case icSig4colorData:
      n=4; break;
    case icSig5colorData: n=5; break;
    case icSig6colorData: n=6; break;
    case icSig7colorData: n=7; break;
    case icSig8colorData: n=8; break;
    case icSig9colorData: n=9; break;
    case icSig10colorData: n=10; break;
    case icSig11colorData: n=11; break;
    case icSig12colorData: n=12; break;
    case icSig13colorData: n=13; break;
    case icSig14colorData: n=14; break;
    case icSig15colorData: n=15; break;
    default:
      n=0; break;
  }
  return n;
}
QString ieICCColorSpaceGetChannelName( icColorSpaceSignature csp_, int pos_ )
{ return QString::number(  pos_ ); }
#endif

void describeIccNcl2( CIccTagNamedColor2 * ncl2, QJsonObject & obj )
{
    int count = ncl2->GetSize();
    const char * prefix = ncl2->GetPrefix();
    const char * suffix = ncl2->GetSufix();
    if(prefix)
        obj["prefix"] = prefix;
    if(suffix)
        obj["suffix"] = suffix;
    obj["count"] = count;

    icColorSpaceSignature csp = ncl2->GetDeviceSpace();
    obj["device_space"] = ieICCColorSpaceGetName( csp );
    int device_channel_count = ieICCColorSpaceGetChannelCount( csp );
    obj["device_cannel_count"] = device_channel_count;
    QJsonArray dcarr;
    for(int j = 0; j < device_channel_count; ++j)
        dcarr.push_back( ieICCColorSpaceGetChannelName(csp, j) );
    obj["device_cannel_names"] = dcarr;

    int round = 10000;
    QJsonArray arr;
    for(int i = 0; i < count; ++i)
    {
        std::string sColorName;
        QJsonObject nc;
        ncl2->GetColorName( sColorName, i);
        QString qname (sColorName.c_str());
        nc["key_i18n"] = QJsonValue( qname );
        SIccNamedColorEntry * e = ncl2->GetEntry(i);
        nc["L"] = ((int)(e->pcsCoords[0]*round+0.5))/(double)round;
        nc["a"] = ((int)(e->pcsCoords[1]*round+0.5))/(double)round;
        nc["b"] = ((int)(e->pcsCoords[2]*round+0.5))/(double)round;

        QString value = "L=" + QString::number(((int)(e->pcsCoords[0]*round+0.5))/(double)round) + " a=" + QString::number(((int)(e->pcsCoords[1]*round+0.5))/(double)round) + " b=" + QString::number(((int)(e->pcsCoords[2]*round+0.5))/(double)round) + "\n";
        for(int j = 0; j < device_channel_count; ++j)
        {
            value += dcarr[j].toString() + "=" + QString::number(((int)(e->deviceCoords[j]*round+0.5))/(double)round) + " ";
            nc["device_" + dcarr[j].toString()] = ((int)(e->deviceCoords[j]*round+0.5))/(double)round;
        }
        nc["value"] = value;
        arr.push_back( nc );
    }
    obj["value_array"] = arr;
    //obj["subelements_compound"] = 1; // hide curve from tag elements list
}

void addJObject( QJsonObject & j,
                 const QString & key,
                 const QString & i18nKey,
                 const QString & value )
{
    QJsonObject json;
    if(i18nKey.length())
        json["key_i18n"] = i18nKey;
    if(value.length())
        json["value"] = value;
    j[key] = json;
}

void  addJArray( QJsonArray & j,
                 const QString & key,
                 const QString & i18nKey,
                 const QString & value,
                 const QString & tagType,
                 int offset = 0,
                 int size = 0 )
{
    QJsonObject json;
    QLocale loc;
    if(key.length())
        json["key"] = key;
    if(i18nKey.length())
        json["key_i18n"] = i18nKey;
    if(value.length())
        json["value"] = value;
    if(offset)
        json["offset"] = offset;
    if(size)
        json["size"] = size;
    if(tagType.length())
        json["type"] = tagType;
    j.append( json );
}

QString IccProfile::getSaturationLine()
{
#ifdef OYRANOS_LIB
    if(!m_p) load_();

    QJsonObject json;
    oyProfile_s * profile  = oyProfile_Copy( m_p, NULL ),
                * outspace = oyProfile_FromStd( oyASSUMED_XYZ, 0, NULL );

    int size,
        precision = 20,
        intent = 3;
      oyOption_s * o = NULL;
      oyOptions_s * opts = oyOptions_New(0),
                  * result = NULL;
      oyOptions_MoveInStruct( &opts, OY_TOP_SHARED OY_SLASH OY_DOMAIN_INTERNAL OY_SLASH OY_TYPE_STD OY_SLASH "icc_profile.input",
                              (oyStruct_s**) &profile,
                              OY_CREATE_NEW );
      oyOptions_MoveInStruct( &opts, OY_TOP_SHARED OY_SLASH OY_DOMAIN_INTERNAL OY_SLASH OY_TYPE_STD OY_SLASH "icc_profile.output",
                              (oyStruct_s**) &outspace,
                              OY_CREATE_NEW );
      oyOptions_SetFromInt( &opts,   OY_TOP_SHARED OY_SLASH OY_DOMAIN_INTERNAL OY_SLASH OY_TYPE_STD OY_SLASH "rendering_intent",
                              intent, 0, OY_CREATE_NEW );
      oyOptions_SetFromInt( &opts,   OY_TOP_SHARED OY_SLASH OY_DOMAIN_INTERNAL OY_SLASH OY_TYPE_STD OY_SLASH "precision",
                              precision, 0, OY_CREATE_NEW );

      oyOptions_Handle( "//" OY_TYPE_STD "/graph2d.saturation_line",
                        opts,"saturation_line",
                        &result );

      o = oyOptions_Find( result, "saturation_line.output.double",
                                           oyNAME_PATTERN );

      size = (int) oyOption_GetValueDouble( o, -1 );
      if(size > 1)
      {
        int i,j;
        QVariantList alist;
        alist.reserve(size);
        for(i = 0; i < size; i+=3)
        {
            QJsonArray xyz;
            for(j = 0; j < 3; ++j)
                xyz.push_back( oyOption_GetValueDouble( o, i+j ) );
            alist.push_back( xyz );
        }

        QJsonArray arr;
        arr = QJsonArray::fromVariantList(alist);
        QJsonObject tmp;
        tmp["value_array"] = arr;
        QJsonValue jkey("saturation_line");
        tmp["key"] = jkey;
        QJsonValue jsize(size/3);
        tmp["count"] = jsize;
        json["data"] = tmp;

      } else
        fprintf( stderr, "saturation_line contains no lines: %d\n", size );

    return QString(QJsonDocument(json).toJson(QJsonDocument::Indented));
#endif
    return QString("{}");
}

QString IccProfile::getJSON()
{
    if(!m_p) load_();

    QJsonObject json;

#ifdef OYRANOS_LIB
    if(m_p == NULL)
    {
        json["error"] = QString(tr("Nothing loaded")) + " " + m_file_name;
        return QString(QJsonDocument(json).toJson(QJsonDocument::Indented));
    }
#endif

    QLocale loc;
    addJObject(json, "prefix", "", "FILE_,LOCALE_,HEADER_,TAG_" );
    addJObject(json, "LOCALE_info", "", loc.bcp47Name() );

    QString fn;
    const char * cfn = myProfile_GetFileName(m_p);
    if(cfn)
        fn = cfn;
    else
        fn = m_file_name;
    int size = myProfile_GetSize(m_p);
    if(!size)
    {
      QFileInfo f( fn );
      size = f.size();
      fprintf( stderr, "%s(%s:%d) fn: %s %d\n", __func__,strrchr(__FILE__,'/')+1,__LINE__, fn.toLocal8Bit().data(), size );
    }
    // naive file attempt
    addJObject(json, "PROFILE_name", tr("Name"), myProfile_GetDesc(m_p) );
    addJObject(json, "FILE_name", tr("File Name"), fn );
    addJObject(json, "FILE_size", tr("Size"), QString::number(size) );

    // display profile internals
    CIccProfile * p;
    if(m_bytes.length())
        p = OpenIccProfile( (const icUInt8Number*) m_bytes.data(), m_bytes.length() );
    else
        p = OpenIccProfile( fn.toLocal8Bit().data() );

    if(!p)
        addJObject(json, "error", tr("Could not load file:") + m_file_name, "");
    else {
        CIccTag * tag = 0;
        CIccInfo info;
        std::string text;

        addJObject(json, "HEADER_name", tr("Header"), "");
        QJsonArray jh;

        icHeader * header = &p->m_Header;
        char buf[64];
        addJArray(jh, "size", tr("Size"), QString::number( header->size ), "" );
        addJArray(jh, "cmm", tr("CMM"), ieICCCmmDescription((icCmmSignature)(header->cmmId)), "" );
        addJArray(jh, "version", tr("Version"), info.GetVersionName( header->version ), "" );
        QString cl(info.GetProfileClassSigName( header->deviceClass ));
        cl.remove("Class");
        static const char* class_string[] OY_UNUSED = {
            QT_TR_NOOP("Output"),
            QT_TR_NOOP("Display"),
            QT_TR_NOOP("Link"),
            QT_TR_NOOP("Abstract"),
            QT_TR_NOOP("Color Space"),
            QT_TR_NOOP("Named Color"),
            QT_TR_NOOP("Input"),

            QT_TR_NOOP("Perceptual"),
            QT_TR_NOOP("Saturation"),
            QT_TR_NOOP("Absolute Colorimetric"),
            QT_TR_NOOP("Relative Colorimetric"),
            QT_TR_NOOP("Reflective | Glossy"),
            QT_TR_NOOP("EmbeddedProfileFalse | UseAnywhere"),
            QT_TR_NOOP("undefined"),
            QT_TR_NOOP("")
             };
        addJArray(jh, "class", tr("Class"), tr( cl.toLocal8Bit().data() ), "" );
        addJArray(jh, "color_space", tr("Color Space"), ieICCColorSpaceGetName(header->colorSpace), "" );
        addJArray(jh, "pcs", tr("PCS Color Space"), ieICCColorSpaceGetName(header->pcs), "" );
        addJArray(jh, "date", tr("Date"), QString::number(header->date.year) + "-" + QString::number(header->date.month) + "-" + QString::number(header->date.day) + " " + QString::number(header->date.hours) + ":" + QString::number(header->date.minutes) + ":" + QString::number(header->date.seconds), "" );
        char * magic = (char*)&header->magic;
        if(!magic)
        {
            json.insert( "HEADER_data", jh );
            goto finish_getJSON;
        }
        addJArray(jh, "signature", tr("Signature"), QString(magic[3]) + magic[2] + magic[1] + magic[0], "" );
        addJArray(jh, "platform", tr("Platform"), ieICCPlatformDescription( header->platform ), "" );
        addJArray(jh, "options", tr("Options"), tr(info.GetProfileFlagsName( header->flags )), "" );
        addJArray(jh, "manufacturer", tr("Manufacturer"), icGetSig( buf, header->manufacturer ), "" );
        addJArray(jh, "model", tr("Model"), icGetSig( buf, header->model ), "" );
        addJArray(jh, "creator", tr("Creator"),  icGetSig( buf, header->creator ), "" );
        addJArray(jh, "attributes", tr("Attributes"), tr(info.GetDeviceAttrName( header->attributes )), "" );
        addJArray(jh, "rendering_intent", tr("Rendering Intent"), tr(info.GetRenderingIntentName( (icRenderingIntent) header->renderingIntent )), "" );
        addJArray(jh, "illuminant", tr("Illuminant"), QString::number(header->illuminant.X/65535.0) + " "  + QString::number(header->illuminant.Y/65535.0) + " "  + QString::number(header->illuminant.Z/65535.0), "" );
        addJArray(jh, "profile_id", tr("Profile ID"), info.GetProfileID( &header->profileID ), "" );
        json.insert("HEADER_data", jh );


        addJObject(json, "TAG_name", tr("Tags"), "");
        QJsonArray tags;
        TagEntryList::iterator i;
        int pos = 0;
        for (i = p->m_Tags->begin(); i != p->m_Tags->end(); ++i)
        {
            tag = p->FindTag(i->TagInfo.sig);
            text.clear();
            if(tag)
              tag->Describe(text);
            else
            {
                json.insert("TAG_data", tags);
                goto finish_getJSON;
            }
            std::string zero(1,'\000');
            ReplaceStringInPlace( text, zero, "" );
            QString qtext(text.c_str());
            if(qtext.indexOf("BEGIN_LUT") >= 0)
                qtext.clear();
            if(qtext.size() > 0)
            {
                const QChar c = qtext[qtext.size()-1];
                if(c == '\n')
                    qtext.remove(qtext.size()-1,1);
                const QChar c2 = qtext[qtext.size()-1];
                if(c2 == '\r')
                    qtext.remove(qtext.size()-1,1);
            }
            qtext.remove('\r');
            //qtext.replace("\t"," ");
            //qtext.replace("\n","<br />\n");
            QString sig( info.GetTagSigName( i->TagInfo.sig ) );
            QString type( info.GetTagTypeSigName( tag->GetType() ) );
            sig.remove("Tag"); // remove repeating string element
            type.remove("Type"); // remove repeating string element
            addJArray(tags, sig, ieICCTagDescription(i->TagInfo.sig), qtext,
                      type, i->TagInfo.offset, i->TagInfo.size );

            CIccTagXYZ * XYZ = dynamic_cast<CIccTagXYZ*>(tag);
            if(XYZ)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccXYZ( XYZ, obj );
                tags.replace( pos, obj );
            }

            CIccTagChromaticity * chrm = dynamic_cast<CIccTagChromaticity*>(tag);
            if(chrm)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccChrm( chrm, qtext, obj );
                tags.replace( pos, obj );
            }

            CIccTagParametricCurve * pcurve = dynamic_cast<CIccTagParametricCurve*>(tag);
            if(pcurve)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccPCurve( pcurve, NULL, obj );
                tags.replace( pos, obj );
            }
            CIccTagCurve * curve = dynamic_cast<CIccTagCurve*>(tag);
            if(curve)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccCurve( curve, NULL, obj );
                tags.replace( pos, obj );
            }

            CIccMBB * mbb = dynamic_cast<CIccMBB*>(tag);
            if(mbb)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccMBB( mbb, obj );
                tags.replace( pos, obj );
            }

            CIccTagMultiProcessElement * mpet = dynamic_cast<CIccTagMultiProcessElement *>(tag);
            if(mpet)
            {
                printf("found mpe: %d\n", pos);
                QJsonObject obj = tags.at(pos).toObject();
                describeIccMpet( mpet, obj );
                tags.replace( pos, obj );
            }

            CIccTagColorantTable * colr = dynamic_cast<CIccTagColorantTable*>(tag);
            if(colr)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeColorantTable( colr, qtext, obj );
                tags.replace( pos, obj );
            }

            if(tag->GetType() == icSigTextType ||
               tag->GetType() == icSigTextDescriptionType ||
               tag->GetType() == icSigUtf8TextType ||
               tag->GetType() == icSigZipUtf8TextType ||
               tag->GetType() == icSigZipXmlType ||
               tag->GetType() == icSigUtf16TextType)
            {
              std::string sDescription;
              icGetTagText( tag, sDescription );
              QJsonObject obj = tags.at(pos).toObject();
              obj["value"] = QString(sDescription.data());
              tags.replace( pos, obj );
            }

            CIccTagUnknown * unkn = dynamic_cast<CIccTagUnknown*>(tag);
            if(unkn)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccUnknown(unkn, qtext, obj, i->TagInfo.sig);
                tags.replace( pos, obj );
            }

            CIccTagNamedColor2 * ncl2 = dynamic_cast<CIccTagNamedColor2*>(tag);
            if(ncl2)
            {
                QJsonObject obj = tags.at(pos).toObject();
                describeIccNcl2(ncl2, obj);
                tags.replace( pos, obj );
            }
            ++pos;
        }
        delete p;
        json.insert("TAG_data", tags);
    }

    finish_getJSON:
    LOG("finished loading: " + fn);
    return QString(QJsonDocument(json).toJson(QJsonDocument::Indented));
}

void IccProfile::clearProfile()
{
    myProfile_Release( &m_p );
}

QByteArray readFile(QString url)
{
    QFile f;
    const char * fn = url.toLocal8Bit().data();
    fprintf( stderr, "%s(%s:%d) url.toLocal8Bit().data(): %s\n", __func__,strrchr(__FILE__,'/')+1,__LINE__, fn );
    if(url == "-")
    {
        f.open(stdin, QIODevice::ReadOnly);
        LOG(QString("Load: %1").arg(fn));
    }
    else
    {
        QUrl qurl(url);
        const char * file_name = qurl.toLocalFile().toLocal8Bit().data();
        LOG(QString("Load: %1").arg(fn));
        f.setFileName( file_name );
        f.open(QIODevice::ReadOnly|QIODevice::Unbuffered);
    }
    QByteArray a;
    if(f.isOpen())
    {
        a = f.readAll();
        f.close();
    }
    return a;
}

void IccProfile::load_()
{
    QUrl url(m_file_name);
    QFileInfo f( url.toLocalFile() );
    if(m_p)
        myProfile_Release( &m_p );

    if(f.exists() == false && m_bytes.length() <= 0)
    {
        int found = 0;
        LOG( QString("not an Url ") + url.toLocalFile() );
        f.setFile( m_file_name );
        int exist = f.exists();
        if(f.exists() == false && m_bytes.length() <= 0)
        {
#ifdef OYRANOS_LIB
            m_p = oyProfile_FromName(m_file_name.toLocal8Bit().data(),0,NULL);
            if(m_p)
            {
                const char * pn = myProfile_GetFileName(m_p);
                if(pn)
                {
                    f.setFile(QString(pn));
                    if( f.exists() == true )
                        found = 1;
                }
            }

            if(found == 0)
            {
                LOG( QString("File does not exist on disk ??? ") + m_file_name );
            }
#endif
        }
    }

#ifndef OYRANOS_LIB
    if(m_bytes.length() <= 0)
      m_bytes = readFile(m_file_name);
#endif

    QString fn = f.fileName();
    QString full_name( url.toLocalFile() );
    int s = full_name.count();
    LOG(QString("start loading: ") + (s?full_name:fn));

    if(m_bytes.length())
    {
        fn = m_file_name;
        full_name = m_file_name;
    }

#ifdef OYRANOS_LIB
    if(!m_p && m_bytes.length())
        m_p = oyProfile_FromMem( m_bytes.length(), m_bytes.data(), 0, NULL );
    else
      if(m_p && !m_bytes.length())
    {
        size_t size = 0;
        void * b = oyProfile_GetMem( m_p, &size, 0, malloc );
        QByteArray qb( (const char *)b, (int)size );
        m_bytes = qb;
        if(b && size)
            free(b);
    }
    else if(!m_p)
    {
        if(f.canonicalFilePath().length())
            fn = f.canonicalFilePath();
        else
            fn = m_file_name;
        m_p = oyProfile_FromName( fn.toLocal8Bit().data(), 0, NULL );
    }
#endif
}

QString IccProfile::getLibDescription(int type)
{
    switch(type)
    {
    case 0:
        return QString(ICCPROFLIBVER);
    case 1:
        return QString(tr("%1 Version")).arg(ICC_LIB_NAME);
    case 2:
        return QString(tr("%1 Version")).arg("Oyranos");
    case 3:
        return QString(OYRANOS_VERSION_NAME);
    case 4:
    {
        const char * v = oyVersionString(1);
        QString qv(v);
        return qv;
    }
    }
    return QString("no description found for type ") + QString::number(type);
}

#ifdef OYRANOS_LIB
extern "C" {
int  oyColorConvert_ ( oyProfile_s       * p_in,
                        oyProfile_s       * p_out,
                        oyPointer           buf_in,
                        oyPointer           buf_out,
                        oyDATATYPE_e        buf_type_in,
                        oyDATATYPE_e        buf_type_out,
                        oyOptions_s       * options,
                        int                 count );
}
#endif

QVariantList IccProfile::getAbsoluteXYZ(QVariantList rgb)
{
    QVariantList absXYZ;
#ifdef OYRANOS_LIB
    double r[3], buf[3]; int i;
    for(i = 0; i < 3; ++i)
        r[i]= rgb.at(i).toDouble();

    oyOptions_s * options = NULL;
    /* default to absolute colorimetric  */
    oyOptions_SetFromString( &options, OY_BEHAVIOUR_STD "/rendering_intent",
                           "3", OY_CREATE_NEW );
    /* absolute intent in lcms2 is always full adapted to D50 */
    oyOptions_SetFromString( &options, OY_BEHAVIOUR_STD "/adaption_state",
                           "0.0", OY_CREATE_NEW );
    oyProfile_s * p_out = oyProfile_FromStd( oyEDITING_XYZ, 0, NULL );
    oyColorConvert_( m_p, p_out, r, buf, oyDOUBLE, oyDOUBLE, options, 1);
    for(i = 0; i < 3; ++i)
        absXYZ.append(buf[i]);

    oyProfile_Release( &p_out );
    oyOptions_Release( &options );
#endif
    return absXYZ;
}

QVariantList IccProfile::getColor(QVariantList lab, QString profile_name)
{
    QVariantList color;
#ifdef OYRANOS_LIB
    double r[3], buf[3]; int i;
    for(i = 0; i < 3; ++i)
        r[i]= lab.at(i).toDouble();

    oyOptions_s * options = NULL;
    /* default to absolute colorimetric  */
    oyOptions_SetFromString( &options, OY_BEHAVIOUR_STD "/rendering_intent",
                           "0", OY_CREATE_NEW );
    /* absolute intent in lcms2 is always full adapted to D50 */
    oyOptions_SetFromString( &options, OY_BEHAVIOUR_STD "/adaption_state",
                           "0.0", OY_CREATE_NEW );
    oyProfile_s * p_in = oyProfile_FromStd( oyEDITING_LAB, 0, NULL );
    oyProfile_s * p_out = oyProfile_FromName( profile_name.toLocal8Bit().data(), 0, NULL );
    oyColorConvert_( p_in, p_out, r, buf, oyDOUBLE, oyDOUBLE, options, 1);
    for(i = 0; i < 3; ++i)
        color.append( buf[i] );

    oyProfile_Release( &p_in );
    oyProfile_Release( &p_out );
    oyOptions_Release( &options );
#endif
    return color;
}

QString IccProfile::getProfileList()
{
    QJsonObject json;
#ifdef OYRANOS_LIB
    QJsonArray arr;

    oyProfiles_s * ps = oyProfiles_Create( NULL, 0, 0 );
    int count = oyProfiles_Count(ps), i;
    for(i = 0; i < (int)count; ++i)
    {
      oyProfile_s * p = oyProfiles_Get( ps, i );

      const char * sfn = myProfile_GetFileName(p);
      const char * desc = myProfile_GetDesc(p);
      if(!sfn)
          icc_examin_log(__FILE__,__func__, QString("oyProfile_GetFileName() failed"));
      if(!desc)
          icc_examin_log(__FILE__,__func__, QString("oyProfile_GetText() failed"));
      QString t;
      t =  QString(desc) + " (" + sfn + ")";
      QJsonObject jp;
      QJsonValue jdesc(desc);
      jp["desc"] = jdesc;
      QJsonValue jsfn(sfn);
      jp["file_name"] = jsfn;
      QJsonValue jt(t);
      jp["text"] = jt;
      arr.append( jp );

      oyProfile_Release( &p );
    }
    oyProfiles_Release( &ps );

    QJsonObject tmp;
    tmp["value_array"] = arr;
    tmp["key"] = "profiles";
    tmp["count"] = count;
    json["data"] = tmp;
#endif

    return QString(QJsonDocument(json).toJson(QJsonDocument::Indented));
}
