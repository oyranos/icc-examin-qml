/** @file IccFileDialog.qml
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/10/09
 *
 *  Custom FileDialog
 */

import QtQuick 2.0
import QtQuick.Dialogs 1.2

FileDialog {
    id: fileDialog
    width: {
        if( fullscreen  )
            mainWindow.width
        else
            50*dens
    }
    height: {
        if( fullscreen )
            mainWindow.height
        else
            35*dens
    }
    nameFilters: ["ICC files (*.icc *.icm)", "Text files (*.txt)", "HTML files (*.html)", "All files (*.*)"]
    onAccepted: {
        loadFile( fileUrl, false )
    }
}
