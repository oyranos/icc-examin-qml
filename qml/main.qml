/** @file main.qml
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/10/09
 *
 *  QML ApplicationWindow mainWindow
 */

// developed with Qt 5.7-5.9

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
//import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2
import IccProfile 1.0
import IccDownload 1.0
import "qrc:/qml"

ApplicationWindow {
    id: mainWindow
    visible: true
    readonly property bool fullscreen: (Qt.platform.os === "android" ||
                                        Qt.platform.os === "blackberry" ||
                                        Qt.platform.os === "ios" ||
                                        Qt.platform.os === "tvos" ||
                                        Qt.platform.os === "windowsphone")
    width: if( !fullscreen )
               200*dens
    height: if( !fullscreen )
                150*dens
    minimumWidth: 100*dens
    minimumHeight: 75*dens

    readonly property int dens: Math.round(Screen.logicalPixelDensity)
    readonly property string dpiName: {
//        if(density >= 21.26) // 560
//            "xxxxhdpi"
//        else if(density >= 14.173)  // 360
//            "xxxhdpi"
//        else
        if ( Screen.pixelDensity >= 10.630 ) // 270
            "xxhdpi"
        else if ( Screen.pixelDensity >= 7.0866 ) // 180
            "xhdpi"
        else if ( Screen.pixelDensity >= 5.3150 ) // 135
            "hdpi"
        else
            "mdpi"
    }

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    // DEBUG
    property int icc_debug: 0

    Screen.onPrimaryOrientationChanged: {
        if(icc_debug)
            statusText = "oPrimOrientCh(): old index: " + pages.currentIndex + "/" + pagesContentIndex + " landscape: " + landscape
        //statusText = screenOrientationString(Screen.primaryOrientation)

    }

    function screenOrientationString(i) {
        switch (i) {
        case Qt.PrimaryOrientation:
            return qsTr("Primary")
        case Qt.LandscapeOrientation:
            return qsTr("Landscape")
        case Qt.PortraitOrientation:
            return qsTr("Portrait")
        case Qt.InvertedLandscapeOrientation:
            return qsTr("Inverted Landscape")
        case Qt.InvertedPortraitOrientation:
            return qsTr("Inverted Portrait")
        }
        return "unknown"
    }


    IccProfile {
        id: profile
        onFileNameChanged: {
            statusText = qsTr("Obtained new fileName:") + " " + profile.fileName
            redXYZ = [0,0,0]
            greenXYZ = [0,0,0]
            blueXYZ = [0,0,0]
            whiteXYZ = [0,0,0]
            blackXYZ = [0,0,0]
            profileTagView.html = ""
            saturationLine = profile.getSaturationLine()
            profileJsonString = profile.getJSON()
            if(icc_debug)
                statusText = "Obtained JSON" + " \"" + profile.fileName + "\""
            if(icc_debug)
                textArea2.text = profileJsonString
            if(icc_debug)
                statusText = "Loaded JSON into textArea1"
            tagListView.load( profileJsonString )
            if(icc_debug)
                statusText = "Loaded JSON into tagListView"
            var f = urledit.find(profile.fileName)
            if(icc_debug)
                statusText = "f:" + f
            if(f === -1)
            {
                urledit.ignore = true
                urledit.model.insert(0, {text: profile.fileName} )
                if(indexSelected === false)
                    urledit.currentIndex = 0
                else
                    ++urledit.currentIndex
                urledit.ignore = false
            } else
            {
                if(indexSelected === false)
                    urledit.currentIndex = f
            }
            unsetBusyTimer.start()
            statusText = qsTr("Loaded") + " " + profile.fileName
        }
        onQuitMessage: {
            statusText = profile.quitMsg
            // show a text field? without calling a qml::Dialog!
        }
    }

    // Take file load out of UI thread
    property string fileUrl
    Timer {
        id: loadTimer
        interval: 100
        onTriggered: profile.fileName = fileUrl
    }

    property bool indexSelected: false
    function loadFile( fileUrlVar, index_selected )
    {
        statusText = ""
        setBusyTimer.start()

        indexSelected = index_selected
        fileUrl = fileUrlVar
        // move file loading into Timer's own thread
        loadTimer.start()
    }

    IccFileDialog { id: fileDialog }

    Action {
        id: fileOpenAction
        text: qsTr("Open")
        shortcut: "Ctrl+O"
        iconName: "document-open"
        onTriggered: fileDialog.open()
    }
    Action {
        id: copyAction
        text: qsTr("Copy")
        shortcut: "Ctrl+C"
        iconSource: "qrc:/images/" + dpiName + "/ic_action_copy.png"
        iconName: "edit-copy"
        enabled: (!!activeFocusItem && !!activeFocusItem["copy"])
        onTriggered: activeFocusItem.copy()
    }
    Action {
        id: pasteAction
        text: qsTr("Paste")
        shortcut: "ctrl+v"
        iconSource: "qrc:/images/" + dpiName + "/ic_action_paste.png"
        iconName: "edit-paste"
        enabled: (!!activeFocusItem && !!activeFocusItem["paste"])
        onTriggered: activeFocusItem.paste()
    }
    Action {
        id: helpAction
        text: qsTr("About...")
        shortcut: "F1"
        iconSource: "qrc:/images/" + dpiName + "/ic_action_about.png"
        iconName: "about"
        onTriggered: pages.currentIndex = 3
    }

    Action {
        id: labAction
        text: qsTr("CIE*xy")
        onTriggered: {
            profileTagView.projection_xyz = !profileTagView.projection_xyz
            profileTagView.repaint = !profileTagView.repaint
            if( profileTagView.projection_xyz )
                text = qsTr("CIE*Lab")
            else
                text = qsTr("CIE*xy")
        }
    }
    Action {
        id: screenInfoAction
        text: qsTr("Screen Infos ...")
        shortcut: "Ctrl+I"
        onTriggered: {
            infoText =
            "<html><body><p align=\"center\"><table border=\"0\" style=\"border-spacing:10px\">"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Name") + ":</td><td style=\"font-weight:bold;\">" + Screen.name + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Dimensions") + ":</td><td>" + Screen.width + "x" + Screen.height + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Pixel Density") + ":</td><td>" + Screen.pixelDensity.toFixed(2) + " " + qsTr("dots/mm") + " " + (Screen.pixelDensity * 25.4).toFixed(2) + " " + qsTr("dots/inch") + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Logical Pixel Density") + ":</td><td>" + Screen.logicalPixelDensity.toFixed(2) + " " + qsTr("dots/mm") + " " + (Screen.logicalPixelDensity * 25.4).toFixed(2) + " " + qsTr("dots/inch") + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Device Pixel Ratio") + ":</td><td>" + Screen.devicePixelRatio.toFixed(2) + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Orientation") + ":</td><td>" + screenOrientationString(Screen.primaryOrientation) + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Virtual Desktop Dimensions") + ":</td><td>" + Screen.desktopAvailableWidth  + "x" + Screen.desktopAvailableHeight + "</td></tr>"
                            + "<tr><td align=\"right\" style=\"padding-right:10;\">" + qsTr("Platform") + ":</td><td>" + Qt.platform.os + "</td></tr>"
                            + "</table></p></body></html>"
            showJson = false
            setPage(2)
        }
        // screenInfoBox.open()
    }
    Action {
        id: downloadAction
        text: qsTr("Download")
        onTriggered: {
            if(icc_debug)
                statusText = "Should do \"" + urledit.text + "\""
            download.url = urledit.text
        }
    }
    Action {
        id: quitAction
        text: qsTr("Quit")
        shortcut: "Ctrl+q"
        onTriggered: {
            statusText = qsTr("Quit")
            Qt.quit()
        }
    }

    menuBar: MenuBar {
        Menu {
            iconSource: "qrc:/images/" + dpiName + "/ic_action_menu.png"
            title: qsTr("&File ...")
            MenuItem { action: fileOpenAction }
            MenuItem { action: copyAction }
            MenuItem { action: pasteAction }
            MenuItem { action: helpAction }
            MenuItem { action: labAction }
            MenuItem { action: screenInfoAction }
            MenuItem { action: quitAction }
        }
    }

    toolBar: ToolBar {
        id: mainToolBar
        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: pageLeft
                enabled: pages.currentIndex > 0 ? 1 : 0
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: enabled ? 2 : 0
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: enabled ? "❮" : ""
                        color: myPalette.windowText
                    }
                }
                onClicked: {
                    if(pages.currentIndex > 0)
                        setPage(pages.currentIndex - 1)
                    else
                        setPage( 0 )
                }
            }
            Item {
                width: 1
                height: pageLeft.height * 0.7
                Rectangle {
                    height: parent.height
                    width: height
                    color: "#55888888"
                    radius: height*0.5 // make a circle
                    transform: Scale { xScale: 0.1 } // transform into elipse
                }
            }
            ToolButton {
                id: urlDo
                style: ButtonStyle {
                    background: Rectangle {
                        id: nextTagButtonRect
                        border.width: enabled ? 2 : 0
                        border.color: "#888"
                        color: control.pressed ? myPalette.button : myPalette.window
                        radius: 4
                    }
                    label: Text {
                        text: enabled ? "⟳" : ""
                        color: myPalette.windowText
                    }
                }
                onClicked: loadFile( urledit.editText, true )
            }
            ComboBox {
                id: urledit
                Layout.alignment: Qt.AlignBaseline
                Layout.fillWidth: true
                editable: true;
                textRole: "text"
                ListModel { id: profileList }
                Component.onCompleted: {
                    // show installed profiles
                    var jt   = profile.getProfileList();
                    var list = JSON.parse(jt);
                    for(var index in list.data.value_array)
                    {
                        var p = list.data.value_array[index]
                        profileList.append({
                                             text : p.desc,
                                             key  : p.file_name,
                                             desc : p.desc
                                           })
                    }
                    // a few short cuts; eventually a fallback to inbuild profiles
                    profileList.append( { key: "rgb", text: "rgb" } )
                    profileList.append( { key: "web", text: "web" } )
                    profileList.append( { key: "cmyk", text: "cmyk" } )
                    profileList.append( { key: "gray", text: "gray" } )
                    profileList.append( { key: "lab", text: "lab" } )
                    profileList.append( { key: "xyz", text: "xyz" } )
                    profileList.append( { key: "proof", text: "proof" } )
                    profileList.append( { key: "effect", text: "effect" } )
                    // a online profile
                    profileList.append({
                                         text : "www:sRGB.icm",
                                         key: "http://www.oyranos.com/download/sRGB.icm"
                                       })
                }
                model: profileList
                property bool ignore: false
                onCurrentIndexChanged: {
                    if( ignore )
                        return
                    var p = profileList.get( currentIndex )
                    if(typeof p != "undefined" )
                    {
                        var url
                        if(typeof p.key != "undefined")
                            url = p.key
                        else if(typeof url.text != "undefined")
                            url = p.text
                        if(typeof url != "undefined")
                            loadFile( url, true )
                    }
                }
                onAccepted: {
                    if (find(currentText) === -1) {
                        profileList.insert(0, {text: editText})
                        currentIndex = find(editText)
                    }
                    loadFile( urledit.currentText, false )
                }
            }
        }
    }

    // Take busy out of non UI function
    property string timer_status_text_
    Timer {
        id: setBusyTimer
        triggeredOnStart: false
        interval: 0
        onTriggered: {
            if(isbusy === false)
            {
                timer_status_text_ = statusText
                statusText = qsTr("Busy")
                isbusy = true
            }
        }
    }
    Timer {
        id: unsetBusyTimer
        triggeredOnStart: false
        interval: 200
        onTriggered: {
            if(isbusy === true)
            {
                isbusy = false
                if(timer_status_text_.length)
                    statusText = timer_status_text_
                timer_status_text_ = ""
            }
        }
    }

    property int landscape: 0
    function checkLandscape ()
    {
        if(icc_debug)
            statusText = "checkLand(): old index: " + pages.currentIndex + "/" + pagesContentIndex + " landscape: " + landscape + " indexAt(): " + pages.indexAt(1,1)
        if(width > height)
            landscape = 1
        else
            landscape = 0
    }
    onLandscapeChanged: {
        if(icc_debug)
            statusText = "onLandCh(): old index: " + pages.currentIndex + "/" + pagesContentIndex + " landscape: " + landscape + " indexAt(): " + pages.indexAt(1,1)
        if(pages.currentIndex < 2)
            setPage( 0 )
        //pages.interactive = !landscape
    }

    Component.onCompleted: checkLandscape()
    onWidthChanged: checkLandscape()
    onHeightChanged: checkLandscape()

    property string statusText
    onStatusTextChanged:
        profile.log = "statusText: " + statusText
    property int pagesContentIndex: 0
    function setPage(page)
    {
        if(icc_debug)
            statusText = "setPage("+page+") old index: " + pages.currentIndex + "/" + pagesContentIndex + " landscape: " + landscape

        if(page === -1)
            page = pagesContentIndex
        else
        if(page === 0 || page === 1)
            pagesContentIndex = page

        if(landscape && page < 2)
        {
            pages.currentIndex = -1 // delesect list item to get properly back
            page = 0
        }
        else
        if(landscape === 0 && page < 2)
        {
            page = pagesContentIndex
        }
        pages.currentIndex = page
    }

    ListView {
        id: pages

        onXChanged: if(icc_debug) statusText = "onXChan x: " + x
        width: parent.width
        height: parent.height

        model: pagesModel
        currentIndex: 0
        orientation: ListView.Horizontal
        snapMode: ListView.SnapToItem
        flickDeceleration: 2000

        highlightFollowsCurrentItem: true
        highlightMoveDuration: 500
        focus: true
        Keys.onPressed: {
            if (event.key === Qt.Key_Down) {
                tagListView.list.incrementCurrentIndex()
                tagListView.list.selectItem( tagListView.list.currentIndex )
                event.accepted = true
            } else
            if (event.key === Qt.Key_Up) {
                tagListView.list.decrementCurrentIndex()
                tagListView.list.selectItem( tagListView.list.currentIndex )
                event.accepted = true
            }
        }

        onCurrentIndexChanged: {
            if(icc_debug)
                statusText = "onCurrIndxCh(): currentIndex: " + currentIndex + "/" + pagesContentIndex + "/" + pagesModel.children[0].x + " landscape: " + landscape + " indexAt(): " + indexAt(1,1)
        }
    }

    property real red_x: -1
    property real red_y: -1
    property real green_x: -1
    property real green_y: -1
    property real blue_x: -1
    property real blue_y: -1
    property real white_x: -1
    property real white_y: -1
    property real black_x: -1
    property real black_y: -1
    property var redXYZ: [0,0,0]
    property var greenXYZ: [0,0,0]
    property var blueXYZ: [0,0,0]
    property var whiteXYZ: [0,0,0]
    property var blackXYZ: [0,0,0]
    property var curve_red
    property var curve_green
    property var curve_blue
    property var curve_gray
    property string profileJsonString
    property string infoText
    property string saturationLine
    property var profileJsonObject
    property var tagJsonObject
    property var colorantTable
    property var colorantTableOut
    property int clearSubTagPreSelection: 0
    onTagJsonObjectChanged: {
        profileTagView.load()
        profileTagView.repaint = !profileTagView.repaint
    }


    property bool showJson: false
    VisualItemModel {
        id: pagesModel

        Rectangle {
            width: {
                if(landscape)
                    pages.width/2
                else
                    pages.width
            }
            height: pages.height
            id: firstPage
            color: "transparent"

            ProfileTagListView {
                id: tagListView
                height: pages.height - statusBar.height
                width: parent.width
                h: statusLabel.font.pixelSize * 2
            }
        }
        Rectangle {
            width: {
                if(landscape)
                    pages.width/2
                else
                    pages.width
            }
            height: pages.height
            id: twoPage
            color: "transparent"
            TagView {
                id: profileTagView
                h: statusLabel.font.pixelSize * 2
            }
        }
        Rectangle {
            width: pages.width
            height: pages.height
            id: threePage
            color: "transparent"

            TextArea {
                id: textArea2
                width: parent.width
                height: threePage.height - font.pixelSize * 3 // keep some space for the button

                Accessible.name: "2D graph"
                backgroundVisible: false // keep the area visually simple
                frameVisible: false      // keep the area visually simple
                style: TextAreaStyle { textColor: myPalette.windowText }
                //anchors.fill: parent
                textFormat: showJson ? Qt.PlainText : Qt.RichText
                wrapMode: TextEdit.Wrap
                readOnly: true
                text: showJson ? profileJsonString : infoText
            }
            RowLayout {
                id: buttons
                width: parent.width
                height: textArea2.font.pixelSize * 3
                anchors.bottom: threePage.bottom
                anchors.left: textArea2.left

                ToolButton { // detail button
                    id: detailsButton
                    text: showJson ? qsTr("Show Less") : qsTr("Show More")
                    onClicked: showJson = !showJson
                }
            }
        }
        Rectangle {
            width: pages.width
            height: pages.height
            id: aboutPage
            About { }
        }
    }

    statusBar:
        RowLayout {
            id: mainStatusBar
            anchors.fill: parent
            Label {
                id: statusLabel
                text: statusText
            }
        }

    property bool isbusy: false
    BusyIndicator {
       id: busy
       anchors.centerIn: parent
       running: isbusy
       opacity: 0.85
       Layout.alignment: Qt.AlignLeft
    }
}
