TEMPLATE = app

QT += qml quick widgets xml svg
QTPLUGIN += qsvg

QMAKE_CXXFLAGS += -DREFICCMAX_LIB
QMAKE_CXXFLAGS += -DOYRANOS_LIB

android {
  equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/oyranos
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/oyjl/
    LIBS += -L/run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/lib/
  }
  equals(ANDROID_TARGET_ARCH, x86) {
    INCLUDEPATH+=/run/media/kuwe/KAI-DA/linux/x86-linux-androideabi-4.9/include/oyranos
    LIBS += -L/run/media/kuwe/KAI-DA/linux/x86-linux-androideabi-4.9/lib/
  }
  LIBS += -loyranos-static
  LIBS += -llcms2-static
  INCLUDEPATH += /run/media/kuwe/KAI-DA/linux/arm-linux-androideabi-4.9/include/RefIccMAX/IccProfLib2
  LIBS += -lIccProfLib2-static
  LIBS += -lpng16
  LIBS += -lopenicc-static
  LIBS += -loyjl-static
  LIBS += -lyajl-static
  LIBS += -lxml2-static
  LIBS += -loyjl-core-static
  #LIBS += -lc
  LIBS += -lm
  QMAKE_LFLAGS+=-fopenmp
}
unix:!macx:!android {
INCLUDEPATH+=/home/kuwe/.local/include/oyranos
INCLUDEPATH+=/home/kuwe/.local/include/oyjl
INCLUDEPATH+=/opt/local/include/oyranos
INCLUDEPATH+=/home/kuwe/.local/include/RefIccMAX/IccProfLib2/
#LIBS+=-L/home/kuwe/programme/build-RefIccMaxSuite-Desktop-Debug/IccProfLib/
#LIBS+=-L/home/kuwe/programme/build-RefIccMaxSuite-Desktop_Qt_5_8_0_GCC_64bit-Debug/IccProfLib/
LIBS+=-L/home/kuwe/.local/lib64
LIBS+=-L/opt/local/lib64
LIBS += -lIccProfLib2-static
LIBS += -loyranos-static
LIBS += -loyjl-static
LIBS += -loyjl-core-static
LIBS+=-fopenmp
LIBS+=-lc
LIBS+=-ldl
LIBS+=-lcups
//LIBS+=-lelektra-core
//LIBS+=-lelektra-kdb
LIBS+=-ldbus-1
LIBS+=-lexiv2
LIBS+=-lICE
LIBS+=-ljpeg
LIBS+=-llcms2
LIBS+=-lm
LIBS+=-lopenicc-static
LIBS+=-lpng16
LIBS+=-lraw
LIBS+=-lsane
LIBS+=-lSM
LIBS+=-lstdc++
LIBS+=-lX11
LIBS+=-lXcm
LIBS+=-lXcmX11
LIBS+=-lXcmEDID
LIBS+=-lXext
LIBS+=-lXfixes
LIBS+=-lXinerama
LIBS+=-lxml2
LIBS+=-lXrandr
LIBS+=-lXxf86vm
LIBS+=-lyajl
LIBS+=-lyaml
LIBS+=-lz
QMAKE_LFLAGS+=-fopenmp
}

SOURCES += main.cpp \
    src/icc_profile.cpp \
    src/utils.cpp

RESOURCES += \
    app.qrc

TRANSLATIONS = translations/app_de.ts

lupdate_only{
SOURCES += qml/About.qml qml/main.qml qml/ProfileTagListView.qml qml/TagView.qml
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    include/icc_profile.h \
    include/utils.h \
    include/icc_profile_manager.h \
    include/icc_download.h

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml \
    android/src/com/oyranos/iccExamin/IccExaminActivity.java

DISTFILES += \
    translations/iccExamin_de.ts \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-ldpi/icon.png \
    android/res/drawable-mdpi/icon.png \
    qml/About.qml \
    qml/main.qml \
    qml/ProfileTagListView.qml \
    qml/TagView.qml \
    qml/color.js
