package com.oyranos.iccExamin;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import java.io.*;
import java.lang.Object;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.HttpURLConnection;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;


public class IccExaminActivity extends QtActivity {

    public static native void passViewIntent( String uri );
    public static native void passMemIntent( byte[] input, int len, String uri );
    public static native void passMessageCanNotHandleNewTask( );
    private static int init = 0;
    private static int debug = 0;

    // during first activity call in a task
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onCreateBundle() [" + getTaskId() + "]  init: " + init );

        super.onCreate(savedInstanceState);

        ++init;

        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction()))
        {
            if( intent.getData() != null )
            {
                Uri uri = intent.getData();
                String scheme = uri.getScheme();
                if(BuildConfig.DEBUG || debug == 1)
                  System.out.println( "ACTION_VIEW on " + scheme + " " + uri.toString() + " " +
                    intent.getAction() + " " + intent.getFlags() + " " + intent.getPackage() + " " + intent.getType() );

                if(scheme.equals("content"))
                {
                    try
                    {
                        InputStream input = getContentResolver().openInputStream( uri );
                        int count = input.available();
                        if(BuildConfig.DEBUG || debug == 1)
                          System.out.println( "content size: " + count );
                        byte[] data = new byte[count];
                        count = input.read( data, 0, count );
                        passMemIntent( data, count, uri.toString() );
                    }
                    catch (FileNotFoundException e)
                    {
                        System.err.println("No Stream found");
                    }
                    catch (IOException e2)
                    {
                        System.err.println("Unable to open Stream");
                    }

                } else
                    passViewIntent( uri.toString() );
            }
        }

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onCreateBundle()" );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onActivityResult request: " + requestCode + " result: " + resultCode );

        super.onActivityResult( requestCode, resultCode, data );

        if( data.getAction().equals( android.content.Intent.ACTION_VIEW ) &&
            data.getData() != null )
        {
            Uri uri = data.getData();
            String scheme = uri.getScheme();
            if(BuildConfig.DEBUG || debug == 1)
              System.out.println( "ACTION_VIEW on " + scheme + " "  + uri.toString() + " request: " + requestCode + " result: " + resultCode );

            passViewIntent( uri.toString() );
        } else
            if(BuildConfig.DEBUG || debug == 1)
              System.out.println( "request: " + requestCode + " result: " + resultCode );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onActivityResult()" );
    }

    @Override
    protected void onNewIntent (Intent intent)
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start onNewIntent()" );

        super.onNewIntent( intent );

        if( intent.getAction().equals( android.content.Intent.ACTION_VIEW ) &&
            intent.getData() != null )
        {
            Uri uri = intent.getData();
            String scheme = uri.getScheme();
            if(BuildConfig.DEBUG || debug == 1)
              System.out.println( "ACTION_VIEW on " + scheme + " " + uri.toString() + " " +
              intent.getAction() + " " + intent.getFlags() + " " + intent.getPackage() + " " + intent.getType() );

            if(scheme.equals("content"))
            {
                // avoid security exception
                if(BuildConfig.DEBUG || debug == 1)
                  System.out.println( scheme + " can not be handled from non main task; retry!! finish " + uri.toString() );
                passMessageCanNotHandleNewTask();

            } else
                passViewIntent( uri.toString() );
        }
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end onNewIntent()" );
    }

    @Override
    protected void onPause ()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onPause()" );

        super.onPause( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onPause()" );
    }

    @Override
    protected void onResume ()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onResume()" );

        super.onResume( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onResume()" );
    }

    @Override
    protected void onStart()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onStart()" );

        super.onStart( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onStart()" );
    }

    @Override
    protected void onRestart()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onRestart()" );

        super.onRestart( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onRestart()" );
    }

    @Override
    protected void onStop()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onStop()" );

        super.onStop( );

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onStop()" );
    }

    @Override
    protected void onDestroy()
    {
        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "start on onDestroy()" );

        super.onDestroy( );
        --init;

        if(BuildConfig.DEBUG || debug == 1)
          System.out.println( "end on onDestroy()" );
    }

    /*@Override
    protected void onAttachFragment(Fragment fragment)
    {
        System.out.println( "start on onAttachFragment()" );

        super.onAttachFragment( fragment );

        System.out.println( "end on onAttachFragment()" );
    }*/

    // a separate lib could be created for just android or
    // just add a callable function in libICCExamin.so
    /*static {
        System.loadLibrary("ICCExamin");
    }*/
}

