<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name></name>
    <message>
        <source>!!! ERROR</source>
        <translation type="vanished">!!! Fehler</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>&amp;gt; - next image</source>
        <extracomment>&apos;&gt;&apos;
</extracomment>
        <translation type="vanished">&amp;gt; - nächstes Bild</translation>
    </message>
    <message>
        <source>&amp;lt; - previous image</source>
        <extracomment>&apos;&lt;&apos;
</extracomment>
        <translation type="vanished">&amp;lt; - vorheriges Bild</translation>
    </message>
    <message>
        <source>(device) link class</source>
        <translation type="vanished">(Geräte) Farbraumverknüpfung</translation>
    </message>
    <message>
        <source>+ - zoom in</source>
        <translation type="vanished">+ - Vergrößere Ansicht</translation>
    </message>
    <message>
        <source>- - zoom out</source>
        <translation type="vanished">- - Verkleinere Ansicht</translation>
    </message>
    <message>
        <source>----</source>
        <translation type="vanished">----</translation>
    </message>
    <message>
        <source>--module name</source>
        <translation type="vanished">--module Name</translation>
    </message>
    <message>
        <source>-d device_position_start_from_zero</source>
        <translation type="vanished">-d garäte_position_startet_von_null</translation>
    </message>
    <message>
        <source>-v verbose</source>
        <translation type="vanished">-v plauders</translation>
    </message>
    <message>
        <source>/</source>
        <translation type="vanished">/</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="vanished">0</translation>
    </message>
    <message>
        <source>0/45, 45/0</source>
        <translation type="vanished">0/45, 45/0</translation>
    </message>
    <message>
        <source>0/d or d/0</source>
        <translation type="vanished">0/d oder d/0</translation>
    </message>
    <message>
        <source>1 - map one image pixel to one screen pixel</source>
        <translation type="vanished">1 - zeige einen Bildpunkt auf einem Bildschirmpunkt</translation>
    </message>
    <message>
        <source>1. Channel</source>
        <translation type="vanished">1. Kanal</translation>
    </message>
    <message>
        <source>10 degree (1964)</source>
        <translation type="vanished">10 Grad (1964)</translation>
    </message>
    <message>
        <source>10. Channel</source>
        <translation type="vanished">10. Kanal</translation>
    </message>
    <message>
        <source>100</source>
        <translation type="vanished">100</translation>
    </message>
    <message>
        <source>10color</source>
        <translation type="vanished">10farbig</translation>
    </message>
    <message>
        <source>11. Channel</source>
        <translation type="vanished">11. Kanal</translation>
    </message>
    <message>
        <source>11color</source>
        <translation type="vanished">11farbig</translation>
    </message>
    <message>
        <source>12. Channel</source>
        <translation type="vanished">12. Kanal</translation>
    </message>
    <message>
        <source>12color</source>
        <translation type="vanished">12farbig</translation>
    </message>
    <message>
        <source>13. Channel</source>
        <translation type="vanished">13. Kanal</translation>
    </message>
    <message>
        <source>13color</source>
        <translation type="vanished">13farbig</translation>
    </message>
    <message>
        <source>14. Channel</source>
        <translation type="vanished">14. Kanal</translation>
    </message>
    <message>
        <source>14color</source>
        <translation type="vanished">14farbig</translation>
    </message>
    <message>
        <source>15. Channel</source>
        <translation type="vanished">15. Kanal</translation>
    </message>
    <message>
        <source>15color</source>
        <translation type="vanished">15farbig</translation>
    </message>
    <message>
        <source>16-bit</source>
        <translation type="vanished">16-bit</translation>
    </message>
    <message>
        <source>16. Channel</source>
        <translation type="vanished">16. Kanal</translation>
    </message>
    <message>
        <source>1964 10° Observer Graph:</source>
        <translation type="vanished">1964 10° Betrachter Zeichnung:</translation>
    </message>
    <message>
        <source>2 degree (1931)</source>
        <translation type="vanished">2 Grad (1931)</translation>
    </message>
    <message>
        <source>2. Channel</source>
        <translation type="vanished">2. Kanal</translation>
    </message>
    <message>
        <source>2D Graph from profiles:</source>
        <translation type="vanished">2D Zeichnung vom Farbprofil</translation>
    </message>
    <message>
        <source>2color</source>
        <translation type="vanished">2farbig</translation>
    </message>
    <message>
        <source>3. Channel</source>
        <translation type="vanished">3. Kanal</translation>
    </message>
    <message>
        <source>3color</source>
        <translation type="vanished">3farbig</translation>
    </message>
    <message>
        <source>4. Channel</source>
        <translation type="vanished">4. Kanal</translation>
    </message>
    <message>
        <source>4color</source>
        <translation type="vanished">4farbig</translation>
    </message>
    <message>
        <source>5. Channel</source>
        <translation type="vanished">5. Kanal</translation>
    </message>
    <message>
        <source>5color</source>
        <translation type="vanished">5farbig</translation>
    </message>
    <message>
        <source>6. Channel</source>
        <translation type="vanished">6. Kanal</translation>
    </message>
    <message>
        <source>6color</source>
        <translation type="vanished">6farbig</translation>
    </message>
    <message>
        <source>7. Channel</source>
        <translation type="vanished">7. Kanal</translation>
    </message>
    <message>
        <source>7color</source>
        <translation type="vanished">7farbig</translation>
    </message>
    <message>
        <source>8-bit</source>
        <translation type="vanished">8-bit</translation>
    </message>
    <message>
        <source>8. Channel</source>
        <translation type="vanished">8. Kanal</translation>
    </message>
    <message>
        <source>8color</source>
        <translation type="vanished">8farbig</translation>
    </message>
    <message>
        <source>9. Channel</source>
        <translation type="vanished">9. Kanal</translation>
    </message>
    <message>
        <source>9color</source>
        <translation type="vanished">9farbig</translation>
    </message>
    <message>
        <source>?? Nothing to add ??</source>
        <translation type="vanished">?? Nichts hinzuzufügen ??</translation>
    </message>
    <message>
        <source>???</source>
        <extracomment>case icMaxEnumIluminant: return _(&quot;Illuminant ---&quot;); break;
case icMaxStdObs: return _(&quot;---&quot;); break;
case icMaxGeometry: return _(&quot;---&quot;);
case icMaxFlare: return _(&quot;---&quot;);
</extracomment>
        <translation type="vanished">???</translation>
    </message>
    <message>
        <source>A Color Matching Module (CMM) does the computational work to transform colors. It can provide alternative features regarding appearance, security, speed or resource overhead.</source>
        <translation type="vanished">Ein Farbübertragungsmodul (CMM) berechnet Übertragungsfunktionen und Farben. Es kann alternative Eigenschaften anbieten bezüglich Farbanmutung, Sicherheit, Geschwiundigkeit oder Resourcenverbrauch.</translation>
    </message>
    <message>
        <source>A single path option is required</source>
        <translation type="vanished">Eine einzelne Pfadangabe wird benötigt</translation>
    </message>
    <message>
        <source>About Oyranos</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Über Oyranos</translation>
    </message>
    <message>
        <source>Absolute Colorimetric</source>
        <translation type="vanished">Absolut Farbmetrisch</translation>
    </message>
    <message>
        <source>Abstract</source>
        <extracomment>abst
</extracomment>
        <translation type="vanished">Abstrakter Farbraum</translation>
    </message>
    <message>
        <source>Action for Image profile and Editing profile mismatches.</source>
        <translation type="vanished">Verhalten bei Ungleichheit von Bild- und Editierfarbraum</translation>
    </message>
    <message>
        <source>Activate profiles:</source>
        <translation type="vanished">Aktivieren eines Profiles:</translation>
    </message>
    <message>
        <source>Active Policy:</source>
        <translation type="vanished">Aktive Richtlinie:</translation>
    </message>
    <message>
        <source>Active matrix monitor</source>
        <extracomment>AMD
</extracomment>
        <translation type="vanished">Aktivmatrixmonitor</translation>
    </message>
    <message>
        <source>Adaptation State</source>
        <translation type="vanished">Farbanpassungsgrad</translation>
    </message>
    <message>
        <source>Adaptation state for absolute colorimetric intent</source>
        <translation type="vanished">Farbanpassungsgrad für die absolut farbmetrische Übertragung</translation>
    </message>
    <message>
        <source>All the small details for using this module.</source>
        <translation type="vanished">All die kleinen Details für dieses Modul</translation>
    </message>
    <message>
        <source>Allowed option values are -f openicc and -f xml. unknown format:</source>
        <translation type="vanished">Erlaubte Werte sind -f openicc und -f xml. Unbekanntes Format:</translation>
    </message>
    <message>
        <source>Already enabled</source>
        <translation type="vanished">Bereits eingeschalten</translation>
    </message>
    <message>
        <source>Alt + - brighter</source>
        <translation type="vanished">Alt + - Heller</translation>
    </message>
    <message>
        <source>Alt - - darker</source>
        <translation type="vanished">Alt - - Dunkler</translation>
    </message>
    <message>
        <source>Alt . - reset to normal values</source>
        <translation type="vanished">Alt . - Setzt Helligkeit zurück</translation>
    </message>
    <message>
        <source>Alt 0 - all channels</source>
        <translation type="vanished">Alt 0 - Zeige all Farbkanäle</translation>
    </message>
    <message>
        <source>Alt 1 - first channel</source>
        <translation type="vanished">Alt 1 - Zeige ersten Farbkanal</translation>
    </message>
    <message>
        <source>Alt 2 - second channel</source>
        <translation type="vanished">Alt 2 - Zeige zweiten Farbkanal</translation>
    </message>
    <message>
        <source>Alt n - n-th channel</source>
        <translation type="vanished">Alt n - Zeige n-ten Farbkanal</translation>
    </message>
    <message>
        <source>Alt v - set window fullscreen</source>
        <translation type="vanished">Alt v - Setze Fenstergröße auf Vollbild</translation>
    </message>
    <message>
        <source>Analyze ICC profile information on your system.</source>
        <translation type="vanished">Details zu ICC Profilen in Ihrem System</translation>
    </message>
    <message>
        <source>Apply regions of interesst in form of simple rectangles.</source>
        <translation type="vanished">Wende Regionen an in Form von einfachen Rechtecken.</translation>
    </message>
    <message>
        <source>Argyll CMS CMM</source>
        <extracomment>argl
</extracomment>
        <translation type="vanished">Argyll CMS CMM</translation>
    </message>
    <message>
        <source>Assign Assumed Profile</source>
        <translation type="vanished">Vermutetes Profil Zuweisen</translation>
    </message>
    <message>
        <source>Assign No Profile</source>
        <extracomment>choices
</extracomment>
        <translation type="vanished">Weise kein Profil zu</translation>
    </message>
    <message>
        <source>Assign profile to device:</source>
        <translation type="vanished">Weise Farbprofil einem Gerät zu:</translation>
    </message>
    <message>
        <source>Assigns an untagged CIE*Lab Image this color space</source>
        <translation type="vanished">Weise einem CIE*Lab Bild ohne Profil ein Farbraum zu</translation>
    </message>
    <message>
        <source>Assigns an untagged Cmyk Image this color space</source>
        <translation type="vanished">Weise einem nicht festgeschriebenen Cmyk Bild diesen Farbraum zu</translation>
    </message>
    <message>
        <source>Assigns an untagged Gray Image this color space</source>
        <translation type="vanished">Weise einem nicht festgeschriebenen Grau Bild diesen Farbraum zu</translation>
    </message>
    <message>
        <source>Assigns an untagged Rgb Image this color space</source>
        <translation type="vanished">Weise einem Rgb Bild ohne Profil ein Farbraum zu</translation>
    </message>
    <message>
        <source>Assigns an untagged Rgb Image with source from the WWW this color space</source>
        <translation type="vanished">Weise einem Rgb Bild ihne ICC Profil aus dem WWW diesen Farbraum zu</translation>
    </message>
    <message>
        <source>Assigns an untagged Rgb Image with source from the WWW this color space. This will always be sRGB as defined by W3C.</source>
        <translation type="vanished">Weise einem Rgb Bild ohne ICC Profile aus dem WWW diesen Farbraum zu. Das wird sRGB wie vom W3C vorgegeben sein.</translation>
    </message>
    <message>
        <source>Assigns an untagged XYZ Image this color space</source>
        <translation type="vanished">Weise einem XYZ Bild ohne Profil ein Farbraum zu</translation>
    </message>
    <message>
        <source>Assumed Cmyk source</source>
        <translation type="vanished">Vermutete Cmyk Quelle</translation>
    </message>
    <message>
        <source>Assumed Color Space</source>
        <translation type="vanished">Vermuteter Farbraum</translation>
    </message>
    <message>
        <source>Assumed Color Space for untagged colors</source>
        <translation type="vanished">Erwarteter Farbraum für Farben ohne Farbprofil</translation>
    </message>
    <message>
        <source>Assumed Gray source</source>
        <translation type="vanished">Vermutete Grau Quelle</translation>
    </message>
    <message>
        <source>Assumed Lab source</source>
        <translation type="vanished">Vermutete Lab Quelle</translation>
    </message>
    <message>
        <source>Assumed Rgb source</source>
        <translation type="vanished">Vermutete Rgb Quelle</translation>
    </message>
    <message>
        <source>Assumed Web source</source>
        <translation type="vanished">Vermutete Web Quelle</translation>
    </message>
    <message>
        <source>Assumed XYZ source</source>
        <translation type="vanished">Vermutete XYZ Quelle</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation type="vanished">Attribute</translation>
    </message>
    <message>
        <source>Attributes:      </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Attribute:       </translation>
    </message>
    <message>
        <source>Available Device Profiles from Taxi DB</source>
        <translation type="vanished">Verfügbare Geräteprofile von Taxi DB</translation>
    </message>
    <message>
        <source>Available Device Profiles:</source>
        <translation type="vanished">Verfügbare Geräteprofile:</translation>
    </message>
    <message>
        <source>Available devices</source>
        <translation type="vanished">Verfügbare Geräte</translation>
    </message>
    <message>
        <source>Avoid force of White on White mapping. Default for absolute rendering intent.</source>
        <translation type="vanished">Lasse das Erzwingen einer Weiß zu Weiß übertragung fort. Standard bei Absolut Farbmetrischer Übertragung.</translation>
    </message>
    <message>
        <source>BPC affects often only the Relative Colorimetric Rendering intent.</source>
        <translation type="vanished">BPC beeinflusst oft nur die Relativ Farbmetrische Übertragungsart.</translation>
    </message>
    <message>
        <source>Behaviour</source>
        <translation type="vanished">Verhalten</translation>
    </message>
    <message>
        <source>Behaviour for preselecting Hardproofing with a Proofing Profile at print time</source>
        <translation type="vanished">Verhalten für die Gerätenachbildung mit dem Standardsimulationsprofil auf dem Drucker</translation>
    </message>
    <message>
        <source>Behaviour of color space transformation for proofing</source>
        <translation type="vanished">Verhalten von Farbtransformationen bei der Gerätesimulation</translation>
    </message>
    <message>
        <source>Behaviour of color space transformation for proofing. Most people want a preview on screen only. The Relative Colorimetric intent is right for that. The Absolute Colorimetric intent needs a very careful profiling and non-trivial setup, but allows for side-by-side comparisons.</source>
        <translation type="vanished">Verhalten der Farbübertragung bei der Simulation. Die meisten Nutzer wünschen eine Simulation nur an einem Bildschirm. Die relativ farbmetrische Übertragung ist richtig dafür. Die absolut farbmetrische Übertragung benötigt eine sorgfältige komplizierte Einrichtung für Seit an Seit Vergleiche.</translation>
    </message>
    <message>
        <source>Bits per Sample</source>
        <translation type="vanished">Bit pro Farbkanal</translation>
    </message>
    <message>
        <source>Black</source>
        <translation type="vanished">Schwarz</translation>
    </message>
    <message>
        <source>Black Preservation</source>
        <translation type="vanished">Schwarzerhalt</translation>
    </message>
    <message>
        <source>Blackbody Radiator Spectrum Graph:</source>
        <translation type="vanished">Spektrum eines Lambertstrahlers</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="vanished">Blau</translation>
    </message>
    <message>
        <source>Blue Colorant</source>
        <translation type="vanished">Blaue Grundfarbe</translation>
    </message>
    <message>
        <source>Blue tone reproduction curve</source>
        <translation type="vanished">Blaue Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>CIE *L</source>
        <translation type="vanished">CIE *L</translation>
    </message>
    <message>
        <source>CIE *a</source>
        <translation type="vanished">CIE *a</translation>
    </message>
    <message>
        <source>CIE *b</source>
        <translation type="vanished">CIE *b</translation>
    </message>
    <message>
        <source>CIE *u</source>
        <translation type="vanished">CIE *u</translation>
    </message>
    <message>
        <source>CIE *v</source>
        <translation type="vanished">CIE *v</translation>
    </message>
    <message>
        <source>CIE A spectral power distribution</source>
        <translation type="vanished">CIE A Strahlungsverteilung</translation>
    </message>
    <message>
        <source>CIE D65 spectral power distribution</source>
        <translation type="vanished">CIE D65 Strahlungsverteilung</translation>
    </message>
    <message>
        <source>CIE X</source>
        <translation type="vanished">CIE X</translation>
    </message>
    <message>
        <source>CIE Y (Luminance)</source>
        <translation type="vanished">CIE Y (Leuchtdichte)</translation>
    </message>
    <message>
        <source>CIE Z</source>
        <translation type="vanished">CIE Z</translation>
    </message>
    <message>
        <source>CIE x</source>
        <translation type="vanished">CIE x</translation>
    </message>
    <message>
        <source>CIE y</source>
        <translation type="vanished">CIE y</translation>
    </message>
    <message>
        <source>CLASS</source>
        <translation type="vanished">KLASSE</translation>
    </message>
    <message>
        <source>CLUT is a levels x levels*levels sized PPM, --levels defaults for clut to 64</source>
        <translation type="vanished">3D Interpolationstabellen sind Rastergröße x Rastergröße*Rastergröße dimensionierte PPM-Bilder. --levels (Rastergröße) nutzt initial 64 für -f clut</translation>
    </message>
    <message>
        <source>CMM</source>
        <extracomment>CMM: abbreviation for Color Matching Module
</extracomment>
        <translation type="vanished">CMM</translation>
    </message>
    <message>
        <source>CMM Core</source>
        <translation type="vanished">CMM Kern</translation>
    </message>
    <message>
        <source>CMM Core Fallback</source>
        <translation type="vanished">CMM Kern Notbehelf</translation>
    </message>
    <message>
        <source>CMM Renderer</source>
        <translation type="vanished">CMM Rechner</translation>
    </message>
    <message>
        <source>CMM Renderer Fallback</source>
        <translation type="vanished">CMM Rechner Notbehelf</translation>
    </message>
    <message>
        <source>Calibration date</source>
        <translation type="vanished">Kalibrationsdatum</translation>
    </message>
    <message>
        <source>CameraRaw</source>
        <translation type="vanished">Kamerarohdaten</translation>
    </message>
    <message>
        <source>CameraRaw image</source>
        <translation type="vanished">Kamera Rohdaten</translation>
    </message>
    <message>
        <source>Can not read hardware information from device.</source>
        <translation type="vanished">Kann Geräteinformation nicht auslesen.</translation>
    </message>
    <message>
        <source>Can not write profile</source>
        <translation type="vanished">Kann Profildatei nicht schreiben</translation>
    </message>
    <message>
        <source>Cant read hardware information from device.</source>
        <translation type="vanished">Kann Geräteinformation nicht auslesen.</translation>
    </message>
    <message>
        <source>Cathode ray tube display</source>
        <extracomment>CRT
</extracomment>
        <translation type="vanished">Kathodenstrahlmonitor</translation>
    </message>
    <message>
        <source>Channel Image Filter Object</source>
        <translation type="vanished">Kanalfilterobjekt</translation>
    </message>
    <message>
        <source>Channels:</source>
        <translation type="vanished">Kanäle:</translation>
    </message>
    <message>
        <source>Check if this module can handle a certain command.</source>
        <translation type="vanished">Prüfe ob dieses Modul ein bestimmtes Kommando behandeln kann.</translation>
    </message>
    <message>
        <source>Chromaticity</source>
        <extracomment>chrm
</extracomment>
        <translation type="vanished">Primärfarben</translation>
    </message>
    <message>
        <source>Class</source>
        <translation type="vanished">Klasse</translation>
    </message>
    <message>
        <source>Clip Negatives</source>
        <translation type="vanished">Beschneide Negative</translation>
    </message>
    <message>
        <source>Cmm:             </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">CMM:             </translation>
    </message>
    <message>
        <source>Cmy</source>
        <translation type="vanished">Cmy</translation>
    </message>
    <message>
        <source>Cmyk</source>
        <translation type="vanished">Cmyk</translation>
    </message>
    <message>
        <source>Collections of settings in Oyranos</source>
        <translation type="vanished">Voreinstellungen in Oyranos</translation>
    </message>
    <message>
        <source>Color</source>
        <extracomment>The following strings must match the categories for a menu entry.
</extracomment>
        <translation type="vanished">Farbe</translation>
    </message>
    <message>
        <source>Color Managed Devices</source>
        <translation type="vanished">farbverbindlich eingestellte Geräte</translation>
    </message>
    <message>
        <source>Color Space</source>
        <extracomment>spac
</extracomment>
        <translation type="vanished">Farbraum</translation>
    </message>
    <message>
        <source>Color Transform CLUT&apos;s can additionally use curves for special cases</source>
        <translation type="vanished">Farbtransformationstabellen können für spezielle Fälle zusätzlich mit Kurven kombiniert werden</translation>
    </message>
    <message>
        <source>Color Transforms can be differently stored internally</source>
        <translation type="vanished">Farbumwandlung können intern unterschiedlich gespeichert werden</translation>
    </message>
    <message>
        <source>Color adaption matrix</source>
        <extracomment>chad
</extracomment>
        <translation type="vanished">Farbanpassungsmatrix</translation>
    </message>
    <message>
        <source>Color b</source>
        <translation type="vanished">Farbanteil b</translation>
    </message>
    <message>
        <source>Color channel names</source>
        <extracomment>clrt
</extracomment>
        <translation type="vanished">Farbkanalnamen</translation>
    </message>
    <message>
        <source>Color channel order</source>
        <extracomment>clro
</extracomment>
        <translation type="vanished">Farbkanalordnung</translation>
    </message>
    <message>
        <source>Color channel output names</source>
        <extracomment>clrt
</extracomment>
        <translation type="vanished">Farbkanalnamen Ausgang</translation>
    </message>
    <message>
        <source>Color content can sometimes have no ICC profile assigned. This is a critical situation as the system can not properly convert these colors. Therefore the color processing parts need to pick some profile as a guess. These settings allow to change the picked ICC profile to guide the processing components and allow proper conversion and compositing.</source>
        <translation type="vanished">Farben können mitunter ohne Farbprofil auftreten. Das ist eine schwierige Situation, da das System solche Farben nicht korrekt umwandeln kann. Deshalb sollte ein vermutlich treffendes ICC Profil als Ersatz zugewiesen werden. Die Einstellungen beinflussen die Auswahl des Ersatzprofiles, damit eine Weiterverarbeitung und Mischung mit anderen Farben möglich wird.</translation>
    </message>
    <message>
        <source>Color management has in some situations to resolve unclear situations. This can be done by automatic applying preselected choices or on a case to case base. In unclear situations users can get alerted if they wish and perform manual control.</source>
        <translation type="vanished">Farbmanagement muss gelegentlich in unklare Situationen auflösen. Das kann geschehen in dem Richlinien angewendet werden oder einzelne Optionen gesetzt werden. Benutzer können über Konflikte informiert werden, so sie dies wünschen, und händisch eingreifen.</translation>
    </message>
    <message>
        <source>Color matching between different devices is possible, provided that the color characteristics of the involved devices are known. The quality of a color transform for devices from one color space to an other depends particularly on the quality of the color measurement and the profiling algorithm used during ICC profile creation. Each color space definition happens in reference to a Profile Connection Spaces (PCS). The PCS is a well known color space, based on the average &quot;human observer&quot; as defined by CIE.</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Farbabgleich zwischen verschiedenen Geräten ist möglich, vorausgesetzt die farbmetrischen Eigenschaften der beteiligten Geräte sind bekannt. Die Qualität einer Farbumwandlung von einem Farbgerät zu einem anderen hängt überwiegend von der Genauigkeit der Farbmessung und dem Berechnungsverfahren  für das Erstellen des Farbprofiles ab. Farbraumdefinitionen geschehen in Bezug zu dem Profilverbindungsfarbraum (PCS). Der PCS wurde von der CIE auf Grund der Farbeindrücke von menschlichen Beobachtern ermittelt.</translation>
    </message>
    <message>
        <source>Color measurement data</source>
        <translation type="vanished">Farbmessdaten</translation>
    </message>
    <message>
        <source>Color profiles are often available from manufacturers of imaging devices as digital cameras, monitors and printers. In Oyranos, ICC profiles are assigned to calibration states of color devices, to get close to the device behaviour as was present during ICC profile creation.</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Farbprofile sind oftmals erhältlich von Herstellern solcher Farbgeräte wie Digitalkameras, Bildschirmen oder Druckern. In Oyranos wird ein ICC Profil mit dem Kalibrierzustand dieser Geräte verknüpft, um möglichst nahe an das  Farbverhalten wärend der ICC Profilerzeugung zu gelangen.</translation>
    </message>
    <message>
        <source>Color r</source>
        <translation type="vanished">Farbanteil r</translation>
    </message>
    <message>
        <source>Color space for Simulating an Output Device</source>
        <translation type="vanished">Farbraum zur Nachbildung eines Ausgabegerätes</translation>
    </message>
    <message>
        <source>Color space for showing a effect</source>
        <translation type="vanished">Farbraum zum Zeigen eines Effektes</translation>
    </message>
    <message>
        <source>ColorGear CMM</source>
        <extracomment>CCMS
</extracomment>
        <translation type="vanished">ColorGear CMM</translation>
    </message>
    <message>
        <source>Colour</source>
        <extracomment>The following strings must match the categories for a menu entry.
</extracomment>
        <translation type="obsolete">Farbe</translation>
    </message>
    <message>
        <source>Conversion</source>
        <translation type="vanished">Farbumwandlung</translation>
    </message>
    <message>
        <source>Convert CameraRaw image to Rgb image:</source>
        <translation type="vanished">Wandle Kamera Rohdaten in Rgb Bild um:</translation>
    </message>
    <message>
        <source>Convert Image:</source>
        <translation type="vanished">Wandle Bild um:</translation>
    </message>
    <message>
        <source>Convert automatically</source>
        <translation type="vanished">Wandle automatisch</translation>
    </message>
    <message>
        <source>Convert image through ICC device link profile</source>
        <translation type="vanished">Wandle Bild mittels Geräteverknüpfung um</translation>
    </message>
    <message>
        <source>Convert image to ICC Color Space</source>
        <translation type="vanished">Wandle Bild nach Farbraum um</translation>
    </message>
    <message>
        <source>Convert to Default Cmyk Editing Space</source>
        <translation type="vanished">Übertrage in voreingestellten Cmyk Editierfarbraum</translation>
    </message>
    <message>
        <source>Convert to Default Rgb Editing Space</source>
        <translation type="vanished">Umwandeln zum voreingestellten Rgb Editierfarbraum</translation>
    </message>
    <message>
        <source>Convert to WWW (sRGB)</source>
        <translation type="vanished">Umwandeln für&apos;s WWW (sRGB)</translation>
    </message>
    <message>
        <source>Convert to untagged Cmyk, preserving Cmyk numbers</source>
        <translation type="vanished">Wandle in unspezifziertes Cmyk, erhalte Cmyk Zahlen</translation>
    </message>
    <message>
        <source>Copyright</source>
        <translation type="vanished">Kopierrecht</translation>
    </message>
    <message>
        <source>Copyright &amp;copy;</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Kopierrecht &amp;copy;</translation>
    </message>
    <message>
        <source>Copyright:       </source>
        <translation type="vanished">Kopierrecht:     </translation>
    </message>
    <message>
        <source>Could not allocate memory for:</source>
        <translation type="vanished">Kann nicht reservieren für:</translation>
    </message>
    <message>
        <source>Could not create filter</source>
        <translation type="vanished">Konnte Filter nicht erzeugen</translation>
    </message>
    <message>
        <source>Could not download from WWW. Please install curl or wget.</source>
        <translation type="vanished">Konnte nicht aus dem Netz kopieren. Bitte installieren Sie curl oder wget.</translation>
    </message>
    <message>
        <source>Could not find any meta module. Did you set the OY_MODULE_PATH variable, to point to a Oyranos module loader library?</source>
        <translation type="vanished">Konnte keine Metamodul finden. Ist die OY_MODULE_PATH Variable gesetzt? Sie zeigt auf Verzeichnisse in welchen die Moduleladebibliothek liegt.</translation>
    </message>
    <message>
        <source>Could not open ICC profile</source>
        <translation type="vanished">Konnte Profil nicht öffnen</translation>
    </message>
    <message>
        <source>Could not open default ICC profile</source>
        <translation type="vanished">Konnte voreingestelltes Profil nicht laden</translation>
    </message>
    <message>
        <source>Could not open device</source>
        <translation type="vanished">Konnte Gerät nicht öffnen</translation>
    </message>
    <message>
        <source>Could not open meta module API</source>
        <translation type="vanished">Konnte Meta Schnittstelle nicht öffnen</translation>
    </message>
    <message>
        <source>Could not open or invalid data: </source>
        <translation type="vanished">Konnte nicht öffnen oder ungültige Daten: </translation>
    </message>
    <message>
        <source>Could not open profile</source>
        <translation type="vanished">Konnte Profil nicht laden</translation>
    </message>
    <message>
        <source>Could not open: </source>
        <translation type="vanished">Konnte nicht öffnen: </translation>
    </message>
    <message>
        <source>Could not resolve device</source>
        <translation type="vanished">Konnte Gerät nicht auflösen</translation>
    </message>
    <message>
        <source>Could not resolve device.</source>
        <translation type="vanished">Konnte Gerät nicht auflösen.</translation>
    </message>
    <message>
        <source>Could not resolve device_json</source>
        <translation type="vanished">Konnte device_json nicht auflösen</translation>
    </message>
    <message>
        <source>Could not resolve device_name</source>
        <translation type="vanished">Konnte device_name nicht auflösen</translation>
    </message>
    <message>
        <source>Could not resolve rank_json</source>
        <translation type="vanished">Konnte rank_json nicht auflösen</translation>
    </message>
    <message>
        <source>Could not write to file</source>
        <translation type="vanished">Konnte Datei nicht schreiben</translation>
    </message>
    <message>
        <source>Count all profiles with Lab PCS</source>
        <translation type="vanished">Zähle alle Profile mit Lab PCS</translation>
    </message>
    <message>
        <source>Create 3D CLUT</source>
        <translation type="vanished">Erzeuge eine 3D Interpolationstabelle</translation>
    </message>
    <message>
        <source>Create a ICC abstract proofing profile.</source>
        <translation type="vanished">Erstelle ein abstraktes ICC Simulationsprofile</translation>
    </message>
    <message>
        <source>Create a ICC matrix profile.</source>
        <translation type="vanished">Erstelle ein ICC Matrixprofile</translation>
    </message>
    <message>
        <source>Create a ICC proofing profile.</source>
        <translation type="vanished">Erstelle ein ICC Simulationsprofile</translation>
    </message>
    <message>
        <source>Creation Date:   </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Datum:           </translation>
    </message>
    <message>
        <source>Creation Time in UTC</source>
        <translation type="vanished">Erstelldatum in UTC</translation>
    </message>
    <message>
        <source>Creation of rank_map filed from</source>
        <translation type="vanished">Erzeuge rank_map gefüllt von</translation>
    </message>
    <message>
        <source>Creator</source>
        <translation type="vanished">Ersteller</translation>
    </message>
    <message>
        <source>Currently active policy:</source>
        <translation type="vanished">Aktive Richtlinie:</translation>
    </message>
    <message>
        <source>Curve Set</source>
        <extracomment>Multi process elements types
</extracomment>
        <translation type="vanished">Kurvensatz</translation>
    </message>
    <message>
        <source>Curves for Optimization</source>
        <translation type="vanished">Kurven für Optimierungen</translation>
    </message>
    <message>
        <source>Cyan</source>
        <translation type="vanished">Zyan</translation>
    </message>
    <message>
        <source>DISPLAY variable not set: giving up.</source>
        <translation type="vanished">DISPLAY Variable nicht gesetzt: gebe auf.</translation>
    </message>
    <message>
        <source>Data Color Space:</source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Farbraum:        </translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Datum</translation>
    </message>
    <message>
        <source>Debug</source>
        <translation type="vanished">Fehlersuche</translation>
    </message>
    <message>
        <source>Decide how to preserve the black channel for Cmyk to Cmyk transforms</source>
        <translation type="vanished">Entscheide wie der Schwarzkanal erhalten wird bei Umwandlungen von einem Druckfarbraum in einen Anderen</translation>
    </message>
    <message>
        <source>Decide how to resolve conflicts with input color spaces and current settings.</source>
        <translation type="vanished">Was ist zu tuen falls Standardfarbräume nicht mit den Eingestellten übereinstimmen.</translation>
    </message>
    <message>
        <source>Decide what to do when the default color spaces don&apos;t match the current ones.</source>
        <translation type="vanished">Was ist zu tuen falls Standardfarbräume nicht mit den Eingestellten übereinstimmen.</translation>
    </message>
    <message>
        <source>Default Profiles</source>
        <translation type="vanished">Voreingestellte ICC Profile</translation>
    </message>
    <message>
        <source>Description:     </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Beschreibung:    </translation>
    </message>
    <message>
        <source>Device</source>
        <extracomment>CMM: abbreviation for Color Matching Module
</extracomment>
        <translation type="vanished">Gerät</translation>
    </message>
    <message>
        <source>Device Manufacturer</source>
        <translation type="vanished">Geräte Hersteller</translation>
    </message>
    <message>
        <source>Device Model</source>
        <translation type="vanished">Geräte Model</translation>
    </message>
    <message>
        <source>Device Related</source>
        <translation type="vanished">Gerätepassend</translation>
    </message>
    <message>
        <source>Device Settings</source>
        <translation type="vanished">Geräte Einstellungen</translation>
    </message>
    <message>
        <source>Device colors</source>
        <extracomment>DevD
</extracomment>
        <translation type="vanished">Farbmessflächen</translation>
    </message>
    <message>
        <source>Device manufacturerer description</source>
        <translation type="vanished">Geräteherstellerbeschreibung</translation>
    </message>
    <message>
        <source>Device model description</source>
        <translation type="vanished">Gerätebeschreibung</translation>
    </message>
    <message>
        <source>DeviceLink CMM</source>
        <extracomment>RGMS / Rolf Gierling Multitools
</extracomment>
        <translation type="vanished">DeviceLink CMM</translation>
    </message>
    <message>
        <source>Devices</source>
        <translation type="vanished">Geräte</translation>
    </message>
    <message>
        <source>Did you specify a 3 letter manufacturer code?</source>
        <translation type="vanished">Gaben Sie ein Herstellerkürzel mit drei Buchstaben an?</translation>
    </message>
    <message>
        <source>Digital camera</source>
        <extracomment>dcam
</extracomment>
        <translation type="vanished">Digitale Kamera</translation>
    </message>
    <message>
        <source>Display</source>
        <extracomment>mntr
</extracomment>
        <translation type="vanished">Monitor</translation>
    </message>
    <message>
        <source>Dump Device Infos to OpenICC device JSON:</source>
        <translation type="vanished">Schreibe Geräteinformationen nach OpenICC JSON:</translation>
    </message>
    <message>
        <source>Dump data:</source>
        <translation type="vanished">Schreibe Daten:</translation>
    </message>
    <message>
        <source>Dump device color state:</source>
        <translation type="vanished">Schreibe Farbeinstellungen:</translation>
    </message>
    <message>
        <source>Dump out the actual settings:</source>
        <translation type="vanished">Die aktuellen Einstellungen als XML ausgeben</translation>
    </message>
    <message>
        <source>Dye sublimation printer</source>
        <extracomment>dsub
</extracomment>
        <translation type="vanished">Thermosublimationsdrucker</translation>
    </message>
    <message>
        <source>EBU Tech.3213-E</source>
        <translation type="vanished">EBU Tech.3213-E</translation>
    </message>
    <message>
        <source>Edit Options</source>
        <translation type="obsolete">Editiere Optionen ...</translation>
    </message>
    <message>
        <source>Edit Options ...</source>
        <translation type="vanished">Editiere Optionen ...</translation>
    </message>
    <message>
        <source>Editing Cmyk</source>
        <translation type="vanished">Editier Cmyk</translation>
    </message>
    <message>
        <source>Editing Color Space</source>
        <translation type="vanished">Editierfarbraum</translation>
    </message>
    <message>
        <source>Editing Gray</source>
        <translation type="vanished">Editier Grau</translation>
    </message>
    <message>
        <source>Editing Lab</source>
        <translation type="vanished">Editier Lab</translation>
    </message>
    <message>
        <source>Editing Rgb</source>
        <translation type="vanished">Editier Rgb</translation>
    </message>
    <message>
        <source>Editing XYZ</source>
        <translation type="vanished">Editier XYZ</translation>
    </message>
    <message>
        <source>Effect</source>
        <translation type="vanished">Effekt</translation>
    </message>
    <message>
        <source>Effect Settings</source>
        <translation type="vanished">Effekt Einstellungen</translation>
    </message>
    <message>
        <source>Effect Settings allows one to decide about a abstract effect color space.</source>
        <translation type="vanished">Effektinstellungen erlauben die Benutzung eines Farbprofiles zur Farbveränderung.</translation>
    </message>
    <message>
        <source>Effect abtract Color Space</source>
        <translation type="vanished">Effectfarbraum</translation>
    </message>
    <message>
        <source>Electrophotographic printer</source>
        <extracomment>epho
</extracomment>
        <translation type="vanished">Elektrofotografischer Drucker</translation>
    </message>
    <message>
        <source>Electrostatic printer</source>
        <extracomment>esta
</extracomment>
        <translation type="vanished">Electrostatischer Drucker</translation>
    </message>
    <message>
        <source>Embedd device meta tag from image device into given device profile</source>
        <translation type="vanished">Bette Gerätemetainfos von einem Bildgerät in ein gegebenes Farbprofil</translation>
    </message>
    <message>
        <source>Enable effect profile</source>
        <translation type="vanished">Benutze Effektprofil</translation>
    </message>
    <message>
        <source>Enable simulation of an output color space on a local printer. Most users do not work for a certain media and leave color handling to the system. They want no simulation.</source>
        <translation type="vanished">Simuliere einen Ausgabefarbraum auf einem lokalen Drucker. Die meisten Benutzer arbeiten nicht für spezielle Ausgabemedien und überlassen die Farbanpassung dem System. Sie möchten keine Simulation.</translation>
    </message>
    <message>
        <source>Enable simulation of output print on the monitor. Most users do not work for a certain media and leave color handling to the system. They want no simulation.</source>
        <translation type="vanished">Simuliere einen Ausgabefarbraum auf dem Bildschirm. Die meisten Benutzer arbeiten nicht für spezielle Ausgabemedien und überlassen die Farbanpassung dem System. Sie möchten keine Simulation.</translation>
    </message>
    <message>
        <source>Erase profile:</source>
        <translation type="vanished">Melde Profil ab:</translation>
    </message>
    <message>
        <source>Error getting atom</source>
        <translation type="vanished">Fehler beim Suchen des Xatoms</translation>
    </message>
    <message>
        <source>Error obtaining profile</source>
        <translation type="vanished">Fehler beim Erhalten des Profiles</translation>
    </message>
    <message>
        <source>Error setting up atom</source>
        <translation type="vanished">Fehler beim Setzen eines Xatoms</translation>
    </message>
    <message>
        <source>Examine ICC Profile ...</source>
        <translation type="vanished">Untersuche ICC Profil ...</translation>
    </message>
    <message>
        <source>Example</source>
        <translation type="vanished">Beispiel</translation>
    </message>
    <message>
        <source>Example Device</source>
        <translation type="vanished">Beispiel Gerät</translation>
    </message>
    <message>
        <source>Example Devices, for testing and learning purposes only.</source>
        <translation type="vanished">Beispielgerät, nur zum Testen.</translation>
    </message>
    <message>
        <source>Exit!</source>
        <translation type="vanished">Beenden!</translation>
    </message>
    <message>
        <source>Expose Image Filter Object</source>
        <translation type="vanished">FilterObjekt zur Belichtung</translation>
    </message>
    <message>
        <source>Exposure:</source>
        <translation type="vanished">Belichtung:</translation>
    </message>
    <message>
        <source>Extended Options</source>
        <translation type="vanished">Erweiterte Optionen</translation>
    </message>
    <message>
        <source>Extract ICC profile:</source>
        <translation type="vanished">Extrahiere ICC Profil:</translation>
    </message>
    <message>
        <source>FILE</source>
        <translation type="vanished">DATEI</translation>
    </message>
    <message>
        <source>FILE_NAME</source>
        <translation type="vanished">DATEINAME</translation>
    </message>
    <message>
        <source>FORMAT</source>
        <translation type="vanished">FORMAT</translation>
    </message>
    <message>
        <source>File Magic</source>
        <translation type="vanished">Dateisignatur</translation>
    </message>
    <message>
        <source>File exists</source>
        <translation type="vanished">Datei existiert</translation>
    </message>
    <message>
        <source>File name is missed</source>
        <translation type="vanished">Dateiname wird vermißt</translation>
    </message>
    <message>
        <source>File not loaded!</source>
        <translation type="vanished">Datei nicht geladen!</translation>
    </message>
    <message>
        <source>File:            </source>
        <translation type="vanished">Datei:           </translation>
    </message>
    <message>
        <source>Files</source>
        <extracomment>The following strings must match the categories for a menu entry.
</extracomment>
        <translation type="vanished">Dateien</translation>
    </message>
    <message>
        <source>Film scanner</source>
        <extracomment>fscn
</extracomment>
        <translation type="vanished">Filmscanner</translation>
    </message>
    <message>
        <source>Film writer</source>
        <extracomment>fprn
</extracomment>
        <translation type="vanished">Filmbelichter</translation>
    </message>
    <message>
        <source>Flags:           </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Optionen:        </translation>
    </message>
    <message>
        <source>Flexography</source>
        <extracomment>flex
</extracomment>
        <translation type="vanished">Flexodruck</translation>
    </message>
    <message>
        <source>FloatPCS2Lab</source>
        <translation type="vanished">Fließkomma PCS nach Lab</translation>
    </message>
    <message>
        <source>FloatPCS2XYZ</source>
        <translation type="vanished">Fließkomma PCS nach XYZ</translation>
    </message>
    <message>
        <source>For Print</source>
        <translation type="vanished">Für den Druck</translation>
    </message>
    <message>
        <source>For Screen</source>
        <translation type="vanished">Bildschirm</translation>
    </message>
    <message>
        <source>For more informations read the man page:</source>
        <translation type="vanished">Mehr Informationen stehen im Handbuch:</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Found no node in conversion. give up</source>
        <translation type="vanished">Fand keinen Knoten im Graph. Gebe auf.</translation>
    </message>
    <message>
        <source>Gamut Warning</source>
        <translation type="vanished">Farbumfangswarnung</translation>
    </message>
    <message>
        <source>General options:</source>
        <translation type="vanished">Allgemeine Optionen:</translation>
    </message>
    <message>
        <source>Generate CLUT Image:</source>
        <translation type="vanished">Erzeuge 3D Interpolationstabelle:</translation>
    </message>
    <message>
        <source>Generate Device Link Profile:</source>
        <translation type="vanished">Erzeuge Verknüpfungsprofil:</translation>
    </message>
    <message>
        <source>Generate Image:</source>
        <translation type="vanished">Erzeuge Bild:</translation>
    </message>
    <message>
        <source>Get Conversion</source>
        <translation type="vanished">Hole Farbumwandlung</translation>
    </message>
    <message>
        <source>Get ICC Profile:</source>
        <translation type="vanished">Hole ICC Profil:</translation>
    </message>
    <message>
        <source>Get ICC profile</source>
        <translation type="vanished">Hole ICC Profil</translation>
    </message>
    <message>
        <source>Gravure</source>
        <extracomment>grav
</extracomment>
        <translation type="vanished">Tiefdruck</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">Grau</translation>
    </message>
    <message>
        <source>Gray tone reproduction curve</source>
        <translation type="vanished">Schwarze Wiedergabekurve</translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="vanished">Grün</translation>
    </message>
    <message>
        <source>Green Colorant</source>
        <translation type="vanished">Grüne Grundfarbe</translation>
    </message>
    <message>
        <source>Green tone reproduction curve</source>
        <translation type="vanished">Grüne Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>Handle mixed color spaces in preparing a document for print output.</source>
        <translation type="vanished">Behandlung von uneinheitlichen Farbräumen in Vorbereitung eines Dokumentes für die Druckausgabe</translation>
    </message>
    <message>
        <source>Handle mixed color spaces in preparing a document for print output. A conversion to the default editing color space is typically what most users want.</source>
        <translation type="vanished">Behandlung von uneinheitlichen Farbräumen in Vorbereitung eines Dokumentes für die Druckausgabe. Die Umwandlung in den voreingestellten Editierfarbraum ist, was die meisten Benutzer benötigen.</translation>
    </message>
    <message>
        <source>Handle mixed color spaces in preparing a document for screen output.</source>
        <translation type="vanished">Behandlung von uneinheitlichen Farbräumen in Vorbereitung eines Dokumentes für die Bildschirmausgabe</translation>
    </message>
    <message>
        <source>Handle mixed color spaces in preparing a document for screen output. A conversion to sRGB helps in easy data exchange and is what most users want.</source>
        <translation type="vanished">Behandlung von uneinheitlichen Farbräumen in Vorbereitung eines Dokumentes für die Bildschirmausgabe. Eine Umwandlung nach sRGB hilft für einen einfacheren Datenaustausch und ist, was die meisten Benutzer wünschen.</translation>
    </message>
    <message>
        <source>Handle options.</source>
        <translation type="vanished">Moduloptionen.</translation>
    </message>
    <message>
        <source>Handling of Mixed Color Spaces inside one single Document</source>
        <translation type="vanished">Behandlung von gemischten Farbräumen in einem einzelnen Dokument</translation>
    </message>
    <message>
        <source>Hardproof</source>
        <translation type="vanished">Nachbildung beim Ausdruck</translation>
    </message>
    <message>
        <source>Header</source>
        <translation type="vanished">Kopf</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hilfe</translation>
    </message>
    <message>
        <source>Help Shortcuts</source>
        <translation type="vanished">Hilfe zur Tastatur</translation>
    </message>
    <message>
        <source>Hint: search paths are influenced by the XDG_CONFIG_HOME shell variable.</source>
        <translation type="vanished">Hinweis: Suchpfade werden durch die XDG_CONFIG_HOME Kommandozeilenvariable
         beeinflusst.</translation>
    </message>
    <message>
        <source>Hls</source>
        <translation type="vanished">Hls</translation>
    </message>
    <message>
        <source>Hsv</source>
        <translation type="vanished">Hsv</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation type="vanished">Farbton</translation>
    </message>
    <message>
        <source>ICC Class</source>
        <translation type="vanished">ICC Klasse</translation>
    </message>
    <message>
        <source>ICC Examin</source>
        <translation type="vanished">ICC Examin</translation>
    </message>
    <message>
        <source>ICC Examin Object</source>
        <translation type="obsolete">ICC Examin</translation>
    </message>
    <message>
        <source>ICC Examin output filter</source>
        <translation type="obsolete">ICC Examin</translation>
    </message>
    <message>
        <source>ICC Version</source>
        <translation type="vanished">ICC Version</translation>
    </message>
    <message>
        <source>ICC color profile for color transformations</source>
        <translation type="vanished">ICC Farbprofil zur Farbraumtransformation.</translation>
    </message>
    <message>
        <source>ICC color profiles for color transformations</source>
        <translation type="vanished">ICC Farbprofile zur Farbraumumwandlung</translation>
    </message>
    <message>
        <source>ICC header Attributes</source>
        <translation type="vanished">ICC Dateiattribute</translation>
    </message>
    <message>
        <source>ICC profile</source>
        <translation type="vanished">ICC Profil</translation>
    </message>
    <message>
        <source>ICC profile id written</source>
        <translation type="vanished">ICC Profil ID geschrieben</translation>
    </message>
    <message>
        <source>ICC profile search paths</source>
        <translation type="vanished">ICC Profilesuchpfade</translation>
    </message>
    <message>
        <source>ICC profiles</source>
        <translation type="vanished">ICC Profile</translation>
    </message>
    <message>
        <source>ICC profiles reside in known paths. They are defined by OpenICC and can be used by every complying system or application.</source>
        <translation type="vanished">ICC Profile befinden sich in bekannten Verzeichnissen. Die Pfade sind durch OpenICC definiert und stehen allen unterstützenden Systemen und Anwendungen zur Verfügung.</translation>
    </message>
    <message>
        <source>ICC profiles reside in known paths. They are defined by OpenICC and can be used by every complying system or application. XDG_DATA_HOME and XDG_DATA_DIRS environment variables route Oyranos to top directories containing resources. The derived paths for ICC profiles have a &quot;color/icc&quot; appended.</source>
        <translation type="vanished">ICC Profile befinden sich in bekannten Verzeichnissen. Die Pfade sind durch OpenICC definiert und stehen allen unterstützenden Systemen und Anwendungen zur Verfügung. Die XDG_DATA_HOME und XDG_DATA_DIRS Umgebungsvariablen leiten Oyranos zu den oberen Verzeichnissen mit Resourcen. Die daraus gebildeten Pfade für ICC Farbprofile werden durch ein &quot;color/icc&quot; ergänzt.</translation>
    </message>
    <message>
        <source>ICC profiles should always be attached to all colors. In the case that colors are undefined, users can set various default ICC profiles in Oyranos settings.</source>
        <translation type="vanished">Alle Farben sollten auf einem Rechner mit ICC Farbprofilen definiert sein. Für nicht definierte Farben können Benutzer ICC Farbprofile in Oyranos voreinstellen.</translation>
    </message>
    <message>
        <source>ICC_FILE_NAME</source>
        <translation type="vanished">ICC_DATEINAME</translation>
    </message>
    <message>
        <source>ITU-R BT.709</source>
        <translation type="vanished">ITU-R BT.709</translation>
    </message>
    <message>
        <source>Identity</source>
        <extracomment>Identities
</extracomment>
        <translation type="vanished">Idendität</translation>
    </message>
    <message>
        <source>Illuminant</source>
        <translation type="vanished">Beleuchtung</translation>
    </message>
    <message>
        <source>Illuminant A</source>
        <translation type="vanished">Farbtemperatur A</translation>
    </message>
    <message>
        <source>Illuminant D50</source>
        <translation type="vanished">Farbtemperatur D50</translation>
    </message>
    <message>
        <source>Illuminant D55</source>
        <translation type="vanished">Farbtemperatur D55</translation>
    </message>
    <message>
        <source>Illuminant D65</source>
        <translation type="vanished">Farbtemperatur D65</translation>
    </message>
    <message>
        <source>Illuminant D93</source>
        <translation type="vanished">Farbtemperatur D93</translation>
    </message>
    <message>
        <source>Illuminant F2</source>
        <translation type="vanished">Farbtemperatur F2</translation>
    </message>
    <message>
        <source>Illuminant F8</source>
        <translation type="vanished">Farbtemperatur F8</translation>
    </message>
    <message>
        <source>Illuminant Spectrum Graph:</source>
        <translation type="vanished">Beleuchtungsspektrum A</translation>
    </message>
    <message>
        <source>Illuminant unknown</source>
        <translation type="vanished">Illuminant unbekannt</translation>
    </message>
    <message>
        <source>Illuminant with equal energy E</source>
        <translation type="vanished">Farbtemperatur mit ausgeglichener Energie E</translation>
    </message>
    <message>
        <source>Illuminant:      </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Beleuchtung:     </translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Bild</translation>
    </message>
    <message>
        <source>Image PNG Socket</source>
        <translation type="vanished">PNG Bilddose</translation>
    </message>
    <message>
        <source>Image Plug</source>
        <translation type="vanished">Bildstecker</translation>
    </message>
    <message>
        <source>Image Socket</source>
        <translation type="vanished">Bilddose</translation>
    </message>
    <message>
        <source>Image has no color space embedded. What default action shall be performed?</source>
        <translation type="vanished">Bild enthält kein Farbprofil. Welche Standardhandlung soll ausgeführt werden?</translation>
    </message>
    <message>
        <source>Image/Simple Image[channel]</source>
        <translation type="vanished">Bild/Einfaches Bild[Kanal]</translation>
    </message>
    <message>
        <source>Image/Simple Image[expose]</source>
        <translation type="vanished">Bild/Einfaches Bild[Belichtung]</translation>
    </message>
    <message>
        <source>Image/Simple Image[scale]</source>
        <translation type="vanished">Bild/Einfaches Bild[Skalierung]</translation>
    </message>
    <message>
        <source>Image[channel]</source>
        <translation type="vanished">Bild[Kanal]</translation>
    </message>
    <message>
        <source>Image[expose]</source>
        <translation type="vanished">Bild[Belichtung]</translation>
    </message>
    <message>
        <source>Image[iemn_filter]</source>
        <translation type="obsolete">Bild[my_filter]</translation>
    </message>
    <message>
        <source>Image[input_png]</source>
        <translation type="vanished">Bild[input_png]</translation>
    </message>
    <message>
        <source>Image[input_ppm]</source>
        <translation type="vanished">Bild[input_ppm]</translation>
    </message>
    <message>
        <source>Image[lraw]</source>
        <translation type="vanished">Bild[lraw]</translation>
    </message>
    <message>
        <source>Image[my_filter]</source>
        <translation type="vanished">Bild[my_filter]</translation>
    </message>
    <message>
        <source>Image[out]</source>
        <translation type="vanished">Bild[Ausgang]</translation>
    </message>
    <message>
        <source>Image[scale]</source>
        <translation type="vanished">Bild[Skalierung]</translation>
    </message>
    <message>
        <source>Image[write_png]</source>
        <translation type="vanished">Bild[write_png]</translation>
    </message>
    <message>
        <source>Image[write_ppm]</source>
        <translation type="vanished">Bild[write_ppm]</translation>
    </message>
    <message>
        <source>Img</source>
        <translation type="vanished">Bild</translation>
    </message>
    <message>
        <source>Incoming color spaces can differ from the default editing color space. For most users, an automatic conversion is fine. However, for more demanding work, the numbers need to be preserved.</source>
        <translation type="vanished">Eingehende Farbräume können von der Voreinstellung abweichen. Für viele Benutzer ist eine automatische Umwandlung kein Problem. Für anspruchsvollere Anwendungen sollten die Zahlenwerte stets erhalten bleiben.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">Informationen</translation>
    </message>
    <message>
        <source>Information:</source>
        <translation type="vanished">Informationen:</translation>
    </message>
    <message>
        <source>InkJet printer</source>
        <extracomment>ijet
</extracomment>
        <translation type="vanished">Tintenstrahldrucker</translation>
    </message>
    <message>
        <source>Input</source>
        <extracomment>scnr
</extracomment>
        <translation type="vanished">Eingabe</translation>
    </message>
    <message>
        <source>Input Color Space</source>
        <translation type="vanished">Eingabefarbraum</translation>
    </message>
    <message>
        <source>Input PNG Image Filter Object</source>
        <translation type="vanished">Filterobjekt eines zu lesendes PNG Bildes</translation>
    </message>
    <message>
        <source>Input PPM Image Filter Object</source>
        <translation type="vanished">Filterobjekt eines zu lesendes PPM Bildes</translation>
    </message>
    <message>
        <source>Input libraw Image Filter Object</source>
        <translation type="vanished">Filterobjekt für libraw Eingangsbilder</translation>
    </message>
    <message>
        <source>Install ICC profile:</source>
        <translation type="vanished">Installiere ICC Profil:</translation>
    </message>
    <message>
        <source>Install profile</source>
        <translation type="vanished">Installiere Profil</translation>
    </message>
    <message>
        <source>Install selected profile</source>
        <translation type="vanished">Installiere ICC Profil</translation>
    </message>
    <message>
        <source>Install the selected profile.</source>
        <translation type="vanished">Installiere das ausgewählte ICC Profil.</translation>
    </message>
    <message>
        <source>Intent</source>
        <translation type="vanished">Übertragung</translation>
    </message>
    <message>
        <source>Internal Error</source>
        <translation type="vanished">Interner Fehler</translation>
    </message>
    <message>
        <source>Internal stored Size</source>
        <translation type="vanished">Interne Größe</translation>
    </message>
    <message>
        <source>Internet: &lt;a href=&quot;http://www.oyranos.org&quot;&gt;www.oyranos.org&lt;/a&gt;&lt;br&gt;</source>
        <translation type="vanished">Internet: &lt;a href=&quot;http://www.oyranos.org&quot;&gt;www.oyranos.org&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>Lab</source>
        <translation type="vanished">Lab</translation>
    </message>
    <message>
        <source>Lab2FloatPCS</source>
        <extracomment>Float to floatPCS
</extracomment>
        <translation type="vanished">Lab nach Fließkomma PCS</translation>
    </message>
    <message>
        <source>Lab2XYZ</source>
        <translation type="vanished">Lab nach XYZ</translation>
    </message>
    <message>
        <source>Lightness</source>
        <translation type="vanished">Helligkeit</translation>
    </message>
    <message>
        <source>Link</source>
        <extracomment>link
</extracomment>
        <translation type="vanished">Verknüpfung</translation>
    </message>
    <message>
        <source>List ICC Profiles:</source>
        <translation type="vanished">Liste der verfügbaren ICC Farbprofile</translation>
    </message>
    <message>
        <source>List Taxi DB profiles for selected device:</source>
        <translation type="vanished">Liste Profile von Taxi für gewähltes Gerät:</translation>
    </message>
    <message>
        <source>List available ICC profiles:</source>
        <translation type="vanished">Liste der verfügbaren ICC Farbprofile</translation>
    </message>
    <message>
        <source>List available device manufacturers:</source>
        <translation type="vanished">Liste der verfügbaren Gerätehersteller:</translation>
    </message>
    <message>
        <source>List available policies:</source>
        <translation type="vanished">Liste der verfügbaren Richtlinien:</translation>
    </message>
    <message>
        <source>List device classes:</source>
        <translation type="vanished">Zeige Geräteklassen:</translation>
    </message>
    <message>
        <source>List devices:</source>
        <translation type="vanished">Geräteliste:</translation>
    </message>
    <message>
        <source>List included ICC tags:</source>
        <translation type="vanished">Liste der enthaltenen ICC Elemente:</translation>
    </message>
    <message>
        <source>List local DB profiles for selected device:</source>
        <translation type="vanished">Liste lokale DB Profile für gewähltes Gerät:</translation>
    </message>
    <message>
        <source>List modules:</source>
        <translation type="vanished">Modulliste:</translation>
    </message>
    <message>
        <source>List search URL:</source>
        <translation type="vanished">Zeige Suchadresse:</translation>
    </message>
    <message>
        <source>List search paths:</source>
        <translation type="vanished">Zeige Suchpfade:</translation>
    </message>
    <message>
        <source>Little CMS</source>
        <translation type="vanished">Little CMS</translation>
    </message>
    <message>
        <source>Little CMS 2</source>
        <translation type="vanished">Little CMS 2</translation>
    </message>
    <message>
        <source>Little CMS CMM</source>
        <extracomment>lcms
</extracomment>
        <translation type="vanished">Little CMS CMM</translation>
    </message>
    <message>
        <source>Little CMS can use curves before and after CLUT&apos;s for special cases like gamma encoded values to and from linear gamma values. Performance will suffer.</source>
        <translation type="vanished">Little CMS kann Kurven vor und nach 3D Interpolationstabellen benutzen. Dadurch zeigen sich weniger Fehler bei der Übertragung zwischen linearen und gammabehafteten Werten auf Kosten der Geschwindigkeit.</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="vanished">Lade</translation>
    </message>
    <message>
        <source>Load Image File Object</source>
        <translation type="vanished">Lade Bilddateiobjekt</translation>
    </message>
    <message>
        <source>Look Up Table</source>
        <translation type="vanished">Interpolationstabelle</translation>
    </message>
    <message>
        <source>Lookup table, PCS to device, intent perceptual</source>
        <translation type="vanished">Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Fotografisch</translation>
    </message>
    <message>
        <source>Lookup table, PCS to device, intent relative colorimetric</source>
        <translation type="vanished">Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <source>Lookup table, PCS to device, intent saturation</source>
        <translation type="vanished">Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Sättigung</translation>
    </message>
    <message>
        <source>Lookup table, device to PCS, intent perceptual</source>
        <translation type="vanished">Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Fotografisch</translation>
    </message>
    <message>
        <source>Lookup table, device to PCS, intent relative colorimetric</source>
        <translation type="vanished">Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <source>Lookup table, device to PCS, intent saturation</source>
        <translation type="vanished">Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Sättigung</translation>
    </message>
    <message>
        <source>Luminance</source>
        <translation type="vanished">Leuchtdichte</translation>
    </message>
    <message>
        <source>Luminance Y</source>
        <translation type="vanished">Leuchtdichte Y</translation>
    </message>
    <message>
        <source>Luv</source>
        <translation type="vanished">Luv</translation>
    </message>
    <message>
        <source>MEM Error.</source>
        <translation type="vanished">Speicher Fehler</translation>
    </message>
    <message>
        <source>MIT</source>
        <translation type="vanished">MIT</translation>
    </message>
    <message>
        <source>MIT license: http://www.opensource.org/licenses/mit-license.php</source>
        <translation type="vanished">MIT Lizenz: http://www.opensource.org/licenses/mit-license.php</translation>
    </message>
    <message>
        <source>MODULE_NAME</source>
        <translation type="vanished">MODULE_NAME</translation>
    </message>
    <message>
        <source>Macintosh</source>
        <translation type="vanished">Macintosh</translation>
    </message>
    <message>
        <source>Magenta</source>
        <translation type="vanished">Magenta</translation>
    </message>
    <message>
        <source>Magic</source>
        <translation type="vanished">Signatur</translation>
    </message>
    <message>
        <source>Magic:           </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Signatur:        </translation>
    </message>
    <message>
        <source>Manufacturer</source>
        <translation type="vanished">Hersteller</translation>
    </message>
    <message>
        <source>Manufacturer:    </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Hersteller:      </translation>
    </message>
    <message>
        <source>Mark Out Of Gamut colors</source>
        <translation type="vanished">Markiere nicht darstellbare Farben</translation>
    </message>
    <message>
        <source>Matrix</source>
        <translation type="vanished">Matrix</translation>
    </message>
    <message>
        <source>Me</source>
        <translation type="vanished">Ich</translation>
    </message>
    <message>
        <source>Measured colors</source>
        <extracomment>CIED
</extracomment>
        <translation type="vanished">Farbmessergebnisse</translation>
    </message>
    <message>
        <source>Measurement</source>
        <translation type="vanished">Messart</translation>
    </message>
    <message>
        <source>Media black point</source>
        <translation type="vanished">Medienschwarzpunkt</translation>
    </message>
    <message>
        <source>Media white point</source>
        <translation type="vanished">Medienweißpunkt</translation>
    </message>
    <message>
        <source>Meta Data</source>
        <translation type="vanished">Allgemeine Daten</translation>
    </message>
    <message>
        <source>Microsoft</source>
        <translation type="vanished">Microsoft</translation>
    </message>
    <message>
        <source>Mismatching</source>
        <translation type="vanished">Abweichung</translation>
    </message>
    <message>
        <source>Mixed color spaces inside one single document can be difficult to handle for some systems. The settings can give the option to flatten document color spaces.</source>
        <translation type="vanished">Gemischte Farbräume in einem Dokument können in einigen Systemen zu Problemen führen. Die Einstellung bietet die Möglichkeit den Farbraum zu vereinheitlichen.</translation>
    </message>
    <message>
        <source>Model</source>
        <translation type="vanished">Modell</translation>
    </message>
    <message>
        <source>Model:           </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Modell:          </translation>
    </message>
    <message>
        <source>Modify Profile:</source>
        <translation type="vanished">Verändere Prmprofil</translation>
    </message>
    <message>
        <source>Module Options</source>
        <translation type="vanished">Moduloptionen</translation>
    </message>
    <message>
        <source>Monitor</source>
        <translation type="vanished">Bildschirm</translation>
    </message>
    <message>
        <source>Monitors, which can be detected through the video card driver and windowing system.</source>
        <translation type="vanished">Bildschirme, welche durch das Modul erkannt werden.</translation>
    </message>
    <message>
        <source>More bits mean more precission for processing and more size.</source>
        <translation type="vanished">Mehr Bit bedeutet mehr Daten für höhere Genauigkeit zu verarbeiten.</translation>
    </message>
    <message>
        <source>More indepth help  for My example filter.</source>
        <translation type="vanished">Mehr Hilfetext für den Mein Beispielfilter.</translation>
    </message>
    <message>
        <source>Most users want a simple display of colors and will not check if colors match the simulation color space.</source>
        <translation type="vanished">Viele Benutzer möchten eine einfache Farbdarstellungen und wollen nicht prüfen ob Farben in den Simulationsfarbraum passen.</translation>
    </message>
    <message>
        <source>My Filter</source>
        <translation type="vanished">Mein Filter</translation>
    </message>
    <message>
        <source>My Filter Object</source>
        <translation type="vanished">Filterobjekt Mein</translation>
    </message>
    <message>
        <source>My Name</source>
        <translation type="vanished">Mein Name</translation>
    </message>
    <message>
        <source>My filter introduction.</source>
        <translation type="vanished">Meine Filter Einführung.</translation>
    </message>
    <message>
        <source>My project; www: http://www.my-adress.com; support/email: my@adress.com; sources: http://www.my-adress.com/download</source>
        <translation type="vanished">Oyranos Projekt: www: http://www.oyranos.com; Support/email: ku.b@gmx.de; Quellen: http://www.oyranos.com/wiki/index.php?title=Oyranos/Download</translation>
    </message>
    <message>
        <source>My project; www: http://www.oyranos.org; support/email: ku.b@gmx.de; sources: http://www.oyranos.org/wiki/index.php?title=Oyranos/Download</source>
        <translation type="obsolete">Oyranos Projekt: www: http://www.oyranos.com; Support/email: ku.b@gmx.de; Quellen: http://www.oyranos.com/wiki/index.php?title=Oyranos/Download</translation>
    </message>
    <message>
        <source>NAME</source>
        <translation type="vanished">NAME</translation>
    </message>
    <message>
        <source>NUMBER</source>
        <translation type="vanished">NUMMER</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Name</translation>
    </message>
    <message>
        <source>Named Color</source>
        <extracomment>nmcl
</extracomment>
        <translation type="vanished">Einzelarben</translation>
    </message>
    <message>
        <source>Named Color 2</source>
        <translation type="vanished">Einzelfarben 2</translation>
    </message>
    <message>
        <source>Navigation:</source>
        <translation type="vanished">Navigation</translation>
    </message>
    <message>
        <source>Need a ICC profile argument.</source>
        <translation type="vanished">Benötige ein ICC Profil Argument.</translation>
    </message>
    <message>
        <source>Need a ICC profile to modify.</source>
        <translation type="vanished">Benötige ein ICC Profil zum Verändern.</translation>
    </message>
    <message>
        <source>No</source>
        <extracomment>choices
</extracomment>
        <translation type="vanished">Nein</translation>
    </message>
    <message>
        <source>No ICC profile id detected</source>
        <translation type="vanished">kein ICC Profil ID gefunden</translation>
    </message>
    <message>
        <source>No Image profile</source>
        <translation type="vanished">Kein Profil am Bild</translation>
    </message>
    <message>
        <source>No White on White Fix</source>
        <translation type="vanished">Keine Weiß zu Weiß Reparatur</translation>
    </message>
    <message>
        <source>No monitor gamma curves by profile:</source>
        <extracomment>hack
</extracomment>
        <translation type="vanished">Keine Grafikkartengammakurven im Profil:</translation>
    </message>
    <message>
        <source>NoNo</source>
        <translation type="vanished">NeinNein</translation>
    </message>
    <message>
        <source>Offset Lithography</source>
        <extracomment>offs
</extracomment>
        <translation type="vanished">Offsetdruck</translation>
    </message>
    <message>
        <source>On Cmyk Mismatch</source>
        <translation type="vanished">Bei Cmyk Ungleichheit</translation>
    </message>
    <message>
        <source>On Rgb Mismatch</source>
        <translation type="vanished">Bei Rgb Ungleichheit</translation>
    </message>
    <message>
        <source>Operating System</source>
        <translation type="vanished">Betriebssystem</translation>
    </message>
    <message>
        <source>Optimization</source>
        <translation type="vanished">Optimierung</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">Option</translation>
    </message>
    <message>
        <source>Option &quot;filename&quot;, a valid filename</source>
        <translation type="vanished">Option &quot;filename&quot;, ein gültiger Dateiname</translation>
    </message>
    <message>
        <source>Option &quot;filename&quot;, a valid filename of a existing PNG image</source>
        <translation type="vanished">Option &quot;filename&quot;, ein gültiger Dateiname eines existierenden PNG Bildes</translation>
    </message>
    <message>
        <source>Option &quot;filename&quot;, a valid filename of a existing PPM image</source>
        <translation type="vanished">Option &quot;filename&quot;, ein gültiger Dateiname eines existierenden PPM Bildes</translation>
    </message>
    <message>
        <source>Option &quot;filename&quot;, a valid filename of a existing image</source>
        <translation type="vanished">Option &quot;filename&quot;, ein gültiger Dateiname eines existierenden Bildes</translation>
    </message>
    <message>
        <source>Option not supported type:</source>
        <translation type="vanished">Option nicht unterstützten Typs:</translation>
    </message>
    <message>
        <source>Output</source>
        <extracomment>prtr
</extracomment>
        <translation type="vanished">Ausgabe</translation>
    </message>
    <message>
        <source>Output Color Space</source>
        <translation type="vanished">Ausgabefarbraum</translation>
    </message>
    <message>
        <source>Output Image Filter Object</source>
        <translation type="vanished">Filterobjekt eines Ausgangsbildes</translation>
    </message>
    <message>
        <source>Oyranos </source>
        <translation type="vanished">Oyranos </translation>
    </message>
    <message>
        <source>Oyranos CUPS</source>
        <translation type="vanished">Oyranos CUPS</translation>
    </message>
    <message>
        <source>Oyranos Dummy</source>
        <translation type="vanished">Oyranos Beispiel</translation>
    </message>
    <message>
        <source>Oyranos Help</source>
        <translation type="vanished">Oyranos Hilfe</translation>
    </message>
    <message>
        <source>Oyranos ICC policy</source>
        <translation type="vanished">Oyranos ICC Richtlinie</translation>
    </message>
    <message>
        <source>Oyranos ICC policy module</source>
        <translation type="vanished">Oyranos ICC Richtlinien Modul</translation>
    </message>
    <message>
        <source>Oyranos Image Display Help</source>
        <translation type="vanished">Oyranos Bildanzeige Hilfe</translation>
    </message>
    <message>
        <source>Oyranos Image Display Info</source>
        <translation type="vanished">Oyranos Bildanzeige Info</translation>
    </message>
    <message>
        <source>Oyranos RAW Image</source>
        <translation type="vanished">Oyranos RAW Bild</translation>
    </message>
    <message>
        <source>Oyranos Scanner</source>
        <translation type="vanished">Oyranos Scanner</translation>
    </message>
    <message>
        <source>Oyranos Settings</source>
        <translation type="vanished">Oyranos Einstellungen</translation>
    </message>
    <message>
        <source>Oyranos Settings Group</source>
        <extracomment>name
</extracomment>
        <translation type="vanished">Oyranos Einstellungen</translation>
    </message>
    <message>
        <source>Oyranos Threads</source>
        <translation type="vanished">Oyranos Programmfäden</translation>
    </message>
    <message>
        <source>Oyranos Version</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Oyranos Version</translation>
    </message>
    <message>
        <source>Oyranos allowes detailed settings like preferred editing color spaces and the behaviour of color conversions or simulation. Oyranos reduces the work involved in all color management related decisions through automation, useful defaults and grouping of settings in selectable policies.</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Oyranos erlaubt detailierte Einstellungen, wie z.B. zu bevorzugten Editierfarbräumen, das Verhalten von Farbumwandlungen oder Simulation. Oyranos verringert den Aufwand für Farbmanagemententscheidungen durch Automation, sinnvolle Voreinstellungen und wählbare Richtlinien.</translation>
    </message>
    <message>
        <source>Oyranos allows to fine tune handling of ICC color management.</source>
        <translation type="vanished">Oyranos erlaubt detailierte ICC Farbmanagementeinstellungen.</translation>
    </message>
    <message>
        <source>Oyranos display filter</source>
        <translation type="vanished">Oyranos Monitor Modul</translation>
    </message>
    <message>
        <source>Oyranos is a Color Management System (CMS), which relies one the ICC file format standard (&lt;a href=&quot;http://www.color.org&quot;&gt;www.color.org&lt;/a&gt;) for color space definitions. The use of ICC color profiles shall enable a flawless and automated color data exchange between different color spaces and various devices with their respective physical color behaviours.</source>
        <extracomment>HTML
</extracomment>
        <translation type="vanished">Oyranos ist ein auf dem ICC Standard (&lt;a href=&quot;http://www.color.org&quot;&gt;www.color.org&lt;/a&gt;) für Farbraumbescheibungen beruhendes Farbmanagementsystem (CMS). Die Benutzung von ICC Farbprofilen soll einen reibungslosen und automatisierten Datenaustausch zwischen unterschiedlichen Farbräumen und den Abgleich auf verschiedenen Geräten mit ihren jeweiligen physikalischen Gegebenheiten ermöglichen.</translation>
    </message>
    <message>
        <source>Oyranos modules</source>
        <translation type="vanished">Oyranos Module</translation>
    </message>
    <message>
        <source>Oyranos project; www: http://www.oyranos.com; support/email: ku.b@gmx.de; sources: http://www.oyranos.com/wiki/index.php?title=Oyranos/Download</source>
        <translation type="vanished">Oyranos Projekt: www: http://www.oyranos.com; Support/email: ku.b@gmx.de; Quellen: http://www.oyranos.com/wiki/index.php?title=Oyranos/Download</translation>
    </message>
    <message>
        <source>Oyranos project; www: http://www.oyranos.org; support/email: ku.b@gmx.de; sources: http://www.oyranos.org/downloads/</source>
        <translation type="vanished">Oyranos Projekt: www: http://www.oyranos.org; Support/email: ku.b@gmx.de; Quellen: http://www.oyranos.org/downloads/</translation>
    </message>
    <message>
        <source>Oyranos settings let you configure ICC color management behaviour for a local computer. The settings can be per user and per system.</source>
        <extracomment>tooltip
</extracomment>
        <translation type="vanished">Oyranos Einstellungen erlauben Farbmanagementverhalten auf dem lokalen Rechner zu konfigurieren. Die Einstellungen erfolgen für den Benutzer und für das System.</translation>
    </message>
    <message>
        <source>Oyranos supplied modules</source>
        <translation type="vanished">Oyranos mitgelieferte Module</translation>
    </message>
    <message>
        <source>P22</source>
        <translation type="vanished">P22</translation>
    </message>
    <message>
        <source>PCS</source>
        <translation type="vanished">PCS</translation>
    </message>
    <message>
        <source>PCS Color Space: </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">PCS Farbraum:    </translation>
    </message>
    <message>
        <source>PROFILENAMES</source>
        <translation type="vanished">PROFILNAMEN</translation>
    </message>
    <message>
        <source>Passive matrix monitor</source>
        <extracomment>PMD
</extracomment>
        <translation type="vanished">Passivmatrixmonitor</translation>
    </message>
    <message>
        <source>Path can not be written</source>
        <translation type="vanished">Verzeichnis kann nicht beschrieben werden.</translation>
    </message>
    <message>
        <source>Paths</source>
        <translation type="vanished">Pfade</translation>
    </message>
    <message>
        <source>Paths where ICC Profiles can be found</source>
        <translation type="vanished">Pfad zu ICC Farbprofilen</translation>
    </message>
    <message>
        <source>Perceptual</source>
        <extracomment>choices
</extracomment>
        <translation type="vanished">Fotografisch</translation>
    </message>
    <message>
        <source>Photo CD</source>
        <extracomment>KPCD
</extracomment>
        <translation type="vanished">Foto CD</translation>
    </message>
    <message>
        <source>PhotoImageSetter</source>
        <extracomment>imgs
</extracomment>
        <translation type="vanished">Fotofilmbelichter</translation>
    </message>
    <message>
        <source>Photographic paper printer</source>
        <extracomment>rpho
</extracomment>
        <translation type="vanished">Photographischer Papierdrucker</translation>
    </message>
    <message>
        <source>Platform</source>
        <translation type="vanished">Plattform</translation>
    </message>
    <message>
        <source>Platform:        </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Plattform:       </translation>
    </message>
    <message>
        <source>Please specify a monitor with the -d option.</source>
        <translation type="vanished">Bitte einen Monitor mit der Option -d wählen.</translation>
    </message>
    <message>
        <source>Policies summarise common settings for different user groups. Such settings make it easy to switch back to simple defaults. Other groups can define own settings and share them accross Oyranos enabled systems.</source>
        <translation type="vanished">Richtlinien fassen gemeinsame Einstellungen für Benutzergruppen zusammen. Solche Richtlinien ermöglichen ein einfaches zurückschalten zu den jeweiligen Voreinstellungen. Gruppen können eigene Richtlinien erstellen und zwischen Oyranos unterstützenden Systemen austauschen.</translation>
    </message>
    <message>
        <source>Policy</source>
        <translation type="vanished">Richtlinie</translation>
    </message>
    <message>
        <source>Policy search paths</source>
        <translation type="vanished">Richtliniensuchpfade</translation>
    </message>
    <message>
        <source>Postscript 2 CRD, absolute colorimetric</source>
        <translation type="vanished">Postscript 2 CRD, Absolut Farbmetrisch</translation>
    </message>
    <message>
        <source>Postscript 2 CRD, perceptual</source>
        <translation type="vanished">Postscript 2 CRD, fotografisch</translation>
    </message>
    <message>
        <source>Postscript 2 CRD, relative colorimetric</source>
        <translation type="vanished">Postscript 2 CRD, relativ farbmetrisch</translation>
    </message>
    <message>
        <source>Postscript 2 CRD, saturated</source>
        <translation type="vanished">Postscript 2 CRD, farbgesättigt</translation>
    </message>
    <message>
        <source>Postscript 2 CSA</source>
        <translation type="vanished">Postscript 2 CSA</translation>
    </message>
    <message>
        <source>Postscript 2 Rendering Intent</source>
        <translation type="vanished">Postscript 2 Übertragungsart</translation>
    </message>
    <message>
        <source>Postscript CRD Information</source>
        <translation type="vanished">Postscript CRD Information</translation>
    </message>
    <message>
        <source>Prefered CMM</source>
        <translation type="vanished">Bevorzugte CMM</translation>
    </message>
    <message>
        <source>Preferred CIE*Lab Editing Color Space</source>
        <translation type="vanished">Bevorzugter CIE*Lab Editierfarbraum</translation>
    </message>
    <message>
        <source>Preferred Cmyk Editing Color Space</source>
        <translation type="vanished">Bevorzugter Cmyk Editierfarbraum</translation>
    </message>
    <message>
        <source>Preferred Gray Editing Color Space</source>
        <translation type="vanished">Bevorzugter Grau Editierfarbraum</translation>
    </message>
    <message>
        <source>Preferred Rgb Editing Color Space</source>
        <translation type="vanished">Bevorzugter Rgb Editierfarbraum</translation>
    </message>
    <message>
        <source>Preferred XYZ Editing Color Space</source>
        <translation type="vanished">Bevorzugter XYZ Editierfarbraum</translation>
    </message>
    <message>
        <source>Preserve Numbers</source>
        <extracomment>choices
</extracomment>
        <translation type="vanished">Erhalte Zahlen</translation>
    </message>
    <message>
        <source>Preview, perceptual</source>
        <translation type="vanished">Voransicht, fotografisch</translation>
    </message>
    <message>
        <source>Preview, relative colorimetric</source>
        <translation type="vanished">Voransicht, relativ farbmetrisch</translation>
    </message>
    <message>
        <source>Preview, saturated</source>
        <translation type="vanished">Voransicht, farbgesättigt</translation>
    </message>
    <message>
        <source>Print a help text:</source>
        <translation type="vanished">Zeige einen Hilfetext an:</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="vanished">Drucker</translation>
    </message>
    <message>
        <source>Printer CUPS</source>
        <translation type="vanished">CUPS Drucker</translation>
    </message>
    <message>
        <source>Printers, which are accessible through the CUPS spooling system.</source>
        <translation type="vanished">Drucker, die durch das CUPS Drucksystem erreichbar sind.</translation>
    </message>
    <message>
        <source>Profile Connection Space</source>
        <translation type="vanished">Profileverbindungsfarbraum</translation>
    </message>
    <message>
        <source>Profile Creator ID</source>
        <translation type="vanished">Profilerzeuger</translation>
    </message>
    <message>
        <source>Profile ID:      </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Profil ID:       </translation>
    </message>
    <message>
        <source>Profile Illuminant</source>
        <translation type="vanished">Profile Beleuchtung</translation>
    </message>
    <message>
        <source>Profile Information</source>
        <translation type="vanished">Profilinformation</translation>
    </message>
    <message>
        <source>Profile Tags</source>
        <translation type="vanished">Profilbausteine</translation>
    </message>
    <message>
        <source>Profile already installed</source>
        <translation type="vanished">Profil ist bereits installiert</translation>
    </message>
    <message>
        <source>Profile contains no description</source>
        <translation type="vanished">Profilbeschreibung vermisst</translation>
    </message>
    <message>
        <source>Profile description</source>
        <translation type="vanished">Profilbeschreibung</translation>
    </message>
    <message>
        <source>Profile description, multilingual</source>
        <extracomment>dscm
</extracomment>
        <translation type="vanished">Profilbeschreibung mehrsprachig</translation>
    </message>
    <message>
        <source>Profile exists already</source>
        <translation type="vanished">Profil ist bereits installiert</translation>
    </message>
    <message>
        <source>Profile not useable</source>
        <translation type="vanished">Profil nicht benutzbar</translation>
    </message>
    <message>
        <source>Profile sequence description</source>
        <translation type="vanished">Beschreibung der Profilverknüpfung</translation>
    </message>
    <message>
        <source>Profile sequence identification</source>
        <translation type="vanished">Identifizierung der Profilverknüpfung</translation>
    </message>
    <message>
        <source>Profiling parameters</source>
        <extracomment>Pmtr
</extracomment>
        <translation type="vanished">Profilierungsparameter</translation>
    </message>
    <message>
        <source>Program not found</source>
        <translation type="vanished">Programm nicht gefunden</translation>
    </message>
    <message>
        <source>Projection Television</source>
        <extracomment>pjtv
</extracomment>
        <translation type="vanished">Fernsehprojektion</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="vanished">Abfragedialog</translation>
    </message>
    <message>
        <source>Proofing</source>
        <translation type="vanished">Simulation</translation>
    </message>
    <message>
        <source>Proofing Rendering Intent</source>
        <translation type="vanished">Nachbildungsübertragung</translation>
    </message>
    <message>
        <source>Proofing Settings</source>
        <translation type="vanished">Einstellungen für Nachbildung (Simulation) von einem Ausgabegerät</translation>
    </message>
    <message>
        <source>Proofing Settings allows one to decide about the simulation color space.</source>
        <translation type="vanished">Simulationseinstellungen erlauben die Simulation eines zusätzlichen Farbraumes.</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">Beenden</translation>
    </message>
    <message>
        <source>RGB primaries missed</source>
        <translation type="vanished">RGB Primärfarben nicht gefunden</translation>
    </message>
    <message>
        <source>Raw camera data, which are in file containing raw sensor data from a camera still picture.</source>
        <translation type="vanished">Rohe Kameradaten, welche in Dateien mit unbearbeiteten Kamerasensordaten enthalten sind.</translation>
    </message>
    <message>
        <source>RawCamera</source>
        <translation type="vanished">Kamerarohdaten</translation>
    </message>
    <message>
        <source>Read</source>
        <translation type="vanished">Lese</translation>
    </message>
    <message>
        <source>Read PNG</source>
        <translation type="vanished">Lese PNG</translation>
    </message>
    <message>
        <source>Read PPM</source>
        <translation type="vanished">Lese PPM</translation>
    </message>
    <message>
        <source>Read from</source>
        <translation type="vanished">Lese von</translation>
    </message>
    <message>
        <source>Rectangles</source>
        <translation type="vanished">Rechtecke</translation>
    </message>
    <message>
        <source>Rectangles Splitter Object</source>
        <translation type="vanished">Rechteck Regionen Objekt</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="vanished">Rot</translation>
    </message>
    <message>
        <source>Red Colorant</source>
        <translation type="vanished">Rote Grundfarbe</translation>
    </message>
    <message>
        <source>Red tone reproduction curve</source>
        <translation type="vanished">Rote Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>Reflective scanner</source>
        <extracomment>rscn
</extracomment>
        <translation type="vanished">Auflichtscanner</translation>
    </message>
    <message>
        <source>Relation of positional parameters:</source>
        <translation type="vanished">Beziehung der Positionsparameter:</translation>
    </message>
    <message>
        <source>Relative Colorimetric</source>
        <extracomment>choices
</extracomment>
        <translation type="vanished">Relativ Farbmetrisch</translation>
    </message>
    <message>
        <source>Remote filter or plug not available.</source>
        <translation type="vanished">Gegenüberliegender Filter oder Stecker nicht verfügbar.</translation>
    </message>
    <message>
        <source>Remove included ICC tag:</source>
        <translation type="vanished">Lösche enthaltenes ICC Element</translation>
    </message>
    <message>
        <source>Rendering</source>
        <translation type="vanished">Übertragung</translation>
    </message>
    <message>
        <source>Rendering Intent</source>
        <translation type="vanished">Übertragungsart</translation>
    </message>
    <message>
        <source>Rendering Intent:</source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Übertragungsart: </translation>
    </message>
    <message>
        <source>Rendering intent for color space transformations.</source>
        <translation type="vanished">Übertragungsart für Farbtransformationen.</translation>
    </message>
    <message>
        <source>Rgb</source>
        <translation type="vanished">Rgb</translation>
    </message>
    <message>
        <source>Root Image</source>
        <translation type="vanished">Quellbild</translation>
    </message>
    <message>
        <source>Root Image Filter Object</source>
        <translation type="vanished">FilterObjekt eines Quellbildes</translation>
    </message>
    <message>
        <source>SGI</source>
        <translation type="vanished">SGI</translation>
    </message>
    <message>
        <source>SMPTE RP145-1994</source>
        <translation type="vanished">SMPTE RP145-1994</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="vanished">Sättigung</translation>
    </message>
    <message>
        <source>Save Mixed Color Space Documents</source>
        <translation type="vanished">Speichere Dokumente mit gemischten Farbräumen</translation>
    </message>
    <message>
        <source>Save to a new policy:</source>
        <translation type="vanished">Speichern in eine neue Richtlinie:</translation>
    </message>
    <message>
        <source>Scale Image Filter Object</source>
        <translation type="vanished">Skalierfilterobjekt</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">Scanner</translation>
    </message>
    <message>
        <source>Scanner data, which come from SANE library.</source>
        <translation type="vanished">Scanndaten von der SANE Bibliothek.</translation>
    </message>
    <message>
        <source>Screening</source>
        <translation type="vanished">Rasterung</translation>
    </message>
    <message>
        <source>Screening Description</source>
        <translation type="vanished">Rasterung Beschreibung</translation>
    </message>
    <message>
        <source>Seldom used Profile claimed Intent</source>
        <translation type="vanished">Selten benutzte profilbestimmte Übertragungsart</translation>
    </message>
    <message>
        <source>Select a CMM in Oyranos</source>
        <translation type="vanished">Wähle eine CMM in Oyranos</translation>
    </message>
    <message>
        <source>Select a color profile for adding a effect.</source>
        <translation type="vanished">Füge einen Farbeffekt hinzu.</translation>
    </message>
    <message>
        <source>Select a color profile representing a device to be simulated. This profile will only be applied if proofing is enabled.</source>
        <translation type="vanished">Diese Option wählt ein Farbprofil, welches ein zu simulierendes Gerät farblich abbildet. Das Profile wird nur wirksam bei aktivierter Simulation.</translation>
    </message>
    <message>
        <source>Select active policy:</source>
        <translation type="vanished">Wählen der Richtlinie:</translation>
    </message>
    <message>
        <source>Select the core CMM</source>
        <translation type="vanished">Wähle die Kern CMM</translation>
    </message>
    <message>
        <source>Select the processing CMM</source>
        <translation type="vanished">Wähle die Rechner CMM</translation>
    </message>
    <message>
        <source>Set ICC profiles in Oyranos for device Color Management.</source>
        <translation type="vanished">ICC Profileinstellungen in Oyranos für Gerätefarbmanagement</translation>
    </message>
    <message>
        <source>Set a X Color Management region.</source>
        <translation type="vanished">Setze eine X Color Management Region.</translation>
    </message>
    <message>
        <source>Set a X Color Management update toggle.</source>
        <translation type="vanished">Setze eine X Color Management Schalter.</translation>
    </message>
    <message>
        <source>Set new profile:</source>
        <translation type="vanished">Setze neues Profil:</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Settings affecting the Behaviour in various situations</source>
        <translation type="vanished">Einstellungen für das Verhalten unter verschiedenen Umständen</translation>
    </message>
    <message>
        <source>Settings can be grouped in Oyranos policies for easier adaption to demands, better remembering and synchronisation.</source>
        <translation type="vanished">Einstellungen können zur Vereinfachung in Richtlinien zusammengefasst werden. Diese lassen sich besser merken und einfacher austauschen.</translation>
    </message>
    <message>
        <source>Setup device:</source>
        <translation type="vanished">Initialisiere Gerät:</translation>
    </message>
    <message>
        <source>Show CIE*xy chromaticities:</source>
        <translation type="vanished">Zeige CIE*xy Primärfarben</translation>
    </message>
    <message>
        <source>Show Help:</source>
        <translation type="vanished">Zeige Hilfe:</translation>
    </message>
    <message>
        <source>Show Infos</source>
        <translation type="vanished">Zeige Infos</translation>
    </message>
    <message>
        <source>Show Profile ID:</source>
        <translation type="vanished">Zeige Profil ID:</translation>
    </message>
    <message>
        <source>Show available Device Profiles from remote Taxi DB for a selected device. The metadata from Taxi DB and from the device must fit.</source>
        <translation type="vanished">Zeige für ein gewähltes Gerät alle verfügbaren Geräteprofile aus der Internet Taxi Datenbank. Die Metadaten aus Taxi DB und vom Geräte müssen passen.</translation>
    </message>
    <message>
        <source>Show only ICC profiles, which have metadata matching a given device.</source>
        <translation type="vanished">Zeige nur Farbprofile, deren Metadaten zum Gerät passen.</translation>
    </message>
    <message>
        <source>Show only device related ICC profiles</source>
        <translation type="vanished">Zeige nur zum Gerät passende ICC Profile</translation>
    </message>
    <message>
        <source>Show which policy matches by combining all involved settings.</source>
        <translation type="vanished">Zeige an, welche Richtlinie zur Kombination aller Einstellungen passt.</translation>
    </message>
    <message>
        <source>Signal</source>
        <translation type="vanished">Signal</translation>
    </message>
    <message>
        <source>Silkscreen</source>
        <extracomment>silk
</extracomment>
        <translation type="vanished">Siebdruck</translation>
    </message>
    <message>
        <source>Simulate the output print on the monitor</source>
        <translation type="vanished">Bilde einen Ausdruck auf dem Bildschirm nach</translation>
    </message>
    <message>
        <source>Simulation/Proof Color Space</source>
        <translation type="vanished">Simulationsfarbraum</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">Größe</translation>
    </message>
    <message>
        <source>Size:            </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Größe:           </translation>
    </message>
    <message>
        <source>Skip White Point on White point alignment</source>
        <translation type="vanished">Überspringe Weiß zu Weiß überlagerung</translation>
    </message>
    <message>
        <source>So something with options.</source>
        <translation type="vanished">Etwas mit Optionen.</translation>
    </message>
    <message>
        <source>SoftProof</source>
        <translation type="vanished">Nachbildung am Bildschirm</translation>
    </message>
    <message>
        <source>Solaris</source>
        <translation type="vanished">Solaris</translation>
    </message>
    <message>
        <source>Some help for My example filter.</source>
        <translation type="vanished">Hilfe für den Mein Beispielfilter.</translation>
    </message>
    <message>
        <source>Source and Target Profiles for various situations</source>
        <translation type="vanished">Quell- und Zielprofile für verschiedene Situationen</translation>
    </message>
    <message>
        <source>Standard Observer 1931 2° Graph:</source>
        <translation type="vanished">Standard Betrachter 1931 2° Zeichnung:</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Start</translation>
    </message>
    <message>
        <source>Switch for enabling the effect profile.</source>
        <translation type="vanished">Schalter für das ausgewählte ICC Effektprofil.</translation>
    </message>
    <message>
        <source>Taligent</source>
        <translation type="vanished">Taligent</translation>
    </message>
    <message>
        <source>Taxi DB Profiles</source>
        <translation type="vanished">Verfügbare Taxiprofile</translation>
    </message>
    <message>
        <source>Taxi DB entries for device</source>
        <translation type="vanished">Taxi DB Einträge für das Gerät</translation>
    </message>
    <message>
        <source>Taxi ID</source>
        <translation type="vanished">Taxi ID</translation>
    </message>
    <message>
        <source>The </source>
        <extracomment>&quot;The &quot; CMM_NICK &quot; module supports the generic device protocol.&quot;
</extracomment>
        <translation type="vanished">Das </translation>
    </message>
    <message>
        <source>The &quot;display&quot; filter supports applications to show image content on single and multi monitor displays. It cares about the server communication in declaring the region as prematched. So a X11 server side color correction does not disturb the displayed colors and omits the provided rectangle. The &quot;display&quot; filter matches the provided image content to each monitor it can find. Of course this has limitations to distorted windows, like wobbly effects or matrix deformed windows.</source>
        <translation type="vanished">Das &quot;display&quot; Modul unterstützt Anwendungen bei der Darstellung von Bildern auf einfachen- und Mehrfachmonitorkonfigurationen. Es teilt dem X Server mit, daß ein bearbeitete Region farbkorrigiert ist. Somit wird bei einer spätenserverseitigen Farbkorrektur eine doppelte Anwendung vermieden. Das &quot;display&quot; Modul korrigiert für jeden gefundenen Monitor einzeln. Dies hat allerding Nachteile bei deformierten Regionen wie wackelnden oder verzerrten Fenstern.</translation>
    </message>
    <message>
        <source>The CUPS module supports the generic device protocol.</source>
        <translation type="vanished">Das CUPS Modul unterstützt Oyranos’ allgemeines Geräteprotokoll.</translation>
    </message>
    <message>
        <source>The CUPS/printer module for Oyranos.</source>
        <translation type="vanished">Das CUPS/Drucker Modul für Oyranos</translation>
    </message>
    <message>
        <source>The Editing Color Spaces shall be well behaving in order to allow editing of per channel curves. Well behaving means for Rgb, that editing of all three curves maintains gray neutrality. Hint: editing color spaces are sometimes called a working space.</source>
        <translation type="vanished">Der Editierfarbraum sollte wohlgeformt sein, um das Editieren von Farbkanälen zu erleichtern. Wohlgeformt meint für Rgb, daß ein gleichzeitiges Verändern aller drei Farbkanäle die Neutralität der Grauachse nicht beeinflusst.</translation>
    </message>
    <message>
        <source>The Option &quot;filename&quot; should contain a valid filename to read the image data from. If the file does not exist, a error will occure.</source>
        <translation type="vanished">Die Option &quot;filename&quot; sollte einen gültigen Dateinamen zum Lesen des Bildes enthalten. Falls die Datei nicht existiert wird ein Fehler gemeldet.
Das oyEDITING_RGB ICC Farbprofil wird angehangen.</translation>
    </message>
    <message>
        <source>The Option &quot;filename&quot; should contain a valid filename to read the png data from. If the file does not exist, a error will occure.
The iCCP chunk is searched for or a oyASSUMED_WEB/oyASSUMED_GRAY ICC profile will be attached to the resulting image. A embedded renering intent will be ignored.</source>
        <translation type="vanished">Die Option &quot;filename&quot; sollte einen gültigen Dateinamen zum Lesen des PNG Bildes enthalten. Falls die Datei nicht existiert wird ein Fehler gemeldet.
Es wird nach dem iCCP Element gesucht. Falls dieses nicht gefunden werden  kann, wird ein oyASSUMED_WEB/oyASSUMED_GRAY ICC Farbprofil an das erzeugte Bild angehangen. Eine interne Farbübertragungsart wird ignoriert.</translation>
    </message>
    <message>
        <source>The Option &quot;filename&quot; should contain a valid filename to read the ppm data from. If the file does not exist, a error will occure.
The oyEDITING_RGB ICC profile is attached.</source>
        <translation type="vanished">Die Option &quot;filename&quot; sollte einen gültigen Dateinamen zum Lesen des PPM Bildes enthalten. Falls die Datei nicht existiert wird ein Fehler gemeldet.
Das oyEDITING_RGB ICC Farbprofil wird angehangen.</translation>
    </message>
    <message>
        <source>The Option &quot;filename&quot; should contain a valid filename to write the png data into. A existing file will be overwritten without notice.</source>
        <translation type="vanished">Die Option &quot;filename&quot; sollte einen gültigen Dateinamen zum Erstellen des PNG Bildes enthalten. Eine existierende Datei wird ohne Warnung überschrieben.</translation>
    </message>
    <message>
        <source>The Option &quot;filename&quot; should contain a valid filename to write the ppm data into. A existing file will be overwritten without notice.</source>
        <translation type="vanished">Die Option &quot;filename&quot; sollte einen gültigen Dateinamen zum Erstellen des PPM Bildes enthalten. Eine existierende Datei wird ohne Warnung überschrieben.</translation>
    </message>
    <message>
        <source>The Oyranos &quot;threads_handler&quot; command will initialise the oyJob_s APIs with a available threading model.</source>
        <translation type="vanished">Das Oyranos &quot;threads_handler&quot; Kommando initialisiert die oyJob_s Schnittstellen mit einem verfügbaren parallelen Verfahren.</translation>
    </message>
    <message>
        <source>The adaption state should be between 0 and 1.0 and will apply to the absolute colorimetric intent.</source>
        <translation type="vanished">Der Farbanpassungsgrad sollte zwischen 0 und 1.0 liegen und wird auf die absolut farbmetrische Übertragung angewendet.</translation>
    </message>
    <message>
        <source>The client side window data handler of Oyranos.</source>
        <translation type="vanished">Die benutzerseitige Fensterdatenagent von Oyranos.</translation>
    </message>
    <message>
        <source>The conversion between color spaces of different shape and size can happen in various ways. The Perceptual rendering intent is usually the best choice for photographs and artistic imagery. It is used in many automatic systems. The Relative Colorimetric rendering intent provides a well-defined standard, one-to-one color mapping, but without applying white point adaption. This can lead to color clipping in case of colors falling outside the target gamut as a price for the otherwise colorimetric correctness. The Relative Colorimetric intent is often used in combination with Black Point Compensation. The Saturation rendering intent shall provide an optimal use of saturated device colors. The Absolute Colorimetric rendering intent works like the relative colorimetric one except the white point is adapted. All rendering intents beside the colorimetric ones rely on the color tables designed by the profile vendor.</source>
        <translation type="vanished">Die Umwandlung zwischen Farbräumen mit verschiedener Form und Größe kann auf verschiedene Arten stattfinden. Die Fotografische Übertragungsart ist oftmals die beste Wahl für Fotos und künstlerische Inhalte. Sie wird in vielen automatischen Systemen verwendet. Die Relativ Farbmetrische Übertragung bietet eine vorhersehbare standardisierte eins zu eins Übertragung mit unverändertem Weißpunkt. Die Relativ Farbmetrisch Übertragung wird oft zusammen mit der Schwarzpunktkompensation verwendet. Die Sättigende Übertragung soll eine optimale Ausnutzung von satten Gerätefarben ermöglichen. Die Absolut Farbmetrische Übertragung wirkt wie die Relativ Farbmetrische Übertragung, nur mit Weißpunktanpassung. Die Farbtabellen allen Übertragungsarten,  ausser die der Farbmetrischen, werden von den Profilerstellern gestaltet.</translation>
    </message>
    <message>
        <source>The core Color Matching Module (CMM) takes individual profiles and options and backes a device link. It can provide alternative features regarding appearance, security, speed or resource overhead.</source>
        <translation type="vanished">Das Kernfarbübertragungsmodul (CMM) nimmt einzelne Farbprofile und Optionen und erzeugt eine Geräteverknüpfung. Es kann alternative Eigenschaften anbieten bezüglich Farbanmutung, Sicherheit, Geschwiundigkeit oder Resourcenverbrauch.</translation>
    </message>
    <message>
        <source>The dummy useless module of Oyranos.</source>
        <translation type="vanished">Das Beispielmodul für Oyranos</translation>
    </message>
    <message>
        <source>The filter adapts pixel brightness.</source>
        <translation type="vanished">Der Filter ändert die Bildhelligkeit.</translation>
    </message>
    <message>
        <source>The filter allows to visually observe a images colours.</source>
        <translation type="obsolete">Der Filter bietet eine allgemeine Bildquelle.</translation>
    </message>
    <message>
        <source>The filter can query pixels from its successors in a graph. The results are stored in the internal buffer.</source>
        <translation type="vanished">Der Filter erfragt Rasterbildpunkte von seinem Vorgänger im Graphen. Das Ergebnis wird in internen Datenfeldern gespeichert.</translation>
    </message>
    <message>
        <source>The filter expects a &quot;expose&quot; double option and will process the data accordingly.</source>
        <translation type="vanished">Der Filter erwartet eine &quot;expose&quot; Gleitkomma Option und verarbeitet die Bilddaten entsprechend.</translation>
    </message>
    <message>
        <source>The filter is a possible pixel target in a graph.</source>
        <translation type="vanished">Der Filter ist ein mögliches Rasterbildziel in einem Graphen.</translation>
    </message>
    <message>
        <source>The filter is used to reduce pixels.</source>
        <translation type="vanished">Der Filter verringert die Pixelanzahl.</translation>
    </message>
    <message>
        <source>The filter needs some informations attached to the output image tags of the &quot;output&quot; image filter. The following list describes the X11/Xorg requirements.
 A &quot;window_id&quot; option shall consist of a oyBlob_s object containing the X11 &quot;Window&quot; type in its pointer element.
 A &quot;display_id&quot; option shall consist of a oyBlob_s object containing the X11 &quot;Display&quot; of the application. This is typically exposed as system specific pointer by each individual toolkit.
 A &quot;display_rectangle&quot; option of type oyRectangle_s shall represent the application image region in pixel of the absolute display coordinates. 
 In the &quot;datatype&quot; option, a oyDATATYPE_e encoded as integer is expected, to deliver that data type in a not yet allocated output image. The output data type is by default not changed. A newly not yet allocated output image will be stored as processing data in the socket.
 &quot;preserve_alpha&quot; is a integer option to keep a given alpha in a not yet allocated output image. </source>
        <translation type="vanished">Das Modul benötigt einige Informationen, welche es in den Bilddaten des &quot;output&quot; Bildmodules sucht. Die folgende Liste beschreibt die Bedürfnisse unter Xorg/X11.
Die &quot;window_id&quot; Option, aus einem oyBlob_s Object bestehend, soll einen X11 &quot;Window&quot; Zeiger enthalten.
Die &quot;display_id&quot; Option, aus einem oyBlob_s Object bestehend, soll einen X11 &quot;Display&quot; Zeiger enthalten. Dieser beiden obigen Zeiger werden typischerweise auch von Toolkits bereitgestellt.
Die &quot;display_rectangle&quot; Option, bestehend aus einem oyRectangle_s Objekt, soll die darzustellenden absoluten Bildschirmkoordinaten in Pixel enthalten.
 Die &quot;datatype&quot; Option, soll einen oyDATATYPE_e als Ganzzahloption enthalten, um ein noch nicht reserviertes Ausgabebild neu zu erstellen. Standardmäßig wird der Datentyp nicht geändert. Ein neu erzeugtes Bild wird als Prozessdaten an den Sockel geheftet.
 &quot;preserve_alpha&quot; ist eine Ganzzahloption die Deckkraft in einem neuen Bild zu erhalten.</translation>
    </message>
    <message>
        <source>The filter obtains a image from libraw.</source>
        <translation type="vanished">Der Filter erhält Bilder von libraw.</translation>
    </message>
    <message>
        <source>The filter provides a default oyJob_s handling mechanism for asynchronous processing.</source>
        <translation type="vanished">Der Filter bietet einen voreingestellen Mechanismus für asynchrone Ausführung an.</translation>
    </message>
    <message>
        <source>The filter provides a generic image source.</source>
        <translation type="vanished">Der Filter bietet eine allgemeine Bildquelle.</translation>
    </message>
    <message>
        <source>The filter provides policy settings. These settings can be applied to a graph through the user function oyConversion_Correct().</source>
        <translation type="vanished">Das Modul bietet Einstellungen für Richtlinien. Diese Einstelungen können mit Hilfe der oyConversion_Correct() Funktion auf einen Grafen angewendet werden.</translation>
    </message>
    <message>
        <source>The filter will expect a &quot;channel&quot; double option and will create, fill and process a according data version with a new job ticket. The new job tickets image, array and output_array_roi will be divided by the supplied &quot;channel&quot; factor. It&apos;s plug will request the divided image sizes from the source socket.</source>
        <translation type="vanished">Der Filter erwarted eine &quot;channel&quot; IEEE Double Option und wird ein passendes neues Jobticket erstellen. Das neue Jobticket erhält ein Bild, Feld und Ausgangsdatenregion, welche alle durch den &quot;scale&quot; Factor dividiert werden. Sein Stecker wird die dividierten Bildgrößen von seiner Quellsteckdose beziehen.</translation>
    </message>
    <message>
        <source>The filter will expect a &quot;rectangle&quot; option and will create, fill and process a according rectangle with a new job ticket.</source>
        <translation type="vanished">Der Filter erwartet eine &quot;rectangle&quot; Option und erstellt, füllt und verarbeitet die bezüglichen Rechtecke in jeweils eigenen Arbeitsaufträgen.</translation>
    </message>
    <message>
        <source>The filter will expect a &quot;scale&quot; double option and will create, fill and process a according data version with a new job ticket. The new job tickets image, array and output_array_roi will be divided by the supplied &quot;scale&quot; factor. It&apos;s plug will request the divided image sizes from the source socket.</source>
        <translation type="vanished">Der Filter erwarted eine &quot;scale&quot; IEEE Double Option und wird ein passendes neues Jobticket erstellen. Das neue Jobticket erhält ein Bild, Feld und Ausgangsdatenregion, welche alle durch den &quot;scale&quot; Factor dividiert werden. Sein Stecker wird die dividierten Bildgrößen von seiner Quellsteckdose beziehen.</translation>
    </message>
    <message>
        <source>The following options are available to create color contexts:
 &quot;profiles_simulation&quot;, a option of type oyProfiles_s, can contain device profiles for proofing.
 &quot;profiles_effect&quot;, a option of type oyProfiles_s, can contain abstract color profiles.
 The following Oyranos options are supported: &quot;rendering_gamut_warning&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_intent&quot;, &quot;proof_soft&quot; and &quot;proof_hard&quot;.
 The additional lcms option is supported &quot;cmyk_cmyk_black_preservation&quot; [0 - none; 1 - LCMS_PRESERVE_PURE_K; 2 - LCMS_PRESERVE_K_PLANE], &quot;precalculation&quot;: [0 - normal; 1 - cmsFLAGS_NOOPTIMIZE; 2 - cmsFLAGS_HIGHRESPRECALC, 3 - cmsFLAGS_LOWRESPRECALC], &quot;precalculation_curves&quot;: [0 - none; 1 - cmsFLAGS_CLUT_POST_LINEARIZATION + cmsFLAGS_CLUT_PRE_LINEARIZATION], &quot;adaption_state&quot;: [0.0 - not adapted to screen, 1.0 - full adapted to screen] and &quot;no_white_on_white_fixup&quot;: [0 - force white on white, 1 - keep as is].</source>
        <translation type="vanished">Die folgenden Optionen stehen für die Erstellung von Farbübertragungen zur Verfügung:
 &quot;profiles_simulation&quot;, ist eine Option des Types oyProfiles_s, welche  Geräteprofile einthalten kann zur Simulation.
 &quot;profiles_effect&quot;, ist eine Option des Types oyProfiles_s, welche  Abstrakte Profile enthalten kann.
 Die folgenden Optionen werden unterstützt: &quot;rendering_gamut_warning&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_intent&quot;, &quot;proof_soft&quot; and &quot;proof_hard&quot;.
 Die folgenden zusätzlichen lcms Optionen werden unterstützt: &quot;cmyk_cmyk_black_preservation&quot; [0 - none; 1 - LCMS_PRESERVE_PURE_K; 2 - LCMS_PRESERVE_K_PLANE], &quot;precalculation_curves&quot;: [0 - none; 1 - cmsFLAGS_CLUT_POST_LINEARIZATION + cmsFLAGS_CLUT_PRE_LINEARIZATION], &quot;adaption_state&quot;: [0.0 - not adapted to screen, 1.0 - full adapted to screen] und &quot;no_white_on_white_fixup&quot;: [0 - force white on white, 1 - keep as is].</translation>
    </message>
    <message>
        <source>The following options are available to create color contexts:
 &quot;profiles_simulation&quot;, a option of type oyProfiles_s, can contain device profiles for proofing.
 &quot;profiles_effect&quot;, a option of type oyProfiles_s, can contain abstract color profiles.
 The following Oyranos options are supported: &quot;rendering_gamut_warning&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_intent&quot;, &quot;proof_soft&quot; and &quot;proof_hard&quot;.
 The additional lcms options are supported &quot;cmyk_cmyk_black_preservation&quot; [0 - none; 1 - LCMS_PRESERVE_PURE_K; 2 - LCMS_PRESERVE_K_PLANE] and &quot;precalculation&quot;.</source>
        <translation type="vanished">Die folgenden Optionen stehen für die Erstellung von Farbübertragungen zur Verfügung:
 &quot;profiles_simulation&quot;, ist eine Option des Types oyProfiles_s, welche  Geräteprofile einthalten kann zur Simulation.
 &quot;profiles_effect&quot;, ist eine Option des Types oyProfiles_s, welche  Abstrakte Profile enthalten kann.
 Die folgenden Optionen werden unterstützt: &quot;rendering_gamut_warning&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_intent&quot;, &quot;proof_soft&quot; and &quot;proof_hard&quot;.
 The additional lcms option is supported &quot;cmyk_cmyk_black_preservation&quot; [0 - none; 1 - LCMS_PRESERVE_PURE_K; 2 - LCMS_PRESERVE_K_PLANE].</translation>
    </message>
    <message>
        <source>The kind of ICC gamut mapping for transforming colors between differently sized color spaces</source>
        <translation type="vanished">Die Art der Farbübertragung für verschieden große Farbräume nach ICC Standard</translation>
    </message>
    <message>
        <source>The lcms &quot;color.icc&quot; filter is a one dimensional color conversion filter. It can both create a color conversion context, some precalculated for processing speed up, and the color conversion with the help of that context. The adaption part of this filter transforms the Oyranos color context, which is ICC device link based, to the internal lcms format.</source>
        <translation type="vanished">Das lcms &quot;color.icc&quot; Modul kann eindimensional Farben übertragen. Die CMM, Farbübertragungsmodul, berechnet die Farbübertragungsdaten vor und kann Farben mit deren Hilfe auch übertragen. Es gibt einen Anpassungsteil im Modul, welcher die interne lcms Farbübertragungsdaten an das Oyranos&apos; Format anpasst, welches auf ICC Geräteverknüpfungen, engl. device links, beruht.</translation>
    </message>
    <message>
        <source>The lcms &quot;color_icc&quot; filter is a one dimensional color conversion filter. It can both create a color conversion context, some precalculated for processing speed up, and the color conversion with the help of that context. The adaption part of this filter transforms the Oyranos color context, which is ICC device link based, to the internal lcms format.</source>
        <translation type="vanished">Das lcms &quot;color_icc&quot; Modul kann eindimensional Farben übertragen. Die CMM, Farbübertragungsmodul, berechnet die Farbübertragungsdaten vor und kann Farben mit deren Hilfe auch übertragen. Es gibt einen Anpassungsteil im Modul, welcher die interne lcms Farbübertragungsdaten an das Oyranos&apos; Format anpasst, welches auf ICC Geräteverknüpfungen, engl. device links, beruht.</translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.color_matrix&quot; command lets you create ICC profiles from some given colorimetric coordinates. See the &quot;create_profile&quot; info item.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.color_matrix&quot; Kommando erlaubt die Erstellung eines ICC Farbprofiles von farbmetrischen Koordinaten. Siehe auch den &quot;create_profile&quot; Info Eintrag. </translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.color_matrix&quot; command lets you create ICC profiles from some given colorimetric coordinates. The filter expects a oyOption_s object with name &quot;color_matrix.redx_redy_greenx_greeny_bluex_bluey_whitex_whitey_gamma&quot; containing 9 floats in the order of CIE*x for red, CIE*y for red, CIE*x for green, CIE*y for green, CIE*x for blue, CIE*y for blue, CIE*x for white, CIE*y for white and a gamma value.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.color_matrix&quot; Kommando erlaubt die Erstellung eines ICC Farbprofiles von farbmetrischen Koordinaten der Primärfarben. Der Filter erwartet ein Objekt des Types oyOptions_s mit dem Namen &quot;color_matrix.redx_redy_greenx_greeny_bluex_bluey_whitex_whitey_gamma&quot;. Dieses Wert soll aus 9 Fließkommazahlen bestehen in der Reihenfolge: CIE*x für Rot, CIE*y für Rot, CIE*x für Grün, CIE*y für Grün, CIE*x für Blau, CIE*y für Blau, CIE*x für Weiß, CIE*y für Weiß und ein Gamma Wert.</translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.color_matrix&quot; command lets you create ICC profiles from some given colorimetric coordinates. The filter expects a oyOption_s object with name &quot;color_matrix.redx_redy_greenx_greeny_bluex_bluey_whitex_whitey_gamma&quot; containing 9 floats in the order of CIE*x for red, CIE*y for red, CIE*x for green, CIE*y for green, CIE*x for blue, CIE*y for blue, CIE*x for white, CIE*y for white and a gamma value. The result will appear in &quot;icc_profile&quot; with the additional attributes &quot;create_profile.color_matrix&quot;.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.color_matrix&quot; Kommando erlaubt die Erstellung eines ICC Farbprofiles von farbmetrischen Koordinaten der Primärfarben. Der Filter erwartet ein Objekt des Types oyOptions_s mit dem Namen &quot;color_matrix.redx_redy_greenx_greeny_bluex_bluey_whitex_whitey_gamma&quot;. Dieses Wert soll aus 9 Fließkommazahlen bestehen in der Reihenfolge: CIE*x für Rot, CIE*y für Rot, CIE*x für Grün, CIE*y für Grün, CIE*x für Blau, CIE*y für Blau, CIE*x für Weiß, CIE*y für Weiß und ein Gamma Wert. Das Ergebnis wird in &quot;icc_profile&quot; mit den Attributen &quot;create_profile.color_matrix&quot; erscheinen.</translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.proofing_effect&quot; command lets you create ICC abstract profiles from a given ICC profile for proofing. The filter expects a oyOption_s object with name &quot;proofing_profile&quot; containing a oyProfile_s as value. The options &quot;rendering_intent&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_gamut_warning&quot;, &quot;precalculation&quot; and &quot;cmyk_cmyk_black_preservation&quot; are honoured. The result will appear in &quot;icc_profile&quot; with the additional attributes &quot;create_profile.proofing_effect&quot; as a oyProfile_s object.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.proofing_effect&quot; Kommando erlaubt die Erstellung eines abstrakten ICC Farbprofiles von von einem gegebenen Farbprofil für Garätesimulationen. Der Filter erwartet ein Objekt des Types oyProfile_s mit dem Namen &quot;proofing_profile&quot;. Die Optionen &quot;rendering_intent&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_gamut_warning&quot;, &quot;precalculation&quot; und &quot;cmyk_cmyk_black_preservation&quot; werden berücksichtigt. Das Ergebnis wird in &quot;icc_profile&quot; mit den zusätzlichen Attributen &quot;create_profile.proofing_effect&quot; erscheinen.</translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.proofing_effect&quot; command lets you create ICC abstract profiles from a given ICC profile for proofing. The filter expects a oyOption_s object with name &quot;proofing_profile&quot; containing a oyProfile_s as value. The options &quot;rendering_intent&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_gamut_warning&quot;, &quot;precalculation&quot;, &quot;precalculation_curves&quot;, &quot;cmyk_cmyk_black_preservation&quot;, &quot;adaption_state&quot;  and &quot;no_white_on_white_fixup&quot; are honoured. The result will appear in &quot;icc_profile&quot; with the additional attributes &quot;create_profile.proofing_effect&quot; as a oyProfile_s object.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.proofing_effect&quot; Kommando erlaubt die Erstellung eines abstrakten ICC Farbprofiles von von einem gegebenen Farbprofil für Garätesimulationen. Der Filter erwartet ein Objekt des Types oyProfile_s mit dem Namen &quot;proofing_profile&quot;. Die Optionen &quot;rendering_intent&quot;, &quot;rendering_intent_proof&quot;, &quot;rendering_bpc&quot;, &quot;rendering_gamut_warning&quot;, &quot;precalculation&quot;, &quot;precalculation_curves&quot;, &quot;cmyk_cmyk_black_preservation&quot;, &quot;adaption_state&quot; und &quot;no_white_on_white_fixup&quot; werden berücksichtigt. Das Ergebnis wird in &quot;icc_profile&quot; mit den zusätzlichen Attributen &quot;create_profile.proofing_effect&quot; as ein oyProfile_s Objekt erscheinen.</translation>
    </message>
    <message>
        <source>The littleCMS &quot;create_profile.proofing_effect&quot; command lets you create ICC abstract profiles from some given ICC profile. See the &quot;proofing_effect&quot; info item.</source>
        <translation type="vanished">Das littleCMS &quot;create_profile.proofing_effect&quot; Kommando erlaubt die Erstellung eines abstrakten ICC Farbprofiles von einem Gerätefarbprofil. Siehe auch den &quot;proofing_effect&quot; Info Eintrag. </translation>
    </message>
    <message>
        <source>The module is responsible for many settings in the Oyranos color management settings panel. If applied the module care about rendering intents, simulation, mixed color documents and default profiles.</source>
        <translation type="vanished">Das Modul ist für viele Einstellungen in Oyranos’ Farbmanagementeinstellungsdialog verantwortlich. Falls benutzt kümmert sich das Modul automatisch um Farbübertragungsart, Simulation, gemischte Dokumente und voreingestellte Profile.</translation>
    </message>
    <message>
        <source>The oyX1 modules &quot;send_native_update_event&quot; handler lets you ping X Color Management advanced X11 atom. The implementation uses Xlib.</source>
        <translation type="vanished">Das &quot;send_native_update_event&quot; Kommando des oyX1 Modules setzt X Color Management kompatible erweitert X11 atom. Die Implementation nutzt Xlib.</translation>
    </message>
    <message>
        <source>The oyX1 modules &quot;set_xcm_region&quot; handler lets you set X Color Management compatible client side color regions. The implementation uses libXcm and Oyranos.</source>
        <translation type="vanished">Das &quot;set_xcm_region&quot; Kommando des oyX1 Modules setzt X Color Management kompatible nutzerbestimmte Farbbereiche. Die Implementation nutzt libXcm und Oyranos.</translation>
    </message>
    <message>
        <source>The preferred CIE*Lab Editing Color Space shall describe the CIE*Lab.</source>
        <translation type="vanished">Der bevorzugter CIE*Lab Editierfarbraum soll CIE*Lab beschreiben.</translation>
    </message>
    <message>
        <source>The preferred Cmyk Editing Color Space should represent a color space that complies to well defined printing conditions like FOGRA or SWOP.</source>
        <translation type="vanished">Der bevorzugte Cmyk Editierfarbraum sollte standard Druckbedingungen wie FOGRA oder SWOP representieren.</translation>
    </message>
    <message>
        <source>The preferred Gray Editing Color Space shall describe a single lightness channel color space for grayscale images.</source>
        <translation type="vanished">Der bevorzugte Grautonfarbraum soll Farben mit einem einzigen Helligkeitsfarbkanal beschreiben.</translation>
    </message>
    <message>
        <source>The preferred Rgb Editing Color Space should represent a well behaving color space like sRGB.</source>
        <translation type="vanished">Der bevorzugte Rgb Editierfarbraum sollte wohlgeformt sein.</translation>
    </message>
    <message>
        <source>The preferred XYZ Editing Color Space shall describe CIE*XYZ.</source>
        <translation type="vanished">Der bevorzugter XYZ Editierfarbraum soll CIE*XYZ definieren.</translation>
    </message>
    <message>
        <source>The processing Color Matching Module (CMM) takes one device link as argument. It can provide alternative features regarding security, speed or resource overhead.</source>
        <translation type="vanished">Das Rechnende Farbübertragungsmodul (CMM) nimmt eine Geräteverknüpfung als Argument. Es kann alternative Eigenschaften anbieten bezüglich Sicherheit, Geschwiundigkeit oder Resourcenverbrauch.</translation>
    </message>
    <message>
        <source>The raw image backend of Oyranos.</source>
        <translation type="vanished">Das KameraRAW Bild Modul für Oyranos.</translation>
    </message>
    <message>
        <source>The root image filter can hold pixel data for processing in a graph.</source>
        <translation type="vanished">Der Quellbildfilter kann Rasterdaten zur Bearbeitung in einen Graphen enthalten.</translation>
    </message>
    <message>
        <source>The scanner (hopefully)usefull backend of Oyranos.</source>
        <translation type="vanished">The Scanner Modul für Oyranos.</translation>
    </message>
    <message>
        <source>The selected color space will be assigned to untagged CIE*Lab color content and define colors for further processing.</source>
        <translation type="vanished">Der gewählte Farbraum wird CIE*Lab ohne ICC Farbprofil zugewiesen werden und Farben für die weitere Verarbeitung bestimmen.</translation>
    </message>
    <message>
        <source>The selected color space will be assigned to untagged Cmyk color content and define colors for further processing.</source>
        <translation type="vanished">Der gewählte Farbraum wird Cmyk ohne ICC Farbprofil zugewiesen werden und Farben für die weitere Verarbeitung bestimmen.</translation>
    </message>
    <message>
        <source>The selected color space will be assigned to untagged Gray color content and define colors for further processing.</source>
        <translation type="vanished">Der gewählte Farbraum wird einen Helligkeitsfarbraum ohne ICC Farbprofil zugewiesen werden und Farben für die weitere Verarbeitung bestimmen.</translation>
    </message>
    <message>
        <source>The selected color space will be assigned to untagged Rgb color content and define colors for further processing. Typical sRGB should be assumed.</source>
        <translation type="vanished">Der gewählte Farbraum wird Rgb ohne ICC Farbprofil zugewiesen werden und Farben für die weitere Verarbeitung bestimmen.</translation>
    </message>
    <message>
        <source>The selected color space will be assigned to untagged XYZ color content and define colors for further processing.</source>
        <translation type="vanished">Der gewählte Farbraum wird einem XYZ ohne ICC Farbprofil zugewiesen werden und Farben für die weitere Verarbeitung bestimmen.</translation>
    </message>
    <message>
        <source>The window support module of Oyranos.</source>
        <translation type="vanished">Die Fenstersystemunterstützung für Oyranos</translation>
    </message>
    <message>
        <source>Thermal wax printer</source>
        <extracomment>twax
</extracomment>
        <translation type="vanished">Thermischer Wachsdrucker</translation>
    </message>
    <message>
        <source>This setting decides what to do in the case that colors have no color space assigned. Typically the according assumed ICC profile should be assigned.</source>
        <translation type="vanished">Die Einstellung entscheidet was mit Farben ohne Farbprofil geschehen soll. Typischerweise wird das vermutete Farbprofil automatisch zugewiesen.</translation>
    </message>
    <message>
        <source>Type:            </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Klasse:          </translation>
    </message>
    <message>
        <source>Unassign profile from device:</source>
        <translation type="vanished">Löse Profil vom Gerät:</translation>
    </message>
    <message>
        <source>Under Color Removal (UCR)</source>
        <translation type="vanished">Unterfarbreduktion (UCR)</translation>
    </message>
    <message>
        <source>Unexpected object type:</source>
        <translation type="vanished">Unerwarteter Objekttyp:</translation>
    </message>
    <message>
        <source>Unix and derivatives</source>
        <translation type="vanished">Unix und unixartige</translation>
    </message>
    <message>
        <source>Unset device:</source>
        <translation type="vanished">Setze Gerät zurück:</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation type="vanished">Benutzung</translation>
    </message>
    <message>
        <source>Use Black Point Compensation</source>
        <translation type="vanished">Benutze Schwarzpunktkompensation (BPC)</translation>
    </message>
    <message>
        <source>Use Effect</source>
        <translation type="vanished">Benutze Effekt</translation>
    </message>
    <message>
        <source>Using JSON</source>
        <translation type="vanished">Benute JSON</translation>
    </message>
    <message>
        <source>V2toV4</source>
        <translation type="vanished">V2 nach V4</translation>
    </message>
    <message>
        <source>V4toV2</source>
        <translation type="vanished">V4 nach V2</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Wert</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <source>Version:         </source>
        <extracomment>keep total number of chars equal to original for cli print
</extracomment>
        <translation type="vanished">Version:         </translation>
    </message>
    <message>
        <source>Video Monitor</source>
        <extracomment>vidm
</extracomment>
        <translation type="vanished">Video Bildschirm</translation>
    </message>
    <message>
        <source>Video camera</source>
        <extracomment>vidc
</extracomment>
        <translation type="vanished">Video Kamera</translation>
    </message>
    <message>
        <source>VideoCardGammaTable</source>
        <extracomment>vcgt 1986226036
</extracomment>
        <translation type="vanished">Grafik Karten Gamma Tabelle (vcgt)</translation>
    </message>
    <message>
        <source>View:</source>
        <translation type="vanished">Ansicht:</translation>
    </message>
    <message>
        <source>Viewing Conditions</source>
        <translation type="vanished">Betrachtungsbedingungen</translation>
    </message>
    <message>
        <source>Viewing conditions description</source>
        <translation type="vanished">Beschreibung der Betrachtungbedingungen</translation>
    </message>
    <message>
        <source>WARNING</source>
        <translation type="vanished">WARNUNG</translation>
        <extra-po-flags>c-format</extra-po-flags>
    </message>
    <message>
        <source>Well behaving Color Space for Editing</source>
        <translation type="vanished">Gleichmäßiger Farbraum für die Bearbeitung</translation>
    </message>
    <message>
        <source>Write</source>
        <translation type="vanished">Schreibe</translation>
    </message>
    <message>
        <source>Write PNG</source>
        <translation type="vanished">Schreibe PNG</translation>
    </message>
    <message>
        <source>Write PNG Image Filter Object</source>
        <translation type="vanished">Filterobjekt zum Schreiben eines PNG Bildes</translation>
    </message>
    <message>
        <source>Write PPM</source>
        <translation type="vanished">Schreibe PPM</translation>
    </message>
    <message>
        <source>Write PPM Image Filter Object</source>
        <translation type="vanished">Filterobjekt zum Schreiben eines PPM Bildes</translation>
    </message>
    <message>
        <source>Write to</source>
        <translation type="vanished">Schreibe nach</translation>
    </message>
    <message>
        <source>Write to ICC profile:</source>
        <translation type="vanished">Schreibe ICC Profil:</translation>
    </message>
    <message>
        <source>Wrong ICC profile id detected</source>
        <translation type="vanished">Fehler in ICC Profil ID gefunden!</translation>
    </message>
    <message>
        <source>XRandR CrtcInfo request failed</source>
        <translation type="vanished">XRandR CrtcInfo Anfrage daneben</translation>
    </message>
    <message>
        <source>XYZ</source>
        <translation type="vanished">XYZ</translation>
    </message>
    <message>
        <source>XYZ2FloatPCS</source>
        <translation type="vanished">XYZ nach Fließkomma PCS</translation>
    </message>
    <message>
        <source>XYZ2Lab</source>
        <extracomment>Custom from here, not in the ICC Spec
</extracomment>
        <translation type="vanished">XYZ nach Lab</translation>
    </message>
    <message>
        <source>Xinerama request failed</source>
        <translation type="vanished">Xineramaanfrage daneben</translation>
    </message>
    <message>
        <source>YCbCr</source>
        <translation type="vanished">YCbCr</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="vanished">Gelb</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>You can get them from http://sf.net/projects/openicc</source>
        <translation type="vanished">Sie können es erhalten von http://sf.net/projects/openicc</translation>
    </message>
    <message>
        <source>Yxy</source>
        <translation type="vanished">Yxy</translation>
    </message>
    <message>
        <source>Zero profiles found in Taxi ICC DB</source>
        <translation type="vanished">Keine Profile in Taxi ICC DB gefunden</translation>
    </message>
    <message>
        <source>Zoom:</source>
        <translation type="vanished">Vergrößern:</translation>
    </message>
    <message>
        <source>[none]</source>
        <extracomment>* @brief none profile
*
*  The name of a non existent profile.
*
*  @version Oyranos: 0.1.8
*  @date    2008/02/06
*  @since   2008/02/06 (Oyranos: 0.1.8)

</extracomment>
        <translation type="vanished">[kein]</translation>
    </message>
    <message>
        <source>abstract class</source>
        <translation type="vanished">Abstrakter Farbraum</translation>
    </message>
    <message>
        <source>add prefix</source>
        <translation type="vanished">füge Vorsilbe hinzu</translation>
    </message>
    <message>
        <source>allocation failed</source>
        <translation type="vanished">kein Speicher</translation>
    </message>
    <message>
        <source>alternatively specify the name of a device</source>
        <translation type="vanished">geben Sie alternativ den Namen des Gerätes an</translation>
    </message>
    <message>
        <source>attempt to add a incomplete filter</source>
        <translation type="vanished">Versuch einen unvollständigen Filter hinzuzufügen</translation>
    </message>
    <message>
        <source>can be file name, internal description string, ICC profile ID or wildcard &quot;rgb&quot;, &quot;cmyk&quot;, &quot;gray&quot;, &quot;lab&quot;, &quot;xyz&quot;, &quot;web&quot;, &quot;rgbi&quot;, &quot;cmyki&quot;, &quot;grayi&quot;, &quot;labi&quot;, &quot;xyzi&quot;. Wildcards ending with &quot;i&quot; are assumed profiles. &quot;web&quot; is a sRGB profile. The other wildcards are editing profiles.</source>
        <translation type="vanished">kann ein Dateiname, interner Name, ICC Profil ID oder ein Namenkürzel sein &quot;rgb&quot;, &quot;cmyk&quot;, &quot;gray&quot;, &quot;lab&quot;, &quot;xyz&quot;, &quot;web&quot;, &quot;rgbi&quot;, &quot;cmyki&quot;, &quot;grayi&quot;, &quot;labi&quot;, &quot;xyzi&quot;. Kürzel endend auf &quot;i&quot; sind vermutete Profile. &quot;web&quot; ist ein sRGB Profile. Die anderen Kürzel sind Editierprofile.</translation>
    </message>
    <message>
        <source>check</source>
        <translation type="vanished">prüfe</translation>
    </message>
    <message>
        <source>color space class</source>
        <translation type="vanished">Farbraum</translation>
    </message>
    <message>
        <source>create OpenICC device color state JSON</source>
        <translation type="vanished">erzeuge OpenICC Gerätefarbeinstellungs JSON</translation>
    </message>
    <message>
        <source>create OpenICC device color state JSON including the rank map</source>
        <translation type="vanished">erzeuge OpenICC Gerätefarbeinstellungs JSON mit Wichtungstabelle</translation>
    </message>
    <message>
        <source>create OpenICC device color state rank map JSON</source>
        <translation type="vanished">erzeuge OpenICC Wichtungstabelle JSON zu Gerätefarbeinstellungen</translation>
    </message>
    <message>
        <source>create fallback ICC profile</source>
        <translation type="vanished">erzeuge alternatives ICC Profil</translation>
    </message>
    <message>
        <source>default profile type does not exist:</source>
        <translation type="vanished">Standard Profiltyp existiert nicht:</translation>
    </message>
    <message>
        <source>description</source>
        <translation type="vanished">Beschreibung</translation>
    </message>
    <message>
        <source>device class</source>
        <translation type="vanished">Geräteklasse</translation>
    </message>
    <message>
        <source>device position start from zero</source>
        <translation type="vanished">Geräteposition started von Null</translation>
    </message>
    <message>
        <source>device_class not found:</source>
        <translation type="vanished">Geräteklasse nicht gefunden:</translation>
    </message>
    <message>
        <source>display class</source>
        <translation type="vanished">Monitor</translation>
    </message>
    <message>
        <source>display name</source>
        <translation type="vanished">Anzeigename</translation>
    </message>
    <message>
        <source>download ID from Taxi data base</source>
        <translation type="vanished">Identifikation in Taxi Datenbank</translation>
    </message>
    <message>
        <source>draw gray</source>
        <translation type="vanished">zeichne grau</translation>
    </message>
    <message>
        <source>dump OpenICC device color state rank map JSON</source>
        <extracomment>JSON is a text based file format for exchange of data. The rank map is used to sort color profiles according to their fit to a specific device.
</extracomment>
        <translation type="vanished">erzeuge OpenICC Wichtungstabelle JSON zu Gerätefarbeinstellungen</translation>
    </message>
    <message>
        <source>embed OpenICC device JSON from file</source>
        <translation type="vanished">bette OpenICC JSON Gerät von Datei ein</translation>
    </message>
    <message>
        <source>embedd device and driver information into ICC meta tag</source>
        <translation type="vanished">bette Geräte-und Treiberinformationen in ICC meta Block ein</translation>
    </message>
    <message>
        <source>enable simple defaults</source>
        <translation type="vanished">benutze einfache Grundeinstellungen</translation>
    </message>
    <message>
        <source>error in module:</source>
        <translation type="vanished">Fehler in Modul:</translation>
    </message>
    <message>
        <source>extract ICC color profile</source>
        <translation type="vanished">extrahiere ICC Farbprofil</translation>
    </message>
    <message>
        <source>f - fit to window</source>
        <translation type="vanished">f - Passe Bild ins Fenster ein</translation>
    </message>
    <message>
        <source>failed!</source>
        <translation type="vanished">daneben!</translation>
    </message>
    <message>
        <source>found issues</source>
        <translation type="vanished">fand Probleme</translation>
    </message>
    <message>
        <source>found issues parsing JSON</source>
        <translation type="vanished">Fand Probleme beim Lesen des JSON</translation>
    </message>
    <message>
        <source>found list of wrong type</source>
        <translation type="vanished">fand Liste falschen Typs</translation>
    </message>
    <message>
        <source>found no observer</source>
        <translation type="vanished">keinen Beobachter gefunden</translation>
    </message>
    <message>
        <source>found observer of wrong type</source>
        <translation type="vanished">Fand Beobachter falschen Typs</translation>
    </message>
    <message>
        <source>found profiles for device class</source>
        <translation type="vanished">fand Farbprofil für Geräteklasse</translation>
    </message>
    <message>
        <source>full path and file name</source>
        <translation type="vanished">kompletter Pfad- und Dateiname</translation>
    </message>
    <message>
        <source>gamut</source>
        <translation type="vanished">Farbumfang</translation>
    </message>
    <message>
        <source>generate OpenICC device color reproduction JSON</source>
        <translation type="vanished">erzeuge OpenICC JSON zu Gerätefarbeinstellungen</translation>
    </message>
    <message>
        <source>h - fit to window height</source>
        <translation type="vanished">h - Passe Bildhöhe in Fenster ein</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">Hilfe</translation>
    </message>
    <message>
        <source>input class</source>
        <translation type="vanished">Eingabefarbraum</translation>
    </message>
    <message>
        <source>install in the OpenIccDirectory icc path</source>
        <translation type="vanished">Installieren Sie in den OpenIccDirectory icc Pfad</translation>
    </message>
    <message>
        <source>installation of new policy file failed with error:</source>
        <translation type="vanished">Speicherung der neuen Richtline schlug fehl mit Fehler:</translation>
    </message>
    <message>
        <source>installed new policy</source>
        <translation type="vanished">neue Richtlinie erfolgreich gespeichert</translation>
    </message>
    <message>
        <source>internal name</source>
        <translation type="vanished">interner Name</translation>
    </message>
    <message>
        <source>is a CameraRaw conversion tool</source>
        <translation type="vanished">ist ein Kamera Rohdatenumwandlungswerkzeug</translation>
    </message>
    <message>
        <source>is a ICC color conversion tool</source>
        <translation type="vanished">ist ein Farbumwandlungswerkzeug</translation>
    </message>
    <message>
        <source>is a ICC color profile grapher</source>
        <translation type="vanished">ist ein ICC Farbprofile Zeichner</translation>
    </message>
    <message>
        <source>is a ICC profile information tool</source>
        <translation type="vanished">ist ein ICC Profilinformationswerkzeug</translation>
    </message>
    <message>
        <source>is a Taxi DB tool</source>
        <translation type="vanished">ist ein Taxi DB Werkzeug</translation>
    </message>
    <message>
        <source>is a color profile administration tool for color devices</source>
        <translation type="vanished">ist ein Farbprofilverwaltungswerkzeug für Farbgeräte</translation>
    </message>
    <message>
        <source>is a color profile administration tool for monitors</source>
        <translation type="vanished">ist ein Farbprofilverwaltungswerkzeug für Bildschirme</translation>
    </message>
    <message>
        <source>is a policy administration tool</source>
        <translation type="vanished">ist ein Richtlinienverwaltungswerkzeug</translation>
    </message>
    <message>
        <source>less Xinerama monitors than XRandR ones</source>
        <translation type="vanished">weniger Xinerama als XRandR Monitore</translation>
    </message>
    <message>
        <source>levels from 4-16 make sense</source>
        <translation type="vanished">Werte von 4-16 sind sinnvoll</translation>
    </message>
    <message>
        <source>libraw input filter</source>
        <translation type="vanished">LibRaw Eingangsmodul</translation>
    </message>
    <message>
        <source>linux system path</source>
        <translation type="vanished">Linux Systempfad</translation>
    </message>
    <message>
        <source>littleCMS</source>
        <translation type="vanished">littleCMS</translation>
    </message>
    <message>
        <source>littleCMS 2 project; www: http://www.littlecms.com; support/email: support@littlecms.com; sources: http://www.littlecms.com/downloads.htm; Oyranos wrapper: Kai-Uwe Behrmann for the Oyranos project</source>
        <translation type="vanished">littleCMS 2 Projekt; www: http://www.littlecms.com; Support/email: support@littlecms.com; Quellen: http://www.littlecms.com/downloads.htm; Oyranos Einbindung: Kai-Uwe Behrmann</translation>
    </message>
    <message>
        <source>littleCMS project; www: http://www.littlecms.com; support/email: support@littlecms.com; sources: http://www.littlecms.com/downloads.htm; Oyranos wrapper: Kai-Uwe Behrmann for the Oyranos project</source>
        <translation type="vanished">littleCMS Projekt; www: http://www.littlecms.com; Support/email: support@littlecms.com; Quellen: http://www.littlecms.com/downloads.htm; Oyranos Einbindung: Kai-Uwe Behrmann</translation>
    </message>
    <message>
        <source>machine specific path</source>
        <translation type="vanished">rechnerspezifischer Pfad</translation>
    </message>
    <message>
        <source>match value</source>
        <translation type="vanished">Übereinstimmungswert</translation>
    </message>
    <message>
        <source>missed -w option to write a ICC profile</source>
        <translation type="vanished">vermisse -w Option für das Schreiben eines ICC Profiles</translation>
    </message>
    <message>
        <source>module name</source>
        <translation type="vanished">Modulname</translation>
    </message>
    <message>
        <source>name longer than</source>
        <translation type="vanished">Name ist länger als</translation>
    </message>
    <message>
        <source>named color class</source>
        <translation type="vanished">Einzelfarben 2</translation>
    </message>
    <message>
        <source>new BSD license: http://www.opensource.org/licenses/BSD-3-Clause</source>
        <translation type="vanished">Neue BSD Lizenz: http://www.opensource.org/licenses/BSD-3-Clause</translation>
    </message>
    <message>
        <source>new BSD license: http://www.opensource.org/licenses/bsd-license.php</source>
        <translation type="obsolete">MIT Lizenz: http://www.opensource.org/licenses/mit-license.php</translation>
    </message>
    <message>
        <source>no EDID available from</source>
        <translation type="vanished">kein EDID verfügbari von</translation>
    </message>
    <message>
        <source>no EDID available from X properties</source>
        <translation type="vanished">kein EDID von X Atomen verfügbar</translation>
    </message>
    <message>
        <source>no Screen found</source>
        <translation type="vanished">keine Anzeige gefunden</translation>
    </message>
    <message>
        <source>no oyX1Monitor_s created</source>
        <translation type="vanished">kein oyX1Monitor_s erzeugt</translation>
    </message>
    <message>
        <source>no qarzMonitor_s from</source>
        <translation type="vanished">kein qarzMonitor_s von</translation>
    </message>
    <message>
        <source>no wcsDMonitor_s created</source>
        <translation type="vanished">kein wcsDMonitor_s erzeugt</translation>
    </message>
    <message>
        <source>not a profile:</source>
        <translation type="vanished">kein Profil:</translation>
    </message>
    <message>
        <source>not found:</source>
        <translation type="vanished">nicht gefunden:</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">gut </translation>
    </message>
    <message>
        <source>omit border</source>
        <translation type="vanished">lasse Rand aus</translation>
    </message>
    <message>
        <source>omit the spectral line</source>
        <translation type="vanished">lasse Spektrallinie fort</translation>
    </message>
    <message>
        <source>omit white line of lambert light emitters</source>
        <translation type="vanished">lasse Weißlinie von Lambertstrahlern fort</translation>
    </message>
    <message>
        <source>open X Display failed</source>
        <translation type="vanished">Öffnen des X Displays daneben</translation>
    </message>
    <message>
        <source>open X Screen failed</source>
        <translation type="vanished">Öffnen des X Screens daneben</translation>
    </message>
    <message>
        <source>output class</source>
        <translation type="vanished">Ausgabefarbraum</translation>
    </message>
    <message>
        <source>output image</source>
        <translation type="vanished">Ausgabebild</translation>
    </message>
    <message>
        <source>output_array_roi</source>
        <extracomment>output image region of interesst
</extracomment>
        <translation type="vanished">Ausgabedatenregion</translation>
    </message>
    <message>
        <source>oyranos install path</source>
        <translation type="vanished">Oyranos Installationspfad</translation>
    </message>
    <message>
        <source>path is a link: ignored</source>
        <translation type="vanished">Pfad ist eine Verknüpfung: ignoriert</translation>
    </message>
    <message>
        <source>path is not a directory</source>
        <translation type="vanished">Pfad ist kein Verzeichnis</translation>
    </message>
    <message>
        <source>path is not readable</source>
        <translation type="vanished">Pfad ist nicht lesbar</translation>
    </message>
    <message>
        <source>policy name</source>
        <translation type="vanished">Richtlinienname</translation>
    </message>
    <message>
        <source>policy or filename</source>
        <translation type="vanished">Richtlinie oder Dateiname</translation>
    </message>
    <message>
        <source>print only the file name</source>
        <extracomment>--short argument
</extracomment>
        <translation type="vanished">zeige nur den Profilnamen</translation>
    </message>
    <message>
        <source>print only the profile name</source>
        <extracomment>--short argument
</extracomment>
        <translation type="vanished">zeige nur den Profilnamen</translation>
    </message>
    <message>
        <source>print the full file name</source>
        <extracomment>--path argument
</extracomment>
        <translation type="vanished">zeige den vollständigen Dateiname</translation>
    </message>
    <message>
        <source>print the full module name</source>
        <translation type="vanished">zeige den vollständigen Modulnamen</translation>
    </message>
    <message>
        <source>print the module ID</source>
        <translation type="vanished">zeige die Modulsignatur</translation>
    </message>
    <message>
        <source>print the modules device class</source>
        <translation type="vanished">zeige die Geräteklasse des Modules</translation>
    </message>
    <message>
        <source>profile file name</source>
        <translation type="vanished">Profildateiname</translation>
    </message>
    <message>
        <source>profile name</source>
        <translation type="vanished">Profilname</translation>
    </message>
    <message>
        <source>profile not found in color path:</source>
        <translation type="vanished">Profil nicht im Profilepfad gefunden:</translation>
    </message>
    <message>
        <source>profile not found:</source>
        <translation type="vanished">Farbprofil nicht gefunden:</translation>
    </message>
    <message>
        <source>program is not properly installed or missed</source>
        <translation type="vanished">Das Programm ist nicht korrekt installiert oder nicht vorhanden.</translation>
    </message>
    <message>
        <source>read</source>
        <translation type="vanished">lese</translation>
    </message>
    <message>
        <source>read from file</source>
        <translation type="vanished">Lese von Datei</translation>
    </message>
    <message>
        <source>read input stream</source>
        <translation type="vanished">lese Daten von Eingangsstrom</translation>
    </message>
    <message>
        <source>running new ticket failed</source>
        <translation type="vanished">neue Ausführung schlug fehl</translation>
    </message>
    <message>
        <source>select ICC v2 profiles</source>
        <translation type="vanished">wähle ICC V2 Profile</translation>
    </message>
    <message>
        <source>select ICC v4 profiles</source>
        <translation type="vanished">wähle ICC V4 Profile</translation>
    </message>
    <message>
        <source>select a ICC v2 profile</source>
        <translation type="vanished">wähle ein ICC V2 Profil</translation>
    </message>
    <message>
        <source>select a ICC v4 profile</source>
        <translation type="vanished">wähle ein ICC V4 Profil</translation>
    </message>
    <message>
        <source>select floating point 16-bit precision data format</source>
        <translation type="vanished">wähle Datenformat mit 16-bit Fließkommazahlen</translation>
    </message>
    <message>
        <source>select floating point 32-bit precision data format</source>
        <translation type="vanished">wähle Datenformat mit 32-bit Fließkommazahlen</translation>
    </message>
    <message>
        <source>select floating point 64-bit precision data format</source>
        <translation type="vanished">wähle Datenformat mit 64-bit Fließkommazahlen</translation>
    </message>
    <message>
        <source>select format, currently only clut</source>
        <translation type="vanished">wähle Format, z.Z. nur clut möglich</translation>
    </message>
    <message>
        <source>select format, currently only icc</source>
        <translation type="vanished">wähle Format, z.Z. nur icc möglich</translation>
    </message>
    <message>
        <source>select tag</source>
        <translation type="vanished">wähle Eintrag</translation>
    </message>
    <message>
        <source>select unsigned integer 16-bit precision data format</source>
        <translation type="vanished">wähle Datenformat mit 16-bit Ganzzahlen</translation>
    </message>
    <message>
        <source>select unsigned integer 8-bit precision data format</source>
        <translation type="vanished">wähle Datenformat mit 8-bit Ganzzahlen</translation>
    </message>
    <message>
        <source>show as well non matching profiles</source>
        <translation type="vanished">zeige auch nicht passende Profile</translation>
    </message>
    <message>
        <source>show hints and question GUI</source>
        <translation type="vanished">zeigt Hinweise und Fragen Benutzeroberfläche</translation>
    </message>
    <message>
        <source>show profiles with duplicate profile ID</source>
        <translation type="vanished">zeige Profile mit gleicher ICC Profil ID</translation>
    </message>
    <message>
        <source>skip X Color Management device profile</source>
        <translation type="vanished">ignoriere X Color Management Geräteprofil</translation>
    </message>
    <message>
        <source>skip repair of profile ID</source>
        <translation type="vanished">lasse Reparatur der Profil ID weg</translation>
    </message>
    <message>
        <source>source image</source>
        <translation type="vanished">Quellbild</translation>
    </message>
    <message>
        <source>specify incemental increase of the thickness of the graph lines</source>
        <translation type="vanished">gib die Dickenzunahme zwischen verschiedenen Linien an</translation>
    </message>
    <message>
        <source>specify increase of the thickness of the graph lines</source>
        <translation type="vanished">gib die Verstärkung der Liniendicke an</translation>
    </message>
    <message>
        <source>specify output file format png or svg, default is png</source>
        <translation type="vanished">gib Ausgabeformat PNG oder SVG an, voreingestellt ist PNG</translation>
    </message>
    <message>
        <source>specify output file name, default is output.png</source>
        <translation type="vanished">gib Ausgabedateiname an, voreingestellt ist output.png</translation>
    </message>
    <message>
        <source>specify output image width in pixel</source>
        <translation type="vanished">gib Ausgabebildbreite in Pixel an</translation>
    </message>
    <message>
        <source>system path</source>
        <translation type="vanished">Systempfad</translation>
    </message>
    <message>
        <source>unexpected EDID lenght</source>
        <translation type="vanished">unerwartetet EDID Größe</translation>
    </message>
    <message>
        <source>unexpected user_data type</source>
        <translation type="vanished">Unerwarteter user_data Typ</translation>
    </message>
    <message>
        <source>unfinished</source>
        <translation type="vanished">unvollendet</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">unbekannt</translation>
    </message>
    <message>
        <source>use CIE*xyY *x*y plane for saturation line projection</source>
        <translation type="vanished">benutze CIE*xyY *x*y Ebene für Projektion</translation>
    </message>
    <message>
        <source>use IccXML format</source>
        <translation type="vanished">benutze IccXML Format</translation>
    </message>
    <message>
        <source>use device JSON alternatively to -c and -d options</source>
        <translation type="vanished">benutze Geräte JSON alternativ zu den -c und -d Optionen</translation>
    </message>
    <message>
        <source>use device class</source>
        <translation type="vanished">benutze Geräteklasse</translation>
    </message>
    <message>
        <source>use device sub path</source>
        <translation type="vanished">benutze Geräteunterpfad</translation>
    </message>
    <message>
        <source>use new name</source>
        <translation type="vanished">nutze neuen Namen</translation>
    </message>
    <message>
        <source>use only DB keys for -f=openicc</source>
        <translation type="vanished">benutze nur DB Schlüssel/Wertepaare für -f=openicc</translation>
    </message>
    <message>
        <source>user path</source>
        <translation type="vanished">Benutzerpfad</translation>
    </message>
    <message>
        <source>verbose</source>
        <translation type="vanished">plaudernd</translation>
    </message>
    <message>
        <source>w - fit to window width</source>
        <translation type="vanished">w - Passe Bildbreite in Fenster ein</translation>
    </message>
    <message>
        <source>will overwrite policy file</source>
        <translation type="vanished">werde Richtliniendatei überschreiben</translation>
    </message>
    <message>
        <source>with &apos;.&apos; is not allowed</source>
        <translation type="vanished">mit &apos;.&apos; ist nicht erlaubt</translation>
    </message>
    <message>
        <source>write assigned ICC profile</source>
        <translation type="vanished">Schreibe zugewiesenes ICC Profil</translation>
    </message>
    <message>
        <source>write profile with correct ID</source>
        <translation type="vanished">schreibe Profil mit korrekter ID</translation>
    </message>
    <message>
        <source>write to file</source>
        <translation type="vanished">schreibe nach Datei</translation>
    </message>
    <message>
        <source>write to file, currently only PPM and PNG formats</source>
        <translation type="vanished">schreibe zu einer Datei, derzeit nur im Format PPM und PNG</translation>
    </message>
    <message>
        <source>write to file, currently only PPM format</source>
        <translation type="vanished">schreibe zu einer Datei, derzeit nur im Format PPM</translation>
    </message>
    <message>
        <source>write to specified file</source>
        <translation type="vanished">schreibe nach Datei</translation>
    </message>
    <message>
        <source>wrong argument to option:</source>
        <translation type="vanished">Falsches Argument für Option:</translation>
    </message>
    <message>
        <source>wrong profile for static web color space selected, need sRGB</source>
        <translation type="vanished">Falsches Profil für statischen WWW Farbraum, benötige sRGB</translation>
    </message>
    <message>
        <source>    ICC Examin is licensed under the GPL version 2.    Especially all icc_* files are licensed under the GPL.&lt;br&gt;    The other source files may contain devergine licenses, which are detectable from file headers. </source>
        <translation type="obsolete">    ICC Examin ist unter der GPL Version 2 lizensiert.&lt;br&gt; Insbesondere alle icc_* Dateien sind lizenziert unter der GPL.&lt;br&gt;     Die anderen Dateien k&amp;ouml;nnen abweichende Lizenzbedingungen in den Dateik&amp;ouml;pfen enthalten.</translation>
    </message>
    <message>
        <source>    head  head    128 File header</source>
        <translation type="obsolete">    Kopf  Kopf    128 Dateikopf</translation>
    </message>
    <message>
        <source>  (dE CIE 2000) averaging: </source>
        <translation type="obsolete">  (dE CIE 2000) durchschnittlich: </translation>
    </message>
    <message>
        <source>  FLTK: Bill Spitzack and others www.fltk.org&lt;br&gt;
</source>
        <translation type="obsolete">  FLTK: Bill Spitzack und andere www.fltk.org&lt;br&gt;
</translation>
    </message>
    <message>
        <source>  freeglut: freeglut.sf.net&lt;br&gt;
   (partialy used) &lt;br&gt;</source>
        <translation type="obsolete">  freeglut: freeglut.sf.net&lt;br&gt;
   (partiell eingebunden)&lt;br&gt;</translation>
    </message>
    <message>
        <source>  maximum: </source>
        <translation type="obsolete">  maximal: </translation>
    </message>
    <message>
        <source>  minimum: </source>
        <translation type="obsolete">  minimal: </translation>
    </message>
    <message>
        <source> typ - no text output</source>
        <translation type="obsolete"> Typ - keine Textausgabe</translation>
    </message>
    <message>
        <source> used too early!</source>
        <translation type="obsolete"> zu zeitig benutzt!</translation>
    </message>
    <message>
        <source> with </source>
        <translation type="obsolete"> mit </translation>
    </message>
    <message>
        <source> with Gamma: 1.0</source>
        <translation type="obsolete"> mit Gamma: 1.0</translation>
    </message>
    <message>
        <source> with one entry for gamma: </source>
        <translation type="obsolete"> mit einem Eintrag für Gamma: </translation>
    </message>
    <message>
        <source>%d. path %s does not exist</source>
        <translation type="obsolete">%d. Pfad %s existiert nicht</translation>
    </message>
    <message>
        <source>%s Profile</source>
        <translation type="obsolete">%s Profil</translation>
    </message>
    <message>
        <source>%s:%d %s() nothing allocated %s
</source>
        <translation type="obsolete">%s:%d %s() nichts zu reservieren %s
</translation>
    </message>
    <message>
        <source>&amp;Done</source>
        <translation type="obsolete">&amp;Fertig</translation>
    </message>
    <message>
        <source>--path print the full file name</source>
        <translation type="obsolete">zeige den vollständigen Modulnamen</translation>
    </message>
    <message>
        <source>--short print only the file name</source>
        <translation type="obsolete">--short zeige nur den Dateinamen</translation>
    </message>
    <message>
        <source>/Default Profiles/</source>
        <translation type="obsolete">/Standard Profile/</translation>
    </message>
    <message>
        <source>3D Lockup table with</source>
        <translation type="obsolete">3D Farbtabelle mit</translation>
    </message>
    <message>
        <source>&lt;/b&gt;  maximum: </source>
        <translation type="obsolete">&lt;/b&gt;  maximal: </translation>
    </message>
    <message>
        <source>&lt;/b&gt; Measurement</source>
        <translation type="obsolete">&lt;/b&gt; Messdaten</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>@-3+</source>
        <translation type="obsolete">@-3+</translation>
    </message>
    <message>
        <source>@-3reload</source>
        <translation type="obsolete">@-3reload</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Über</translation>
    </message>
    <message>
        <source>About ICC Examin</source>
        <translation type="obsolete">Über ICC Examin</translation>
    </message>
    <message>
        <source>About ICC_Examin</source>
        <translation type="obsolete">&amp;Uuml;ber ICC_Examin</translation>
    </message>
    <message>
        <source>Acknowledgement</source>
        <translation type="obsolete">Dank</translation>
    </message>
    <message>
        <source>Add Path containing ICC profiles</source>
        <translation type="obsolete">Pfad mit ICC Profilen hinzufügen</translation>
    </message>
    <message>
        <source>Add this directory to my favorites</source>
        <translation type="obsolete">zu Lesezeichen</translation>
    </message>
    <message>
        <source>Add to Favorites</source>
        <translation type="obsolete">Füge Lesezeichen hinzu</translation>
    </message>
    <message>
        <source>All</source>
        <translation type="obsolete">Alle</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="obsolete">Alle Dateien (*)</translation>
    </message>
    <message>
        <source>An error ocurred while trying to delete &apos;%s&apos;.</source>
        <translation type="obsolete">Konnte Datei &apos;%s&apos; nicht löschen.</translation>
    </message>
    <message>
        <source>ArgyllCMS color management module</source>
        <translation type="obsolete">ArgyllCMS Farbumwandlungmodul</translation>
    </message>
    <message>
        <source>Back side</source>
        <translation type="obsolete">Rückseite</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation type="obsolete">Hintergrundfarbe</translation>
    </message>
    <message>
        <source>Behaviour for Softproofing view at application startup</source>
        <translation type="obsolete">Verhalten für die Gerätenachbildung mit dem Standardsimulationsprofil am Bildschirm beim Programmstart</translation>
    </message>
    <message>
        <source>Blue Gamma:</source>
        <translation type="obsolete">Blaue Tonwertkurve:</translation>
    </message>
    <message>
        <source>Blue xy:</source>
        <translation type="obsolete">Blau xy:</translation>
    </message>
    <message>
        <source>CGATS View</source>
        <translation type="obsolete">CGATS Ansicht</translation>
    </message>
    <message>
        <source>CIE*Lab</source>
        <translation type="obsolete">CIE*Lab</translation>
    </message>
    <message>
        <source>CLUT</source>
        <translation type="obsolete">3D Interpolationstabelle</translation>
    </message>
    <message>
        <source>CMM loader</source>
        <translation type="obsolete">CMM Kern</translation>
    </message>
    <message>
        <source>CMM loader filter</source>
        <translation type="obsolete">CMM Rechner</translation>
    </message>
    <message>
        <source>Call next filter in node</source>
        <translation type="obsolete">Führe nächsten Filter im Knoten aus</translation>
    </message>
    <message>
        <source>Can not allocate memory for:</source>
        <translation type="obsolete">Kann nicht reservieren für:</translation>
    </message>
    <message>
        <source>Can not handle different source and destination pixel layouts</source>
        <translation type="obsolete">Kann ungleiche Quell-und Zielpixelkonfigurationen nicht bedienen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Chain -texts</source>
        <translation type="obsolete">Kette -Texte</translation>
    </message>
    <message>
        <source>Chain selection</source>
        <translation type="obsolete">Kettenauswahl</translation>
    </message>
    <message>
        <source>Check if LittleCMS can handle a certain command.</source>
        <translation type="obsolete">Prüfe ob LittleCMS ein bestimmtes Kommando behandeln kann.</translation>
    </message>
    <message>
        <source>Check if Oyranos Threads can handle a certain command.</source>
        <translation type="obsolete">Prüfe ob dieses Modul ein bestimmtes Kommando behandeln kann.</translation>
    </message>
    <message>
        <source>Choose File</source>
        <translation type="obsolete">Wählen Sie eine Datei</translation>
    </message>
    <message>
        <source>Choose one profile tag</source>
        <translation type="obsolete">Wählen Sie einen Profilbaustein</translation>
    </message>
    <message>
        <source>Class:       </source>
        <translation type="obsolete">Klasse:      </translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
    <message>
        <source>Cmyk to Cmyk transformation</source>
        <translation type="obsolete">Cmyk nach Cmyk Umwandlung</translation>
    </message>
    <message>
        <source>Color Space for Simulating real devices</source>
        <translation type="obsolete">Farbraum für die Nachbildung eines Gerätes (Simulation)</translation>
    </message>
    <message>
        <source>Color Transform</source>
        <translation type="obsolete">Farbumwandlung</translation>
    </message>
    <message>
        <source>Color Transform Create</source>
        <translation type="obsolete">Farbumwandlung Anlegen</translation>
    </message>
    <message>
        <source>Color line</source>
        <translation type="obsolete">Farbkreis</translation>
    </message>
    <message>
        <source>Color profiles are often available from manufacturers of optical devices (digital camera, scanner, printer). With ICC Examin you can get an impression of the internal data , stored in a ICC profile. For profiles containing measurement data, ICC Examin can generate a quality report. The deviation, calculated from the measurement data is shown as well in the three dimensional gamut viewer.&lt;br&gt;</source>
        <translation type="obsolete">Farbprofile sind erh&amp;auml;ltlich ofmals von Herstellern optischer Ger&amp;auml;te (digitale Kameras, Scanner, Drucker). Mit ICC Examin haben Sie die M&amp;ouml;glichkeit einen Eindruck von den internen Daten zu erlangen. ICC Examin kann, falls Messdaten im Profil hinterlegt sind, einen Qualit&amp;auml;tsbericht erstellen. Die aus den Messdaten errechneten Farbabweichungen werden auch im dreidimensionalen Farbraumbetrachter dargestellt. &lt;br&gt;</translation>
    </message>
    <message>
        <source>Compact Disk</source>
        <translation type="obsolete">CD</translation>
    </message>
    <message>
        <source>Compare Measurement &lt;-&gt; Profile Colors</source>
        <translation type="obsolete">Vergleich Mess- und Profilfarben</translation>
    </message>
    <message>
        <source>Configure Paths</source>
        <translation type="obsolete">Konfiguriere Pfade</translation>
    </message>
    <message>
        <source>Conversion table with 16-bit precission:</source>
        <translation type="obsolete">Konvertierungskette mit 16-bit Präzission:</translation>
    </message>
    <message>
        <source>Conversion table with 8-bit precission:</source>
        <translation type="obsolete">Konvertierungskette mit 8-bit Präzission:</translation>
    </message>
    <message>
        <source>Could not create directory &apos;%s&apos;. You may not have permission to perform this operation.</source>
        <translation type="obsolete">Ordner &apos;%s&apos; konnte nicht erstellt werden</translation>
    </message>
    <message>
        <source>Could not find
Oyranos configuration dialog
in the actual
executable path.</source>
        <translation type="obsolete">Konnte
Oyranos Konfigurationsdialog
nicht im aktuellen
Ausfuehrungspfad finden.</translation>
    </message>
    <message>
        <source>Could not get Xatom &quot;%s&quot;</source>
        <translation type="obsolete">Nicht gefunden: Xatom &quot;%s&quot;</translation>
    </message>
    <message>
        <source>Could not get Xatom, probably your monitor profile is not set:</source>
        <translation type="obsolete">Konnte Xatom nicht finden. Wahrscheinlich ist das Monitorprofil nicht gesetzt.</translation>
    </message>
    <message>
        <source>Could not open file</source>
        <translation type="obsolete">Konnte Datei nicht laden</translation>
    </message>
    <message>
        <source>Could not open instrument</source>
        <translation type="obsolete">Konnte Instrument nicht öffnen</translation>
    </message>
    <message>
        <source>Created new_ticket</source>
        <translation type="obsolete">Erzeugte new_ticket</translation>
    </message>
    <message>
        <source>Curve Channels:</source>
        <translation type="obsolete">Kurvenkanäle:</translation>
    </message>
    <message>
        <source>Curve Precission:</source>
        <translation type="obsolete">Kurvenpräzission:</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation type="obsolete">Kurven</translation>
    </message>
    <message>
        <source>Data:</source>
        <translation type="obsolete">Daten:</translation>
    </message>
    <message>
        <source>Date:</source>
        <translation type="obsolete">Datum:</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="obsolete">Desktop</translation>
    </message>
    <message>
        <source>Detail mode</source>
        <translation type="obsolete">Detailierte Informationen</translation>
    </message>
    <message>
        <source>Device Colors</source>
        <translation type="obsolete">Gerätefarben</translation>
    </message>
    <message>
        <source>Device Serial:</source>
        <translation type="obsolete">Serien Nr.:</translation>
    </message>
    <message>
        <source>Did not find a type for option:</source>
        <translation type="obsolete">Fand keinen Typ für Option:</translation>
    </message>
    <message>
        <source>Documentation</source>
        <translation type="obsolete">Dokumente</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation type="obsolete">Dokumente</translation>
    </message>
    <message>
        <source>Driver name:</source>
        <translation type="obsolete">Gerätetreibername:</translation>
    </message>
    <message>
        <source>Driver signature/encoding:</source>
        <translation type="obsolete">Gerätetreiberdatentyp:</translation>
    </message>
    <message>
        <source>Driver version:</source>
        <translation type="obsolete">Gerätetreiberversion:</translation>
    </message>
    <message>
        <source>Erase</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Error in ICC profile tag found!</source>
        <translation type="obsolete">Fehler in ICC Profilbaustein gefunden!</translation>
    </message>
    <message>
        <source>Error setting up atom &quot;%s&quot;</source>
        <translation type="obsolete">Fehler beim Setzen des Xatoms &quot;%s&quot;</translation>
    </message>
    <message>
        <source>Exclude this path</source>
        <translation type="obsolete">Diesen Pfad ausschließen</translation>
    </message>
    <message>
        <source>FILENAME</source>
        <translation type="obsolete">DATEINAME</translation>
    </message>
    <message>
        <source>FLU is not installed
and is needed for the
Oyranos configuration dialog.</source>
        <translation type="obsolete">FLU ist nicht installiert
und wird benoetigt von
Oyranos Konfigurationsdialog</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="obsolete">Lesezeichen</translation>
    </message>
    <message>
        <source>File &apos;%s&apos; already exists!</source>
        <translation type="obsolete">Datei &apos;%s&apos; existiert bereits.</translation>
    </message>
    <message>
        <source>File Types</source>
        <translation type="obsolete">Datei Typen</translation>
    </message>
    <message>
        <source>File header invalid</source>
        <translation type="obsolete">Dateikopf ungültig</translation>
    </message>
    <message>
        <source>Filename (ICC data type)</source>
        <translation type="obsolete">Dateiname (ICC Daten)</translation>
    </message>
    <message>
        <source>Filename (VRML)</source>
        <translation type="obsolete">Dateiname</translation>
    </message>
    <message>
        <source>Filename (corrupted ICC data type)</source>
        <translation type="obsolete">Dateiname (beschädigte ICC Daten)</translation>
    </message>
    <message>
        <source>Filename (measurement)</source>
        <translation type="obsolete">Messdatei öffnen</translation>
    </message>
    <message>
        <source>Filename (other data type)</source>
        <translation type="obsolete">Dateiname (anderer Datentyp)</translation>
    </message>
    <message>
        <source>Filename:</source>
        <translation type="obsolete">Dateiname:</translation>
    </message>
    <message>
        <source>Flare</source>
        <translation type="obsolete">Überstrahlung</translation>
    </message>
    <message>
        <source>Floppy Disk</source>
        <translation type="obsolete">Diskette</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="obsolete">Ordner</translation>
    </message>
    <message>
        <source>For the visualisation of gamut (external):&lt;br&gt;
</source>
        <translation type="obsolete">F&amp;uuml;r die Visualisation des Farbumfangs (Erzeugung extern):&lt;br&gt;
</translation>
    </message>
    <message>
        <source>Function for Creating a Color Space Transformation.</source>
        <translation type="obsolete">Funktion zur Erzeugung einer Farbraumtransformation.</translation>
    </message>
    <message>
        <source>Gamma</source>
        <translation type="obsolete">Gamma</translation>
    </message>
    <message>
        <source>Gamma Blue:  </source>
        <translation type="obsolete">Gamma Blau:  </translation>
    </message>
    <message>
        <source>Gamma Green:</source>
        <translation type="obsolete">Gamma Grün: </translation>
    </message>
    <message>
        <source>Gamma Green: </source>
        <translation type="obsolete">Gamma Grün:  </translation>
    </message>
    <message>
        <source>Gamma Red:   </source>
        <translation type="obsolete">Gamma Rot:   </translation>
    </message>
    <message>
        <source>Gamut</source>
        <translation type="obsolete">Farbhülle</translation>
    </message>
    <message>
        <source>Gamut View</source>
        <translation type="obsolete">Farbhüllansicht</translation>
    </message>
    <message>
        <source>Gamut selector</source>
        <translation type="obsolete">Darstellungsoptionen</translation>
    </message>
    <message>
        <source>Get XFORMS:</source>
        <translation type="obsolete">Hole XFORMS</translation>
    </message>
    <message>
        <source>Go back one directory in the history</source>
        <translation type="obsolete">vorheriges Verzeichnis</translation>
    </message>
    <message>
        <source>Go forward one directory in the history</source>
        <translation type="obsolete">nächstes Verzeichnis</translation>
    </message>
    <message>
        <source>Go to the parent directory</source>
        <translation type="obsolete">nächsthöheres Verzeichnis</translation>
    </message>
    <message>
        <source>Grafic Card Gamma Table</source>
        <translation type="obsolete">Grafik Karten Gamma Tabelle (vcgt)</translation>
    </message>
    <message>
        <source>Graphic Designers</source>
        <translation type="obsolete">Grafische Gestalter</translation>
    </message>
    <message>
        <source>Green Gamma:</source>
        <translation type="obsolete">Grünes Tonwertkurve:</translation>
    </message>
    <message>
        <source>Green xy:</source>
        <translation type="obsolete">Grün xy:</translation>
    </message>
    <message>
        <source>HTML Documents (*.htm*)</source>
        <translation type="obsolete">HTML Dokumente (*.htm*)</translation>
    </message>
    <message>
        <source>High Precission</source>
        <translation type="obsolete">Hohe Präzission</translation>
    </message>
    <message>
        <source>Hints</source>
        <translation type="obsolete">Hinweise</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="obsolete">Heimverzeichnis</translation>
    </message>
    <message>
        <source>Huch, unknown exception</source>
        <translation type="obsolete">Huch, unbekannte Ausnahme</translation>
    </message>
    <message>
        <source>ICC Examin is a viewer for color profiles according to ICC standard  - www.color.org . At the moment version 2 profiles are supported.</source>
        <translation type="obsolete">ICC Examin ist ein Betrachter von Farbprofilen gem&amp;auml;&amp;szlig; ICC Standard  - www.color.org . Im Moment werden Version 2 Profile unterst&amp;uuml;tzt.</translation>
    </message>
    <message>
        <source>ICC Examin uses following libraries:&lt;br&gt;
</source>
        <translation type="obsolete">ICC Examin benutzt folgende Bibliotheken:&lt;br&gt;
</translation>
    </message>
    <message>
        <source>ICC color profiles (*.{I,i}{C,c}{M,m,C,c})</source>
        <translation type="obsolete">ICC Farbprofile (*.{I,i}{C,c}{M,m,C,c})</translation>
    </message>
    <message>
        <source>ICC color profiles (*.{I,i}{C,c}{M,m,C,c})	Measurement (*.{txt,it8,IT8,RGB,CMYK,ti*,cgats,CIE,cie,nCIE,oRPT,DLY,LAB,Q60})	Argyll Gamuts (*.{wrl,vrml}</source>
        <translation type="obsolete">ICC Farbprofile (*.{I,i}{C,c}{M,m,C,c})	Messdaten (*.{txt,it8,IT8,RGB,CMYK,ti*,cgats,CIE,cie,nCIE,oRPT,DLY,LAB,Q60})	Argyll Farbraumhüllen (*.{wrl,vrml}</translation>
    </message>
    <message>
        <source>ICC header Flags</source>
        <translation type="obsolete">ICC </translation>
    </message>
    <message>
        <source>Illuminant ---</source>
        <translation type="obsolete">Farbtemperatur ---</translation>
    </message>
    <message>
        <source>Illuminant Type</source>
        <translation type="obsolete">Beleuchtungstyp</translation>
    </message>
    <message>
        <source>Illustration</source>
        <translation type="obsolete">Darstellung</translation>
    </message>
    <message>
        <source>Image[input_libraw-lite]</source>
        <translation type="obsolete">Bild[input_libraw-lite]</translation>
    </message>
    <message>
        <source>Inspect</source>
        <translation type="obsolete">Inspektor</translation>
    </message>
    <message>
        <source>Install the missing profile in the OpenIccDirectory path.</source>
        <translation type="obsolete">Installieren Sie das fehlende Profil in den OpenIccDirectory Pfad.</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="obsolete">Installiert</translation>
    </message>
    <message>
        <source>Intent:      </source>
        <translation type="obsolete">Übertragung: </translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Lizenz</translation>
    </message>
    <message>
        <source>Light gray</source>
        <translation type="obsolete">Hellgrau</translation>
    </message>
    <message>
        <source>Linear output curve</source>
        <translation type="obsolete">lineare Ausgangskurve</translation>
    </message>
    <message>
        <source>Linearisation</source>
        <translation type="obsolete">Linearisierung</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="obsolete">Verknüpfung</translation>
    </message>
    <message>
        <source>Load the current monitor profile</source>
        <translation type="obsolete">Das aktuelle Monitor Profile laden</translation>
    </message>
    <message>
        <source>Loading ..</source>
        <translation type="obsolete">Lade ...</translation>
    </message>
    <message>
        <source>Local Disk</source>
        <translation type="obsolete">Localer Speicher</translation>
    </message>
    <message>
        <source>Manage Favorites</source>
        <translation type="obsolete">Bearbeite Lesezeichen</translation>
    </message>
    <message>
        <source>Manufacturer:</source>
        <translation type="obsolete">Hersteller:</translation>
    </message>
    <message>
        <source>Mark out of gamut colors for the proofing profile.</source>
        <translation type="obsolete">Kennzeichne Farben ausserhalb des nachgebildeten Gerätes</translation>
    </message>
    <message>
        <source>May 2004 - January 2008</source>
        <translation type="obsolete">Mai 2004 - Januar 2008</translation>
    </message>
    <message>
        <source>Meas color</source>
        <translation type="obsolete">Messfarbe</translation>
    </message>
    <message>
        <source>Measurement patch</source>
        <translation type="obsolete">Messfeld</translation>
    </message>
    <message>
        <source>Measurment- and profile colors from &lt;b&gt;</source>
        <translation type="obsolete">Mess- und Profilfarben aus &lt;b&gt;</translation>
    </message>
    <message>
        <source>Model:</source>
        <translation type="obsolete">Modell:</translation>
    </message>
    <message>
        <source>Model:      </source>
        <translation type="obsolete">Modell:     </translation>
    </message>
    <message>
        <source>Modell</source>
        <translation type="obsolete">Modell:</translation>
    </message>
    <message>
        <source>Modell:</source>
        <translation type="obsolete">Modell:</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation type="obsolete">Mein Rechner</translation>
    </message>
    <message>
        <source>My cordially thank is especially dedicated to Marti Maria and the many writers of the Lcms-user email list.</source>
        <translation type="obsolete">Mein herzlicher Dank gilt insbesondere Marti Maria und den vielen Schreibenden der Lcms-user Emailliste.</translation>
    </message>
    <message>
        <source>Named Coulor</source>
        <translation type="obsolete">Einzelfarben</translation>
    </message>
    <message>
        <source>Native Gamut</source>
        <translation type="obsolete">Native Farbhülle</translation>
    </message>
    <message>
        <source>Network Disk</source>
        <translation type="obsolete">Netzwerk Speicher</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Neu</translation>
    </message>
    <message>
        <source>New Directory?</source>
        <translation type="obsolete">Verzeichnis erstellen</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="obsolete">Neuer Ordner</translation>
    </message>
    <message>
        <source>No Color</source>
        <translation type="obsolete">keine Farbe</translation>
    </message>
    <message>
        <source>No active drawable found.</source>
        <translation type="obsolete">Keine aktive Zeichenebene gefunden.</translation>
    </message>
    <message>
        <source>No plug or remote filter</source>
        <translation type="obsolete">Hilfe für den Mein Beispielfilter.</translation>
    </message>
    <message>
        <source>No profil assigned to image.</source>
        <translation type="obsolete">Kein Profil am Bild.</translation>
    </message>
    <message>
        <source>No. Tag   Type   Size Description</source>
        <translation type="obsolete">Nr. Teil  Typ   Größe Beschreibung</translation>
    </message>
    <message>
        <source>Number of colors:</source>
        <translation type="obsolete">Anzahl Farben:</translation>
    </message>
    <message>
        <source>Number of input channels</source>
        <translation type="obsolete">Eingangskanäle</translation>
    </message>
    <message>
        <source>Number of output channels</source>
        <translation type="obsolete">Ausgangskanäle</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>Office + Home</source>
        <translation type="obsolete">Büro + Heim</translation>
    </message>
    <message>
        <source>Onion skins</source>
        <translation type="obsolete">Zwiebelhäutchen</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Öffnen</translation>
    </message>
    <message>
        <source>Open Measurement</source>
        <translation type="obsolete">Messdatei öffnen</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Optionen</translation>
    </message>
    <message>
        <source>Oyranos Color Management</source>
        <translation type="obsolete">Oyranos Farbmanagement</translation>
    </message>
    <message>
        <source>Oyranos Configuration</source>
        <translation type="obsolete">Oyranos Konfiguration</translation>
    </message>
    <message>
        <source>Oyranos Configuration </source>
        <translation type="obsolete">Oyranos Konfiguration </translation>
    </message>
    <message>
        <source>Oyranos Control Panel</source>
        <translation type="obsolete">Oyranos Konfiguration</translation>
    </message>
    <message>
        <source>Oyranos X11</source>
        <translation type="obsolete">Oyranos X11</translation>
    </message>
    <message>
        <source>Oyranos and FLU are not installed.
They are needed by
Oyranos configuration dialog.</source>
        <translation type="obsolete">Oyranos und FLU sind nicht installiert
und werden benoetigt von
Oyranos mit seinem Konfigurationsdialog</translation>
    </message>
    <message>
        <source>Oyranos is not installed.
The Oyranos configuration dialog
is not available.</source>
        <translation type="obsolete">Oyranos ist nicht installiert.
Der Oyranos Konfigurationsdialog
ist nicht verfügbar.</translation>
    </message>
    <message>
        <source>Oyranos project; www: http://www.oyranos.com; support/email: ku.b@gmx.de; sources: http://www.oyranos.com/downloads</source>
        <translation type="obsolete">Oyranos Projekt: www: http://www.oyranos.org; Support/email: ku.b@gmx.de; Quellen: http://www.oyranos.org/downloads/</translation>
    </message>
    <message>
        <source>PCS Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>PCS:         </source>
        <translation type="obsolete">PCS:         </translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="obsolete">Pfad</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Pause</translation>
    </message>
    <message>
        <source>Photographers</source>
        <translation type="obsolete">Fotografen</translation>
    </message>
    <message>
        <source>Please add first a image node to the graph before adding other filters</source>
        <translation type="obsolete">Bitte fügen Sie erst ein Bildknoten in den Graphen vor andere Filtern</translation>
    </message>
    <message>
        <source>Please choose an existing file!</source>
        <translation type="obsolete">Bitte wählen Sie eine existierende Datei!</translation>
    </message>
    <message>
        <source>PrePress</source>
        <translation type="obsolete">Druckvorstufe</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="obsolete">Vorschau</translation>
    </message>
    <message>
        <source>Preview files</source>
        <translation type="obsolete">Vorschau</translation>
    </message>
    <message>
        <source>Preview, photografic</source>
        <translation type="obsolete">Voransicht, fotografisch</translation>
    </message>
    <message>
        <source>Priority:</source>
        <translation type="obsolete">Priorität:</translation>
    </message>
    <message>
        <source>Profile at the moment&quot;.</source>
        <translation type="obsolete">Profil im Moment&quot;.</translation>
    </message>
    <message>
        <source>Profile installation can be done using oyranos-profile-install or oyranos-profiles.</source>
        <translation type="obsolete">Profile können mit oyranos-profile-install oder oyranos-profiles installiert werden.</translation>
    </message>
    <message>
        <source>Profile is embedded and </source>
        <translation type="obsolete">Farbprofil ist eingebettet und </translation>
    </message>
    <message>
        <source>Profile&quot; is designed for an other device</source>
        <translation type="obsolete">Profil&quot; ist für eine anderes Gerät bestimmt</translation>
    </message>
    <message>
        <source>Profile&quot;.</source>
        <translation type="obsolete">ICC Profil&quot;.</translation>
    </message>
    <message>
        <source>Profile:         </source>
        <translation type="obsolete">Profil:          </translation>
    </message>
    <message>
        <source>Proof Profile...</source>
        <translation type="obsolete">Simulationsprofil...</translation>
    </message>
    <message>
        <source>Proofing Color Space</source>
        <translation type="obsolete">Simulationsfarbraum</translation>
    </message>
    <message>
        <source>Query device data base profile:</source>
        <translation type="obsolete">Befrage Gerätedatenbank nach Profil:</translation>
    </message>
    <message>
        <source>Query server profile:</source>
        <translation type="obsolete">Frage nach Serverprofil:</translation>
    </message>
    <message>
        <source>RAM Disk</source>
        <translation type="obsolete">RAM Speicher</translation>
    </message>
    <message>
        <source>Red Gamma:</source>
        <translation type="obsolete">Rote Tonwertkurve:</translation>
    </message>
    <message>
        <source>Red xy:</source>
        <translation type="obsolete">Rot xy:</translation>
    </message>
    <message>
        <source>Reflective, </source>
        <translation type="obsolete">Aufsicht, </translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Erneuern</translation>
    </message>
    <message>
        <source>Refresh this directory</source>
        <translation type="obsolete">Auffrischen</translation>
    </message>
    <message>
        <source>Removable Disk</source>
        <translation type="obsolete">Mobiler Speicher</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="obsolete">Umbenennen</translation>
    </message>
    <message>
        <source>Rendering Intent for Color Space Transformations.</source>
        <translation type="obsolete">Übertragungsart für Farbtransformationen.</translation>
    </message>
    <message>
        <source>Rendering with High Precission in opposite to fast processing</source>
        <translation type="obsolete">Berechne mit hoher Präzission, dafür langsamer</translation>
    </message>
    <message>
        <source>Report View</source>
        <translation type="obsolete">Qualitäts Bericht</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">Standard</translation>
    </message>
    <message>
        <source>Reset to a standard Policy for Oyranos.</source>
        <translation type="obsolete">Zurücksetzen auf Oyranos&apos; Standardrichtlinie</translation>
    </message>
    <message>
        <source>Rotate around slice</source>
        <translation type="obsolete">Drehen um Schnitt</translation>
    </message>
    <message>
        <source>Saturation Graph:</source>
        <translation type="obsolete">Sättigungs Zeichnung:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Sichern</translation>
    </message>
    <message>
        <source>Save Gamut as Profile</source>
        <translation type="obsolete">Speichere Farbhülle als Profil</translation>
    </message>
    <message>
        <source>Save Gamut as VRML</source>
        <translation type="obsolete">Speichere Farbhülle als VRML</translation>
    </message>
    <message>
        <source>Save Report</source>
        <translation type="obsolete">Bericht Speichern</translation>
    </message>
    <message>
        <source>Segmented Curve</source>
        <translation type="obsolete">Stückweise Kurve</translation>
    </message>
    <message>
        <source>Select Behaviour</source>
        <translation type="obsolete">Wählen Sie das Verhalten</translation>
    </message>
    <message>
        <source>Select Profile</source>
        <translation type="obsolete">Wähle Profil</translation>
    </message>
    <message>
        <source>Select a directory with ICC profiles</source>
        <translation type="obsolete">Wählen Sie ein Verzeichnis mit ICC Profilen</translation>
    </message>
    <message>
        <source>Separation</source>
        <translation type="obsolete">Separation</translation>
    </message>
    <message>
        <source>Serial:</source>
        <translation type="obsolete">Serien Nr.:</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="obsolete">Setze</translation>
    </message>
    <message>
        <source>Set the current profile as monitor profile and upload vcgt tag to the video card</source>
        <translation type="obsolete">Das aktuelle Profile als Monitorprofil setzen und die Gamma Tabelle in die Grafikkarte laden</translation>
    </message>
    <message>
        <source>Show Main Window</source>
        <translation type="obsolete">Zeige Hauptfenster</translation>
    </message>
    <message>
        <source>Show Profile failed. iccexamin not found</source>
        <translation type="obsolete">Anzeige des Profiles misslungen. iccexamin nicht gefunden.</translation>
    </message>
    <message>
        <source>Show all channels of this table a own window.</source>
        <translation type="obsolete">Zeige alle Kanäle dieser Tabelle in einem eigenen Fenster an.</translation>
    </message>
    <message>
        <source>Show in external Viewer</source>
        <translation type="obsolete">Anzeige in exteren Betrachter</translation>
    </message>
    <message>
        <source>Show options [include policy]</source>
        <translation type="obsolete">Zeige Optionen [mit Richtlinie]</translation>
    </message>
    <message>
        <source>Show:</source>
        <translation type="obsolete">Zeige:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation type="obsolete">Größe:</translation>
    </message>
    <message>
        <source>Slice</source>
        <translation type="obsolete">Schnitt</translation>
    </message>
    <message>
        <source>Slice plane</source>
        <translation type="obsolete">Querschnitt</translation>
    </message>
    <message>
        <source>Sphere 1dE</source>
        <translation type="obsolete">Kugel 1dE</translation>
    </message>
    <message>
        <source>Sphere 2dE</source>
        <translation type="obsolete">Kugel 2dE</translation>
    </message>
    <message>
        <source>Sphere 4dE</source>
        <translation type="obsolete">Kugel 4dE</translation>
    </message>
    <message>
        <source>Std-exception occured: </source>
        <translation type="obsolete">Std-Ausnahme: </translation>
    </message>
    <message>
        <source>Stop loading </source>
        <translation type="obsolete">Laden angehalten: </translation>
    </message>
    <message>
        <source>Tag Version:</source>
        <translation type="obsolete">Element Version:</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="obsolete">Baustein</translation>
    </message>
    <message>
        <source>Texts</source>
        <translation type="obsolete">Texte</translation>
    </message>
    <message>
        <source>Texts/Arrows on/off</source>
        <translation type="obsolete">Texte/Pfeile an/aus</translation>
    </message>
    <message>
        <source>The appliance of color profiles shall help to achive an easy color data exchange, regardless which application or what kind of operating system is used. As well color adaption between different devices is possible, provided the color characteristics of both devices are known. The qulity of a color transform from one to an other device depends mainly on the quality of the color measurement and the used profiling algorithm during profile creation.</source>
        <translation type="obsolete"> Die Anwendung von Farbprofilen soll einen reibungslosen Datenaustausch zwischen unterschiedlichen Farbr&amp;auml;umen und den Abgleich auf unterschiedlichen Ger&amp;auml;ten mit Ihren jeweiligen physikalischen Gegebenheiten erm&amp;ouml;glichen. Das Gelingen h&amp;auml;ngt insbesondere von der Qualit&amp;auml;t der Farbmessungen und dem verwendeten Algorithmus wärend der Erstellung der Farbprofile ab.</translation>
    </message>
    <message>
        <source>The filter is a libraw image reader.</source>
        <translation type="obsolete">Der Filter ließt Camera Rohbilder mittels libraw.</translation>
    </message>
    <message>
        <source>The filter will expect a &quot;scale&quot; option and will create, fill and process a according data version with a new job ticket.</source>
        <translation type="obsolete">Der Filter erwartet eine &quot;scale&quot; Option und erstellt, füllt und verarbeitet die bezüglichen Daten in einem neuen Arbeitsauftrag.</translation>
    </message>
    <message>
        <source>The internal color transformations are currently realised with functions of the littleCMS program library.</source>
        <translation type="obsolete"> Die Farbumwandlungen werden zur Zeit durch Funktionen der littleCMS Programmbibliothek realisiert.</translation>
    </message>
    <message>
        <source>The oyX1 module supports the generic device protocol.</source>
        <translation type="obsolete">Das oyX1 Modul unterstützt Oyranos’ allgemeines Geräteprotokoll.</translation>
    </message>
    <message>
        <source>The proofing color space represents a real color device for simulation. Possible use cases are to simulate a print machine, a viewing environment in a theater or a expected small monitor gamut.</source>
        <translation type="obsolete">Der Simulationsfarbraum stellt das Farbverhalten eines realen Gerätes dar. Mögliche Anwendungen sind die Simulation einer Druckmaschine, von Betrachtungsbedingungen in einem Theater oder der kleinere Farbumfang eines Bildschirmes.</translation>
    </message>
    <message>
        <source>The qarz module supports the generic device protocol.</source>
        <translation type="obsolete">Das qarz Modul unterstützt Oyranos’ allgemeines Geräteprotokoll.</translation>
    </message>
    <message>
        <source>Transparent, </source>
        <translation type="obsolete">Durchsicht, </translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>Unable to rename &apos;%s&apos; to &apos;%s&apos;</source>
        <translation type="obsolete">Konnte &apos;%s&apos; nicht nach &apos;%s&apos; umbenennen</translation>
    </message>
    <message>
        <source>Undefined</source>
        <translation type="obsolete">undefiniert</translation>
    </message>
    <message>
        <source>Unknown argument</source>
        <translation type="obsolete">Unbekanntes Argument</translation>
    </message>
    <message>
        <source>VRML Files (*.wrl)</source>
        <translation type="obsolete">VRML Dateien (*.wrl)</translation>
    </message>
    <message>
        <source>WARNING: Could not load corrupt or damaged profile.</source>
        <translation type="obsolete">WARNUNG: Konnte koruptes oder beschädigtes Profil nicht laden.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Warning : </source>
        <translation type="obsolete">Warnung : </translation>
    </message>
    <message>
        <source>Watch Colors 3D...</source>
        <translation type="obsolete">3D Farbansicht...</translation>
    </message>
    <message>
        <source>Watch Colors 3D.2...</source>
        <translation type="obsolete">3D Farbansicht2...</translation>
    </message>
    <message>
        <source>What kind of image is this?</source>
        <translation type="obsolete">Was für ein Bild ist dies?</translation>
    </message>
    <message>
        <source>What kind of image is this?
</source>
        <translation type="obsolete">Was für ein Bild ist dies?
</translation>
    </message>
    <message>
        <source>White xy:</source>
        <translation type="obsolete">Weiß xy:</translation>
    </message>
    <message>
        <source>Wide list mode</source>
        <translation type="obsolete">Weite Anzeige</translation>
    </message>
    <message>
        <source>Windows</source>
        <translation type="obsolete">Fenster</translation>
    </message>
    <message>
        <source>Work Space</source>
        <translation type="obsolete">Arbeitsfarbraum</translation>
    </message>
    <message>
        <source>Write Model:</source>
        <translation type="obsolete">Schreibe Modell:</translation>
    </message>
    <message>
        <source>Wrong profil assigned to image.</source>
        <translation type="obsolete">Falsches Profil am Bild.</translation>
    </message>
    <message>
        <source>XineramaScreen:</source>
        <translation type="obsolete">XineramaSchirm:</translation>
    </message>
    <message>
        <source>Your</source>
        <translation type="obsolete">Ihr</translation>
    </message>
    <message>
        <source>attempt to add a non linear filter to a linear graph</source>
        <translation type="obsolete">Versuch ein nicht linearen Filter zu einem linearen Grafen hinzuzufügen</translation>
    </message>
    <message>
        <source>averaging deviations (dE CIE*Lab):&lt;b&gt; </source>
        <translation type="obsolete">Abweichungen (dE CIE*Lab) durchschnittlich:&lt;b&gt; </translation>
    </message>
    <message>
        <source>axis</source>
        <translation type="obsolete">Achse</translation>
    </message>
    <message>
        <source>black/white</source>
        <translation type="obsolete">schwarz/weiss</translation>
    </message>
    <message>
        <source>bytes of  memory for</source>
        <translation type="obsolete">Bytes Speicher für</translation>
    </message>
    <message>
        <source>can be used independently from image.</source>
        <translation type="obsolete">kann unabhängig vom Bild verwendet werden.</translation>
    </message>
    <message>
        <source>can not be used independently from image.</source>
        <translation type="obsolete">kann nicht unabhängig vom Bild verwendet werden.</translation>
    </message>
    <message>
        <source>color</source>
        <translation type="obsolete">farbig</translation>
    </message>
    <message>
        <source>color adjust</source>
        <translation type="obsolete">Farbe einstellen</translation>
    </message>
    <message>
        <source>colored</source>
        <translation type="obsolete">farbig</translation>
    </message>
    <message>
        <source>created by:  </source>
        <translation type="obsolete">erzeugt von: </translation>
    </message>
    <message>
        <source>dump ICC profile</source>
        <translation type="obsolete">schreibe ICC Profil</translation>
    </message>
    <message>
        <source>empty</source>
        <translation type="obsolete">leer</translation>
    </message>
    <message>
        <source>error setting path: %s</source>
        <translation type="obsolete">Fehler beim Setzen des Pfades: &quot;%s&quot;</translation>
    </message>
    <message>
        <source>exception occured: </source>
        <translation type="obsolete">Ausnahme: </translation>
    </message>
    <message>
        <source>f(X)=(a*X+b)^gamma + c for X&gt;=-b/a</source>
        <translation type="obsolete">f(X)=(a*X+b)^gamma + c für X&gt;=-b/a</translation>
    </message>
    <message>
        <source>f(X)=(a*X+b)^gamma + e for X&gt;=d</source>
        <translation type="obsolete">f(X)=(a*X+b)^gamma + e für X&gt;=d</translation>
    </message>
    <message>
        <source>f(X)=(a*X+b)^gamma for X&gt;=-b/a</source>
        <translation type="obsolete">f(X)=(a*X+b)^gamma für X&gt;=-b/a</translation>
    </message>
    <message>
        <source>f(X)=(a*X+b)^gamma for X&gt;=d</source>
        <translation type="obsolete">f(X)=(a*X+b)^gamma für X&gt;=d</translation>
    </message>
    <message>
        <source>f(X)=0 for (X&lt;-b/a)</source>
        <translation type="obsolete">f(X)=0 für (X&lt;-b/a)</translation>
    </message>
    <message>
        <source>f(X)=X^gamma</source>
        <translation type="obsolete">f(X)=X^gamma</translation>
    </message>
    <message>
        <source>f(X)=c for (X&lt;-b/a)</source>
        <translation type="obsolete">f(X)=c für (X&lt;-b/a)</translation>
    </message>
    <message>
        <source>f(X)=c*X for (X&lt;d)</source>
        <translation type="obsolete">f(X)=c*X für (X&lt;d)</translation>
    </message>
    <message>
        <source>f(X)=c*X+f for (X&lt;d)</source>
        <translation type="obsolete">f(X)=c*X+f für (X&lt;d)</translation>
    </message>
    <message>
        <source>file exists, please remove befor installing new profile.</source>
        <translation type="obsolete">Datei existiert, Bitte vor der Installation eines neuen Profiles entfernen.</translation>
    </message>
    <message>
        <source>file name is too long %d
</source>
        <translation type="obsolete">Dateiname ist zu lang %d
</translation>
    </message>
    <message>
        <source>file size 0 for </source>
        <translation type="obsolete">Dateigröße 0 für </translation>
    </message>
    <message>
        <source>fine with: </source>
        <translation type="obsolete">gut mit: </translation>
    </message>
    <message>
        <source>found issues while moving in profile</source>
        <translation type="obsolete">fand Probleme beim schreiben in Profil</translation>
    </message>
    <message>
        <source>found no content for</source>
        <translation type="obsolete">keine Inhalte gefunden für</translation>
    </message>
    <message>
        <source>found observer list of wrong type</source>
        <translation type="obsolete">fand Beobachterliste falschen Typs</translation>
    </message>
    <message>
        <source>gamma</source>
        <translation type="obsolete">gamma</translation>
    </message>
    <message>
        <source>glossy, </source>
        <translation type="obsolete">glänzend, </translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="obsolete">grau</translation>
    </message>
    <message>
        <source>high contrast</source>
        <translation type="obsolete">kontrastreich</translation>
    </message>
    <message>
        <source>integrated sources, lock at the according files for licence conditions:&lt;br&gt;
</source>
        <translation type="obsolete">integrierte Quelltexte, die jeweiligen Lizenzbedingungen sind den entsprechended Dateien zu entnehmen:&lt;br&gt;
</translation>
    </message>
    <message>
        <source>internet: www.behrmann.name &lt;br&gt;</source>
        <translation type="obsolete">Internet: www.behrmann.name &lt;br&gt;</translation>
    </message>
    <message>
        <source>is a Oyranos module options tool</source>
        <translation type="obsolete">ist ein Optionswerkzeug für Oyranosmodule</translation>
    </message>
    <message>
        <source>is a Oyranos o(X)FORMS options tool</source>
        <translation type="obsolete">ist ein o(X)FORMS Optionswerkzeug für Oyranos</translation>
    </message>
    <message>
        <source>is not valid for an</source>
        <translation type="obsolete">ist nocht gültig für ein</translation>
    </message>
    <message>
        <source>key/value pairs found:</source>
        <translation type="obsolete">Schlüssel/Wertepaare gefunden:</translation>
    </message>
    <message>
        <source>left mouse button -&gt; go back</source>
        <translation type="obsolete">linke Maustaste -&gt; zurueck</translation>
    </message>
    <message>
        <source>left-/middle-/right mouse button -&gt; rotate/cut/menu</source>
        <translation type="obsolete">linke-/mittlere-/rechte Maustaste -&gt; Drehen/Schneiden/Menue</translation>
    </message>
    <message>
        <source>lineare entrance curve with 256 steps</source>
        <translation type="obsolete">Lineare Eingangskurve mit 256 Stufungen</translation>
    </message>
    <message>
        <source>lineare exit curve with 256 steps</source>
        <translation type="obsolete">Lineare Ausgangskurve mit 256 Stufungen</translation>
    </message>
    <message>
        <source>list possible choices</source>
        <translation type="obsolete">Liste der verfügbaren Auswahlmöglichkeiten</translation>
    </message>
    <message>
        <source>littleCMS Function for Color Space Transformations</source>
        <translation type="obsolete">littleCMS Funktion zur Farbraumtransformation.</translation>
    </message>
    <message>
        <source>littleCMS color management module</source>
        <translation type="obsolete">littleCMS Farbumwandlungmodul</translation>
    </message>
    <message>
        <source>matte, </source>
        <translation type="obsolete">matt, </translation>
    </message>
    <message>
        <source>mouse</source>
        <translation type="obsolete">Pause</translation>
    </message>
    <message>
        <source>multiple occurencies of default profile</source>
        <translation type="obsolete">mehrmaliges Erscheinen des Standard Profiles</translation>
    </message>
    <message>
        <source>multiple occurencies of default profile
  Did you install multiple times?</source>
        <translation type="obsolete">mehrmaliges Erscheinen des Standard Profiles
  Wurde es mehrfach installiert?</translation>
    </message>
    <message>
        <source>nLUT</source>
        <translation type="obsolete">nLUT</translation>
    </message>
    <message>
        <source>nLut</source>
        <translation type="obsolete">Eingabe</translation>
    </message>
    <message>
        <source>negative, </source>
        <translation type="obsolete">negativ, </translation>
    </message>
    <message>
        <source>no choices</source>
        <translation type="obsolete">keine Auswahl</translation>
    </message>
    <message>
        <source>no measurment data or correct profile conversion available</source>
        <translation type="obsolete">keine Messdaten oder korrekte Farbtransformation verfügbar</translation>
    </message>
    <message>
        <source>no net found in VRML file</source>
        <translation type="obsolete">kein Netz in VRML Datei gefunden</translation>
    </message>
    <message>
        <source>no net found. Is Argyll installed?</source>
        <translation type="obsolete">kein Netz gefunden. Ist Argyll installiert?</translation>
    </message>
    <message>
        <source>not available</source>
        <translation type="obsolete">nicht verfügbar</translation>
    </message>
    <message>
        <source>not inside memory block</source>
        <translation type="obsolete">nicht innerhalb des Speicherblocks</translation>
    </message>
    <message>
        <source>nothing allocated</source>
        <translation type="obsolete">nichts reserviert</translation>
    </message>
    <message>
        <source>nothing to allocate - size:</source>
        <translation type="obsolete">kein Speicher reservierbar - Größe:</translation>
    </message>
    <message>
        <source>nothing to delete</source>
        <translation type="obsolete">nichts zu löschen</translation>
    </message>
    <message>
        <source>o&apos;clock</source>
        <translation type="obsolete">Uhr</translation>
    </message>
    <message>
        <source>output array</source>
        <translation type="obsolete">Ausgabedatenfeld</translation>
    </message>
    <message>
        <source>parametric function type</source>
        <translation type="obsolete">Parametrischer Funktionstyp</translation>
    </message>
    <message>
        <source>path is a directory</source>
        <translation type="obsolete">Pfad ist kein Verzeichnis</translation>
    </message>
    <message>
        <source>path type not supported</source>
        <translation type="obsolete">Option nicht unterstützten Typs:</translation>
    </message>
    <message>
        <source>points length per side</source>
        <translation type="obsolete">Punkten Seitenlänge</translation>
    </message>
    <message>
        <source>positive, </source>
        <translation type="obsolete">positiv, </translation>
    </message>
    <message>
        <source>read XFORMS</source>
        <translation type="obsolete">lese XFORMS</translation>
    </message>
    <message>
        <source>refresh gamma table from video card</source>
        <translation type="obsolete">Gammatabelle aus Grafikkarte auslesen</translation>
    </message>
    <message>
        <source>relative colorimetric RI</source>
        <translation type="obsolete">Relativ Farbmetrisch</translation>
    </message>
    <message>
        <source>reset to standard gamma</source>
        <translation type="obsolete">Auf Standard Gamma zurücksetzen</translation>
    </message>
    <message>
        <source>search paths</source>
        <translation type="obsolete">Zeige Suchpfade:</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="obsolete">Sekunden</translation>
    </message>
    <message>
        <source>show help texts</source>
        <translation type="obsolete">Zeige einen Hilfetext</translation>
    </message>
    <message>
        <source>show policy options</source>
        <translation type="obsolete">zeige Richtlinienoptionen</translation>
    </message>
    <message>
        <source>showing</source>
        <translation type="obsolete">zeigen</translation>
    </message>
    <message>
        <source>steps</source>
        <translation type="obsolete">Stufungen</translation>
    </message>
    <message>
        <source>tag list is corrupted for tag</source>
        <translation type="obsolete">die Elementliste ist beschädigt für Element</translation>
    </message>
    <message>
        <source>the Data Object</source>
        <translation type="obsolete">das Datenobjekt</translation>
    </message>
    <message>
        <source>unknown Color</source>
        <translation type="obsolete">unbekannte Farbe</translation>
    </message>
    <message>
        <source>use sRGB</source>
        <translation type="obsolete">benutze sRGB</translation>
    </message>
    <message>
        <source>with</source>
        <translation type="obsolete">mit</translation>
    </message>
    <message>
        <source>with following choices</source>
        <translation type="obsolete">mit folgenden Alternativen</translation>
    </message>
    <message>
        <source>without markers</source>
        <translation type="obsolete">ohne Farborte</translation>
    </message>
    <message>
        <source>yes</source>
        <translation type="obsolete">Bytes</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="63"/>
        <source>ICC Version 2 and 4 compatible Color Profile Examine Tool</source>
        <translation>ICC Version 2 und 4 kompatibler Farbprofilbetrachter</translation>
    </message>
    <message>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="65"/>
        <source>All Rights reserved.</source>
        <translation>Alle Rechte reserviert.</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="69"/>
        <source>Platform</source>
        <translation>Plattform</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="71"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="73"/>
        <source>Qt Version</source>
        <translation>Qt Version</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="84"/>
        <source>Launched app for </source>
        <translation>Rufe Programm für </translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="94"/>
        <location filename="../qml/About.qml" line="103"/>
        <source>OK</source>
        <translation>Ja</translation>
    </message>
</context>
<context>
    <name>IccDialog</name>
    <message>
        <source>OK</source>
        <translation type="vanished">Ja</translation>
    </message>
</context>
<context>
    <name>IccProfile</name>
    <message>
        <location filename="../src/icc_profile.cpp" line="1566"/>
        <source>Nothing loaded</source>
        <translation>Nichts geladen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="91"/>
        <location filename="../src/icc_profile.cpp" line="1584"/>
        <source>File Name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="92"/>
        <location filename="../src/icc_profile.cpp" line="114"/>
        <location filename="../src/icc_profile.cpp" line="131"/>
        <location filename="../src/icc_profile.cpp" line="1585"/>
        <location filename="../src/icc_profile.cpp" line="1606"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="103"/>
        <location filename="../src/icc_profile.cpp" line="1595"/>
        <source>Could not load file:</source>
        <translation>Konnte Datei nicht laden:</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="111"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="123"/>
        <source>Copyright</source>
        <translation>Kopierrecht</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="125"/>
        <location filename="../src/icc_profile.cpp" line="1601"/>
        <source>Header</source>
        <translation>Kopf</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="132"/>
        <location filename="../src/icc_profile.cpp" line="1607"/>
        <source>CMM</source>
        <translation>CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="145"/>
        <location filename="../src/icc_profile.cpp" line="1629"/>
        <source>Class</source>
        <translation>Klasse</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="141"/>
        <location filename="../src/icc_profile.cpp" line="146"/>
        <location filename="../src/icc_profile.cpp" line="1616"/>
        <location filename="../src/icc_profile.cpp" line="1630"/>
        <source>Color Space</source>
        <translation>Farbraum</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="133"/>
        <location filename="../src/icc_profile.cpp" line="1608"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="137"/>
        <location filename="../src/icc_profile.cpp" line="1612"/>
        <source>Output</source>
        <translation>Ausgabe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="138"/>
        <location filename="../src/icc_profile.cpp" line="1613"/>
        <source>Display</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="139"/>
        <location filename="../src/icc_profile.cpp" line="1614"/>
        <source>Link</source>
        <translation>Verknüpfung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="140"/>
        <location filename="../src/icc_profile.cpp" line="1615"/>
        <source>Abstract</source>
        <translation>Abstrakt</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="142"/>
        <location filename="../src/icc_profile.cpp" line="1617"/>
        <source>Named Color</source>
        <translation>Einzelfarben</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="143"/>
        <location filename="../src/icc_profile.cpp" line="1618"/>
        <source>Input</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="147"/>
        <location filename="../src/icc_profile.cpp" line="1631"/>
        <source>PCS Color Space</source>
        <translation>PCS Farbraum</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="148"/>
        <location filename="../src/icc_profile.cpp" line="1632"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="150"/>
        <location filename="../src/icc_profile.cpp" line="1639"/>
        <source>Signature</source>
        <translation>Signatur</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="151"/>
        <location filename="../src/icc_profile.cpp" line="1640"/>
        <source>Platform</source>
        <translation>Plattform</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="152"/>
        <location filename="../src/icc_profile.cpp" line="1641"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="153"/>
        <location filename="../src/icc_profile.cpp" line="1642"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="154"/>
        <location filename="../src/icc_profile.cpp" line="1643"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="155"/>
        <location filename="../src/icc_profile.cpp" line="1644"/>
        <source>Creator</source>
        <translation>Erzeuger</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="156"/>
        <location filename="../src/icc_profile.cpp" line="1645"/>
        <source>Attributes</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="157"/>
        <location filename="../src/icc_profile.cpp" line="1646"/>
        <source>Rendering Intent</source>
        <translation>Übertragungsart</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="158"/>
        <location filename="../src/icc_profile.cpp" line="1647"/>
        <source>Illuminant</source>
        <translation>Beleuchtung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="161"/>
        <location filename="../src/icc_profile.cpp" line="1648"/>
        <source>Profile ID</source>
        <translation>Profil ID</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="163"/>
        <location filename="../src/icc_profile.cpp" line="1652"/>
        <source>Tags</source>
        <translation>Profilbausteine</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1583"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1621"/>
        <source>Saturation</source>
        <translation>Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1622"/>
        <source>Absolute Colorimetric</source>
        <translation>Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1623"/>
        <source>Relative Colorimetric</source>
        <translation>Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1624"/>
        <source>Reflective | Glossy</source>
        <translation>Reflektierend | Glänzend</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1625"/>
        <source>EmbeddedProfileFalse | UseAnywhere</source>
        <translation>Nicht eingebettetes Profil | Benutze überall</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1626"/>
        <source>undefined</source>
        <translation>nicht definiert</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1894"/>
        <location filename="../src/icc_profile.cpp" line="1896"/>
        <source>%1 Version</source>
        <translation>%1 Version</translation>
    </message>
    <message>
        <source>Curve with %1 segments</source>
        <translation type="vanished">Kurve mit %1 Segmenten</translation>
    </message>
    <message>
        <source>B-Curves</source>
        <translation type="vanished">B-Kurven</translation>
    </message>
    <message>
        <source>M-Curves</source>
        <translation type="vanished">M-Kurven</translation>
    </message>
    <message>
        <source>A-Curves</source>
        <translation type="vanished">A-Kurven</translation>
    </message>
    <message>
        <source>B-Curve</source>
        <translation type="vanished">B-Kurve</translation>
    </message>
    <message>
        <source>Matrix</source>
        <translation type="vanished">Matrix</translation>
    </message>
    <message>
        <source>M-Curve</source>
        <translation type="vanished">M-Kurve</translation>
    </message>
    <message>
        <source>A-Curve</source>
        <translation type="vanished">A-Kurve</translation>
    </message>
    <message>
        <source>B-Curve:
</source>
        <translation type="vanished">B-Kurve:
</translation>
    </message>
    <message>
        <source>Matrix:
</source>
        <translation type="vanished">Matrix:
</translation>
    </message>
    <message>
        <source>M-Curve:
</source>
        <translation type="vanished">M-Kurve:
</translation>
    </message>
    <message>
        <source>A-Curve:
</source>
        <translation type="vanished">A-Kurve:
</translation>
    </message>
    <message>
        <source>Profile description</source>
        <translation type="vanished">Profilbeschreibung</translation>
    </message>
    <message>
        <source>Device manufacturerer description</source>
        <translation type="vanished">Geräte Hersteller</translation>
    </message>
    <message>
        <source>Device model description</source>
        <translation type="vanished">Geräte Model</translation>
    </message>
    <message>
        <source>Media white point</source>
        <translation type="vanished">Medienweißpunkt</translation>
    </message>
    <message>
        <source>Red Colorant</source>
        <translation type="vanished">Rote Grundfarbe</translation>
    </message>
    <message>
        <source>Green Colorant</source>
        <translation type="vanished">Grüne Grundfarbe</translation>
    </message>
    <message>
        <source>Blue Colorant</source>
        <translation type="vanished">Blaue Grundfarbe</translation>
    </message>
    <message>
        <source>Red tone reproduction curve</source>
        <translation type="vanished">Rote Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>Green tone reproduction curve</source>
        <translation type="vanished">Grüne Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>Blue tone reproduction curve</source>
        <translation type="vanished">Blaue Farbwiedergabekurve</translation>
    </message>
    <message>
        <source>Chromaticity</source>
        <translation type="vanished">Primärfarben</translation>
    </message>
    <message>
        <source>EmbeddedProfileFalse</source>
        <translation type="vanished">Nicht eingebettetes Profil</translation>
    </message>
    <message>
        <source>UseAnywhere</source>
        <translation type="vanished">Benutze überall</translation>
    </message>
    <message>
        <source>Reflective</source>
        <translation type="vanished">Reflektierend</translation>
    </message>
    <message>
        <source>Glossy</source>
        <translation type="vanished">Glänzend</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1620"/>
        <source>Perceptual</source>
        <translation>Fotografisch</translation>
    </message>
    <message>
        <source>Head</source>
        <translation type="vanished">Kopf</translation>
    </message>
</context>
<context>
    <name>ProfileTagListView</name>
    <message>
        <location filename="../qml/ProfileTagListView.qml" line="45"/>
        <source>Nothing loaded</source>
        <translation>Nichts geladen</translation>
    </message>
    <message>
        <location filename="../qml/ProfileTagListView.qml" line="135"/>
        <source>Loaded JSON with length of:</source>
        <translation>JSON geladen mit Länge von:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>WARNING</source>
        <translation>WARNUNG</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="111"/>
        <source>!!! ERROR</source>
        <translation>!!! Fehler</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="358"/>
        <source>Can not handle content in running task. Please retry.</source>
        <translation>Kann Inhalte nicht in laufendem Prozess behandeln. Bitte nochmals probieren.</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="212"/>
        <source>XYZ</source>
        <translation>XYZ</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="213"/>
        <source>Lab</source>
        <translation>Lab</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="214"/>
        <source>Luv</source>
        <translation>Luv</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="215"/>
        <source>YCbCr</source>
        <translation>YCbCr</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="216"/>
        <source>Yxy</source>
        <translation>Yxy</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="217"/>
        <source>Rgb</source>
        <translation>Rgb</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="218"/>
        <source>Gray</source>
        <translation>Grau</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="219"/>
        <source>Hsv</source>
        <translation>Hsv</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="220"/>
        <source>Hls</source>
        <translation>Hls</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="221"/>
        <source>Cmyk</source>
        <translation>Cmyk</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="222"/>
        <source>Cmy</source>
        <translation>Cmy</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="223"/>
        <source>2color</source>
        <translation>2farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="224"/>
        <source>3color</source>
        <translation>3farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="225"/>
        <source>4color</source>
        <translation>4farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="226"/>
        <source>5color</source>
        <translation>5farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="227"/>
        <source>6color</source>
        <translation>6farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="228"/>
        <source>7color</source>
        <translation>7farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="229"/>
        <source>8color</source>
        <translation>8farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="230"/>
        <source>9color</source>
        <translation>9farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="231"/>
        <source>10color</source>
        <translation>10farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="232"/>
        <source>11color</source>
        <translation>11farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="233"/>
        <source>12color</source>
        <translation>12farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="234"/>
        <source>13color</source>
        <translation>13farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="235"/>
        <source>14color</source>
        <translation>14farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="236"/>
        <source>15color</source>
        <translation>15farbig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="250"/>
        <source>Lookup table, device to PCS, intent perceptual</source>
        <translation>Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="251"/>
        <source>Lookup table, device to PCS, intent relative colorimetric</source>
        <translation>Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="252"/>
        <source>Lookup table, device to PCS, intent saturation</source>
        <translation>Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="253"/>
        <source>Lookup table, device to PCS, intent absolute</source>
        <translation>Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="254"/>
        <source>Lookup table, device to material</source>
        <translation>Farbtabelle, Gerät an Material</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="255"/>
        <source>Blue Colorant</source>
        <translation>Blaue Grundfarbe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="256"/>
        <source>Blue tone reproduction curve</source>
        <translation>Blaue Farbwiedergabekurve</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="257"/>
        <source>BRDF Lookup table, device to PCS, intent perceptual</source>
        <translation>BRDF Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="258"/>
        <source>BRDF Lookup table, device to PCS, intent relative colorimetric</source>
        <translation>BRDF Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="259"/>
        <source>BRDF Lookup table, device to PCS, intent saturation</source>
        <translation>BRDF Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="260"/>
        <source>BRDF Lookup table, device to PCS, intent absolute</source>
        <translation>BRDF Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="261"/>
        <source>BRDF Lookup table, device to spectral PCS, intent perceptual</source>
        <translation>BRDF Farbtabelle, Gerät an spektralen Kontaktfarbraum, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="262"/>
        <source>BRDF Lookup table, device to spectral PCS, intent relative colorimetric</source>
        <translation>BRDF Farbtabelle, Gerät an spektralen Kontaktfarbraum, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="263"/>
        <source>BRDF Lookup table, device to spectral PCS, intent saturation</source>
        <translation>BRDF Farbtabelle, Gerät an spektralen Kontaktfarbraum, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="264"/>
        <source>BRDF Lookup table, device to spectral PCS, intent absolute</source>
        <translation>BRDF Farbtabelle, Gerät an spektralen Kontaktfarbraum, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="265"/>
        <source>Lookup table, PCS to device, intent perceptual</source>
        <translation>Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="266"/>
        <source>Lookup table, PCS to device, intent relative colorimetric</source>
        <translation>Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="267"/>
        <source>Lookup table, PCS to device, intent saturation</source>
        <translation>Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="268"/>
        <source>Lookup table, PCS to device, intent absolute</source>
        <translation>Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="269"/>
        <source>Calibration date</source>
        <translation>Kalibrationsdatum</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="270"/>
        <source>Color measurement data</source>
        <translation>Farbmessdaten</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="273"/>
        <source>Color encoding parameters</source>
        <translation>Farbkodierungsparameter</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="274"/>
        <source>Color space name</source>
        <translation>Farbraumname</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="276"/>
        <source>Color channel table</source>
        <translation>Farbkanaltabelle</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="277"/>
        <source>Colorant table output</source>
        <translation>Grundfarbentabelle für Ausgabe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="278"/>
        <source>Colorimetric intent image state</source>
        <translation>Farbmetrische Bestimmung des Bildes</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="279"/>
        <source>Copyright</source>
        <translation>Kopierrecht</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="280"/>
        <source>Postscript CRD Information</source>
        <translation>Postscript CRD Information</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="281"/>
        <source>Custom to standard PCS</source>
        <translation>Individueller zu Standard Kontaktfarbraum</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="283"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="284"/>
        <source>Date and time</source>
        <translation>Datum und Zeit</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="285"/>
        <source>Device media white point</source>
        <translation>Weißpunkt des Gerätemediums</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="286"/>
        <source>Device manufacturerer description</source>
        <translation>Geräte Hersteller</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="287"/>
        <source>Device model description</source>
        <translation>Geräte Model</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="288"/>
        <source>Device Settings</source>
        <translation>Geräte Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="289"/>
        <source>Override lookup table, device to PCS, intent perceptual</source>
        <translation>Präzise Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="290"/>
        <source>Override lookup table, device to PCS, intent relative colorimetric</source>
        <translation>Präzise Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="291"/>
        <source>Override lookup table, device to PCS, intent saturation</source>
        <translation>Präzise Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="292"/>
        <source>Override lookup table, device to PCS, intent absolute</source>
        <translation>Präzise Farbtabelle, Gerät an Kontaktfarbraum, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="293"/>
        <source>Override lookup table, PCS to device, intent perceptual</source>
        <translation>Präzise Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="294"/>
        <source>Override lookup table, PCS to device, intent relative colorimetric</source>
        <translation>Präzise Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Relativ Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="295"/>
        <source>Override lookup table, PCS to device, intent saturation</source>
        <translation>Präzise Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Sättigung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="296"/>
        <source>Override lookup table, PCS to device, intent absolute</source>
        <translation>Präzise Farbtabelle, Kontaktfarbraum an Gerät, Anpassung Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="297"/>
        <source>gamut</source>
        <translation>Farbumfang</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="298"/>
        <source>Gray tone reproduction curve</source>
        <translation>Schwarze Wiedergabekurve</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="299"/>
        <source>Green Colorant</source>
        <translation>Grüne Grundfarbe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="300"/>
        <source>Green tone reproduction curve</source>
        <translation>Grüne Farbwiedergabekurve</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="301"/>
        <source>Luminance</source>
        <translation>Leuchtdichte</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="302"/>
        <source>Measurement</source>
        <translation>Messart</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="303"/>
        <source>Media black point</source>
        <translation>Medienschwarzpunkt</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="304"/>
        <source>Media white point</source>
        <translation>Medienweißpunkt</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="305"/>
        <source>Meta Data</source>
        <translation>Allgemeine Daten</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="307"/>
        <source>Named Color 2</source>
        <translation>Einzelfarben 2</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="308"/>
        <source>Preview, perceptual</source>
        <translation>Voransicht, fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="309"/>
        <source>Preview, relative colorimetric</source>
        <translation>Voransicht, relativ farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="310"/>
        <source>Preview, saturated</source>
        <translation>Voransicht, farbgesättigt</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="311"/>
        <source>Profile description</source>
        <translation>Profilbeschreibung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="313"/>
        <source>Profile description, multilingual</source>
        <translation>Profilbeschreibung mehrsprachig</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="314"/>
        <source>Profile sequence description</source>
        <translation>Beschreibung der Profilverknüpfung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="315"/>
        <source>Profile sequence identification</source>
        <translation>Identifizierung der Profilverknüpfung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="316"/>
        <source>Postscript 2 CRD, perceptual</source>
        <translation>Postscript 2 CRD, fotografisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="317"/>
        <source>Postscript 2 CRD, relative colorimetric</source>
        <translation>Postscript 2 CRD, relativ farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="318"/>
        <source>Postscript 2 CRD, saturated</source>
        <translation>Postscript 2 CRD, farbgesättigt</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="319"/>
        <source>Postscript 2 CRD, absolute colorimetric</source>
        <translation>Postscript 2 CRD, Absolut Farbmetrisch</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="320"/>
        <source>Postscript 2 CSA</source>
        <translation>Postscript 2 CSA</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="321"/>
        <source>Postscript 2 Rendering Intent</source>
        <translation>Postscript 2 Übertragungsart</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="322"/>
        <source>Red Colorant</source>
        <translation>Rote Grundfarbe</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="323"/>
        <source>Red tone reproduction curve</source>
        <translation>Rote Farbwiedergabekurve</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="324"/>
        <source>Screening Description</source>
        <translation>Rasterung Beschreibung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="325"/>
        <source>Screening</source>
        <translation>Rasterung</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="326"/>
        <source>Technology</source>
        <translation>Technologie</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="327"/>
        <source>Under Color Removal (UCR)</source>
        <translation>Unterfarbreduktion (UCR)</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="328"/>
        <source>Viewing conditions description</source>
        <translation>Beschreibung der Betrachtungbedingungen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="329"/>
        <source>Viewing Conditions</source>
        <translation>Betrachtungsbedingungen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="331"/>
        <source>Device colors</source>
        <translation>Farbmessflächen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="333"/>
        <source>Measured colors</source>
        <translation>Farbmessergebnisse</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="335"/>
        <source>Profiling parameters</source>
        <translation>Profilierungsparameter</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="337"/>
        <source>VideoCardGammaTable</source>
        <translation>Grafik Karten Gamma Tabelle (vcgt)</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="420"/>
        <source>Curve with gamma %1</source>
        <translation>Kurve mit Gamma %1</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="453"/>
        <source>Type:3 
f(X)=(%2*X+%3)^%1 for X&gt;=%5 
f(X)=%4*X for (X&lt;%5)</source>
        <translation>Typ:3 
f(X)=(%2*X+%3)^%1 für X&gt;=%5 
f(X)=%4*X für (X&lt;%5)</translation>
    </message>
    <message>
        <source>Type:0 
f(X)=X^gamma 
gamma=%1</source>
        <translation type="vanished">Typ:0 
f(X)=X^Gamma 
Gamma=%1</translation>
    </message>
    <message>
        <source>Type:1 
f(X)=(a*X+b)^gamma for X&gt;=-b/a 
f(X)=0 for (X&lt;-b/a) 
gamma=%1 
a=%2 
b=%3</source>
        <translation type="vanished">Typ:1 
f(X)=(a*X+b)^Gamma für X&gt;=-b/a 
f(X)=0 für (X&lt;-b/a) 
Gamma=%1 
a=%2 
b=%3</translation>
    </message>
    <message>
        <source>Type:2 
f(X)=(a*X+b)^gamma + c for X&gt;=-b/a 
f(X)=c for (X&lt;-b/a) 
gamma=%1 
a=%2 
b=%3 
c=%4</source>
        <translation type="vanished">Typ:2 
f(X)=(a*X+b)^Gamma + c für X&gt;=-b/a 
f(X)=c für (X&lt;-b/a) 
Gamma=%1 
a=%2 
b=%3 
c=%4</translation>
    </message>
    <message>
        <source>Type:3 
f(X)=(a*X+b)^gamma for X&gt;=d 
f(X)=c*X for (X&lt;c) 
gamma=%1 
a=%2 
b=%3 
c=%4 
d=%5</source>
        <translation type="vanished">Typ:3 
f(X)=(a*X+b)^Gamma für X&gt;=d 
f(X)=c*X für (X&lt;c) 
Gamma=%1 
a=%2 
b=%3 
c=%4 
d=%5</translation>
    </message>
    <message>
        <source>Type:4 
f(X)=(a*X+b)^gamma+e for X&gt;=d 
f(X)=c*X+f for (X&lt;d) 
gamma=%1 
a=%2 
b=%3 
c=%4 
d=%5 
e=%6 
f=%7</source>
        <translation type="vanished">Typ:4 
f(X)=(a*X+b)^Gamma+e für X&gt;=d 
f(X)=c*X+f für (X&lt;d) 
Gamma=%1 
a=%2 
b=%3 
c=%4 
d=%5 
e=%6 
f=%7</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="467"/>
        <source>Parametric Curve Type: %1</source>
        <translation>Parametrische Kurve Typ: %1</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="715"/>
        <location filename="../src/icc_profile.cpp" line="745"/>
        <location filename="../src/icc_profile.cpp" line="849"/>
        <source>B-Curves</source>
        <translation>B-Kurven</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="741"/>
        <location filename="../src/icc_profile.cpp" line="824"/>
        <source>M-Curves</source>
        <translation>M-Kurven</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="775"/>
        <location filename="../src/icc_profile.cpp" line="795"/>
        <source>A-Curves</source>
        <translation>A-Kurven</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="872"/>
        <source>Elements: %1</source>
        <translation>Elemente: %1</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="887"/>
        <location filename="../src/icc_profile.cpp" line="910"/>
        <source>Curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1229"/>
        <location filename="../src/icc_profile.cpp" line="1280"/>
        <source>Curve</source>
        <translation>Kurve</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1264"/>
        <source>Parametric Curves</source>
        <translation>Parametrische Kurven</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="1294"/>
        <source>Curves with %1 segments</source>
        <translation>Kurven mit %1 Segmenten</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="obsolete">Rot</translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="obsolete">Grün</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="obsolete">Blau</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="271"/>
        <source>Color adaption matrix</source>
        <translation>Farbanpassungsmatrix</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="272"/>
        <source>Chromaticity</source>
        <translation>Primärfarben</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="275"/>
        <source>Color channel order</source>
        <translation>Farbkanalordnung</translation>
    </message>
    <message>
        <source>Color channel names</source>
        <translation type="vanished">Farbkanalnamen</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="341"/>
        <source>----</source>
        <translation>----</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="369"/>
        <source>Macintosh</source>
        <translation>Macintosh</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="370"/>
        <source>Microsoft</source>
        <translation>Microsoft</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="371"/>
        <source>Solaris</source>
        <translation>Solaris</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="372"/>
        <source>SGI</source>
        <translation>SGI</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="373"/>
        <source>Taligent</source>
        <translation>Taligent</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="374"/>
        <source>Unix and derivatives</source>
        <translation>Unix und unixartige</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="388"/>
        <source>Adobe CMM</source>
        <translation>Adobe CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="389"/>
        <source>Agfa CMM</source>
        <translation>Agfa CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="390"/>
        <source>Apple CMM</source>
        <translation>Apple CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="391"/>
        <source>ColorGear CMM</source>
        <translation>ColorGear CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="392"/>
        <source>ColorGear CMM Lite</source>
        <translation>ColorGear CMM Lite</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="393"/>
        <source>ColorGear CMM C</source>
        <translation>ColorGear CMM C</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="394"/>
        <source>EFI CMM</source>
        <translation>EFI CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="395"/>
        <source>Fuji Film CMM</source>
        <translation>Fuji Film CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="396"/>
        <source>Harlequin RIP CMM</source>
        <translation>Harlequin RIP CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="397"/>
        <source>Argyll CMS CMM</source>
        <translation>Argyll CMS CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="398"/>
        <source>LogoSync CMM</source>
        <translation>LogoSync CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="399"/>
        <source>Heidelberg CMM</source>
        <translation>Heidelberg CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="400"/>
        <source>Little CMS CMM</source>
        <translation>Little CMS CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="401"/>
        <source>Kodak CMS</source>
        <translation>Kodak CMS</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="402"/>
        <source>Konica Minolta CMM</source>
        <translation>Konica Minolta CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="403"/>
        <source>Mutho CMM</source>
        <translation>Mutho CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="404"/>
        <source>DeviceLink CMM</source>
        <translation>DeviceLink CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="405"/>
        <source>SampleICC CMM</source>
        <translation>SampleICC CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="406"/>
        <source>the imaging factory CMM</source>
        <translation>the imaging factory CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="407"/>
        <source>Ware To Go CMM</source>
        <translation>Ware To Go CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="408"/>
        <source>Zoran CMM</source>
        <translation>Zoran CMM</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="422"/>
        <source>Curve with %1 segments</source>
        <translation>Kurve mit %1 Segmenten</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="443"/>
        <source>Type:0 
f(X)=X^%1</source>
        <translation>Typ:0 
f(X)=X^%1</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="444"/>
        <source>Type:1 
f(X)=(%2*X+%3)^%1 for X&gt;=-%3/%2 
f(X)=0 for (X&lt;-%3/%2)</source>
        <translation>Typ:1 
f(X)=(%2*X+%3)^%1 für X&gt;=-%3/%2 
f(X)=0 für (X&lt;-%3/%2)</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="448"/>
        <source>Type:2 
f(X)=(%2*X+%3)^%1 + %4 for X&gt;=-%3/%2 
f(X)=%4 for (X&lt;-%3/%2)</source>
        <translation>Typ:2 
f(X)=(%2*X+%3)^%1 + %4 für X&gt;=-%3/%2 
f(X)=%4 für (X&lt;-%3/%2)</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="459"/>
        <source>Type:4 
f(X)=(%2*X+%3)^%1+%6 for X&gt;=%5 
f(X)=%4*X+%7 for (X&lt;%5)</source>
        <translation>Typ:4 
f(X)=(%2*X+%3)^%1+%6 für X&gt;=%5 
f(X)=%4*X+%7 für (X&lt;%5)</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="563"/>
        <location filename="../src/icc_profile.cpp" line="898"/>
        <location filename="../src/icc_profile.cpp" line="909"/>
        <source>CLUT</source>
        <translation>Interpolationstabelle</translation>
    </message>
    <message>
        <location filename="../src/icc_profile.cpp" line="690"/>
        <location filename="../src/icc_profile.cpp" line="728"/>
        <location filename="../src/icc_profile.cpp" line="837"/>
        <location filename="../src/icc_profile.cpp" line="911"/>
        <source>Matrix</source>
        <translation>Matrix</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="2"/>
        <source>The ICC Examin colour examination tool accepts as input ICC profiles, CGATS colour measurement files and named colour files.</source>
        <translation>Der ICC Examin Farbprofilebetrachter liest ICC Farbprofile, CGATS Farbmessdateien und Farblisten.</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="5"/>
        <source>May 18, 2017</source>
        <translation>18. Mai 2017</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="8"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="11"/>
        <source>Show GUI</source>
        <translation>Zeige Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="14"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="17"/>
        <source>Profile Name</source>
        <translation>Profilname</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="20"/>
        <source>ICC_PROFILE_NAME</source>
        <translation>ICC_PROFIL_NAME</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="23"/>
        <location filename="../i18n.cpp" line="26"/>
        <source>Verbose</source>
        <translation>Plaudernd</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="29"/>
        <location filename="../i18n.cpp" line="32"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="35"/>
        <source>SEE ALSO</source>
        <translation>SIEHE AUCH</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="38"/>
        <source>ICC Viewer</source>
        <translation>ICC Betrachter</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="41"/>
        <source>Misc</source>
        <translation>Verschiedenes</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="44"/>
        <source>General options</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="47"/>
        <source>graphical ICC examination tool</source>
        <translation>graphisches ICC Untersuchungswerkzeug</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="50"/>
        <source>ICC Version 2 and 4 compatible Color Profile Examine Tool</source>
        <translation>ICC Version 2 und 4 kompatibler Farbprofilbetrachter</translation>
    </message>
    <message>
        <location filename="../i18n.cpp" line="53"/>
        <source>For more information read the man page:</source>
        <translation>Mehr Informationen stehen im Handbuch:</translation>
    </message>
</context>
<context>
    <name>TagView</name>
    <message>
        <location filename="../qml/TagView.qml" line="137"/>
        <source>Launched app for </source>
        <translation>Rufe Programm für </translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="427"/>
        <location filename="../qml/TagView.qml" line="539"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="428"/>
        <location filename="../qml/TagView.qml" line="544"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="429"/>
        <location filename="../qml/TagView.qml" line="549"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="430"/>
        <location filename="../qml/TagView.qml" line="555"/>
        <source>Cyan</source>
        <translation>Zyan</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="431"/>
        <location filename="../qml/TagView.qml" line="560"/>
        <source>Magenta</source>
        <translation>Magenta</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="432"/>
        <location filename="../qml/TagView.qml" line="565"/>
        <source>Yellow</source>
        <translation>Gelb</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="433"/>
        <location filename="../qml/TagView.qml" line="570"/>
        <source>Black</source>
        <translation>Schwarz</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="434"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="446"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="480"/>
        <source>Count:</source>
        <translation>Anzahl:</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="497"/>
        <source>undefined</source>
        <translation>nicht definiert</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="518"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="575"/>
        <source>CIE*L</source>
        <translation>CIE*L</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="580"/>
        <source>CIE*a</source>
        <translation>CIE*a</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="585"/>
        <source>CIE*b</source>
        <translation>CIE*b</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="591"/>
        <source>CIE*X</source>
        <translation>CIE*X</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="596"/>
        <source>CIE*Y</source>
        <translation>CIE*Y</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="601"/>
        <source>CIE*Z</source>
        <translation>CIE*Z</translation>
    </message>
    <message>
        <source>Curve with Default Gamma of 1.0</source>
        <translation type="vanished">Kurve mit Standardgamma 1,0</translation>
    </message>
    <message>
        <source>Curve with Gamma of %1</source>
        <translation type="vanished">Kurve mit Gamma %1</translation>
    </message>
    <message>
        <source>Curve with %1 segments</source>
        <translation type="vanished">Kurve mit %1 Segmenten</translation>
    </message>
    <message>
        <source>Curve</source>
        <translation type="vanished">Kurve</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="313"/>
        <source>Show Less</source>
        <translation>Zeige weniger</translation>
    </message>
    <message>
        <location filename="../qml/TagView.qml" line="313"/>
        <source>Show More</source>
        <translation>Zeige mehr</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>About Text</source>
        <translation type="obsolete">Über Text</translation>
    </message>
    <message>
        <source>ICC compatible Color Profile Examine Tool</source>
        <translation type="vanished">ICC kompatibler Farbprofilbetrachter</translation>
    </message>
    <message>
        <source>All Rights reserved.</source>
        <translation type="vanished">Alle Rechte reserviert.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="77"/>
        <source>Landscape</source>
        <translation>Querformat</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="79"/>
        <source>Portrait</source>
        <translation>Hochformat</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="81"/>
        <source>Inverted Landscape</source>
        <translation>Umgedreht Querformat</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="83"/>
        <source>Inverted Portrait</source>
        <translation>Umgedreht Hochformat</translation>
    </message>
    <message>
        <source>Screen Info</source>
        <translation type="obsolete">Bildschirm Informationen</translation>
    </message>
    <message>
        <source>Screen Name</source>
        <translation type="obsolete">Bildschirmname</translation>
    </message>
    <message>
        <source>Screen Dimensions</source>
        <translation type="obsolete">Bildschirmdimensionen</translation>
    </message>
    <message>
        <source>Virtual Dimensions</source>
        <translation type="obsolete">Virtuelle Dimensionen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="213"/>
        <location filename="../qml/main.qml" line="214"/>
        <source>dots/mm</source>
        <translation>Punkte/mm</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="211"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="212"/>
        <source>Dimensions</source>
        <translation>Dimensionen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="215"/>
        <source>Device Pixel Ratio</source>
        <translation>Bildpunktseitenverhältnis</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="213"/>
        <source>Pixel Density</source>
        <translation>Bildpunktdichte</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="75"/>
        <source>Primary</source>
        <translation>Primär</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Informationen</translation>
    </message>
    <message>
        <source>Screen Informations</source>
        <translation type="vanished">Bildschirminformationen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="213"/>
        <location filename="../qml/main.qml" line="214"/>
        <source>dots/inch</source>
        <translation>DPI</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="214"/>
        <source>Logical Pixel Density</source>
        <translation>Logische Bildpunktdichte</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="216"/>
        <source>Orientation</source>
        <translation>Orientierung</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="217"/>
        <source>Virtual Desktop Dimensions</source>
        <translation>Virtuelle Desktop Dimensionen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="218"/>
        <source>Platform</source>
        <translation>Plattform</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>Sorry</source>
        <translation type="vanished">Oops</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="128"/>
        <source>Loaded</source>
        <translation>Geladen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="160"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="167"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="176"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="185"/>
        <source>About...</source>
        <translation>Info...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="194"/>
        <location filename="../qml/main.qml" line="201"/>
        <source>CIE*xy</source>
        <translation>CIE*xy</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="199"/>
        <source>CIE*Lab</source>
        <translation>CIE*Lab</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="206"/>
        <source>Screen Infos ...</source>
        <translation>Bildschirminfos ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="227"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="236"/>
        <location filename="../qml/main.qml" line="239"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="247"/>
        <source>&amp;File ...</source>
        <translation>&amp;Datei ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="386"/>
        <source>Busy</source>
        <translation>Beschäftigt</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="599"/>
        <source>Show Less</source>
        <translation>Zeige weniger</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="599"/>
        <source>Show More</source>
        <translation>Zeige mehr</translation>
    </message>
</context>
</TS>
