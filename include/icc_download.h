/** @file icc_download.h
 *
 *  ICC Examin is a color profile viewer.
 *
 *  @par Copyright:
 *            2014-2017 (C) Kai-Uwe Behrmann
 *            All Rights reserved.
 *
 *  @par License:
 *            AGPL-3.0 <https://opensource.org/licenses/AGPL-3.0>
 *  @since    2015/01/06
 *
 *  networking
 */

#ifndef ICC_DOWNLOAD_H
#define ICC_DOWNLOAD_H

#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#ifdef OYRANOS_LIB
# include <oyranos_types.h>
#else
#define OY_UNUSED
#endif

#include "include/utils.h"

class IccDownload : public QObject
{
    Q_OBJECT
    QNetworkAccessManager qnam;
    QNetworkReply * reply;
    QUrl url_;
    QByteArray data_;
    Q_PROPERTY(QString url
               READ getUrl
               WRITE setUrl
               NOTIFY downloadFinished
               REVISION 1)
    Q_PROPERTY(QString data
               READ getData
               NOTIFY downloadFinished
               REVISION 1)
public:
    IccDownload(QObject * parent = 0) : QObject(parent)
    {
        connect(&qnam, SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),
                this, SLOT(authenticationRequired(QNetworkReply*,QAuthenticator*)));
        #ifndef QT_NO_OPENSSL
            connect(&qnam, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
                    this, SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
        #endif
    }

    QByteArray data()
    {
        return data_;
    }

    QString getData()
    {
        return QString(data_.data());
    }

    QString getUrl()
    {
        return url_.toString();
    }
    void setUrl(QString t)
    {
        url(QUrl(t));
    }

    void url(QUrl url)
    {
        connect(this, SIGNAL(setUrlSignal(QUrl)),
                this, SLOT(setUrlSlot(QUrl)),
                Qt::UniqueConnection);

        emit setUrlSignal( url );

        LOG(QString("download start"));
    }
signals:
    void downloadFinished( QByteArray, QString );
    void setUrlSignal( QUrl );
private slots:
    void setUrlSlot(QUrl url)
    {
        data_.clear();
        url_ = url;

        QNetworkRequest request(url);
        reply = qnam.get(request);

        connect(reply, SIGNAL(finished()),
                this, SLOT(finished()));
        connect(reply, SIGNAL(readyRead()),
                this, SLOT(readyRead()));
        connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
                this, SLOT(updateDataReadProgress(qint64,qint64)));

        LOG(QString("download started"));
    }

    void finished()
    {
        if(reply->error() != 200)
            LOG(QString("download error"));
        else
            LOG(QString("download finished"));
        reply->deleteLater();
        emit downloadFinished( data_, url_.toString() );
    }
    void readyRead()
    {
        LOG(QString("download ready"));
        data_ += reply->readAll();
    }
    void updateDataReadProgress(qint64 bytesRead, qint64 totalBytes) { LOG(QString("downloaded:") + QString::number(bytesRead) + " from " + QString::number(totalBytes) ); }
    void authenticationRequired(QNetworkReply*,QAuthenticator *) { LOG(QString("download requires authentication: not implemented")); }
#ifndef QT_NO_OPENSSL
    void sslErrors(QNetworkReply*,const QList<QSslError> OY_UNUSED &errors) { LOG(QString("download gave ssl errors: not implemented")); }
#endif
};

#endif // ICC_DOWNLOAD_H

